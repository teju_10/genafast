package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.MyOrderAdapter;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.model.MyOrderModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.ConnectionDetector;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class MyOrderActivity extends BaseActivity {
    static Context mContext;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.list_notification)
    ListView listNotification;
    private ArrayList<MyOrderModel> arrayList = new ArrayList<>();
    private MyOrderAdapter myOrderAdapter;

    public static boolean checkInternet(Context context) {
        // TODO Auto-generated method stub
        mContext = context;
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    public static void showErrorDialog(Context c) {
        final NiftyDialogBuilder material;
        material = NiftyDialogBuilder.getInstance(c);
        material.withTitle(c.getString(R.string.text_warning))
                .withMessage(c.getString(R.string.internet_check_error))
                .withDialogColor(c.getString(R.string.colorErrorDialog))
                .withButton1Text(mContext.getString(R.string.str_login)).setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                material.cancel();
            }
        }).withDuration(1000).withEffect(Effectstype.Fadein).show();
    }

    @Override
    protected int getContentResId() {
        return R.layout.activity_my_order;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("My Orders");
        }else{
            setToolbarWithBackButton("Mis ordenes");
        }

       // setToolbarWithBackButton(getApplication().getString(R.string.txt_my_orders));

        listNotification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(MyOrderActivity.this, CompleteOrder.class);
                i.putExtra("orderid", arrayList.get(position).getOrder_no());
                i.putExtra("key", "notification");
                startActivity(i);
            }
        });

        if (checkInternet(MyOrderActivity.this)) {
            callApiForGetMyOrders();
        } else showErrorDialog(MyOrderActivity.this);

    }

    private void callApiForGetMyOrders() {
        S.E("UserDataHelper.getInstance().getData().get(0).getUserid() : " + UserDataHelper.getInstance().getData().get(0).getUserid());
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_order_history(
                UserDataHelper.getInstance().getData().get(0).getUserid()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("my order response : " + response);
                try {
                    arrayList.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")) {
                        JSONArray order = jsonObject.getJSONArray("order");
                        for (int i = 0; i < order.length(); i++) {
                            JSONObject json = order.getJSONObject(i);
                            MyOrderModel myOrderModel = new MyOrderModel();
                            myOrderModel.setOrder_no(json.getString("order_no"));
                            myOrderModel.setTotal_amount(json.getString("total_amount"));
                            myOrderModel.setItems(json.getString("items"));
                            myOrderModel.setDate(json.getString("date"));
                            myOrderModel.setCurrency(json.getString("currency"));
                            myOrderModel.setRestaurant_name(json.getString("restaurant_name"));
                            myOrderModel.setRestaurant_address(json.getString("restaurant_address"));
                            myOrderModel.setStatus(json.getString("status"));
                            myOrderModel.setRestuarantId(json.getString("rest_id"));
                            myOrderModel.setRestaurantRatting(json.getString("restaurant_ratting"));

                            arrayList.add(myOrderModel);
                        }

                        myOrderAdapter = new MyOrderAdapter(MyOrderActivity.this, arrayList);
                        listNotification.setAdapter(myOrderAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext,MainActivity.class));
        finish();
    }
}
