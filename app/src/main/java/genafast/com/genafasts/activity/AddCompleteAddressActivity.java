package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class AddCompleteAddressActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_comple_address)
    EditText ed_comple_address;
    @BindView(R.id.ed_landmark)
    EditText ed_landmark;
    @BindView(R.id.btn_sndCompleteAddress)
    Button btn_sndCompleteAddress;
    @BindView(R.id.tv_change_address)
    TextView tv_change_address;

    @BindView(R.id.tv_home)
    TextView tv_home;
    @BindView(R.id.tv_work)
    TextView tv_work;
    @BindView(R.id.tv_other)
    TextView tv_other;
    Geocoder geocoder = null;
    String strAddress = "", str_NickName = "Home";
    Context mContext;
    private double latitudecur, longitudecur;

    @Override
    protected int getContentResId() {
        return R.layout.activity_addaddress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setToolbarWithBackButton("");
        mContext = this;
        geocoder = new Geocoder(this);
        init();
    }

    private void init() {
        tv_change_address.setOnClickListener(this);
        tv_home.setOnClickListener(this);
        tv_work.setOnClickListener(this);
        tv_other.setOnClickListener(this);
        btn_sndCompleteAddress.setOnClickListener(this);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("ADD DIRECTION");
        }else{
            setToolbarWithBackButton("AÑADIR DIRECCIÓN");
        }

//        try {
//            if (!getIntent().getStringExtra(Constants.ADDRESS).equals("")) {
//                strAddress = getIntent().getStringExtra(Constants.ADDRESS);
//                ed_address.setText(strAddress);
//                Utility.setSharedPreference(mContext, Constants.ADDRESS, strAddress);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        if (!Utility.getSharedPreferences(mContext, Constants.ADDRESS).equals("")) {
            ed_address.setText(Utility.getSharedPreferences(mContext, Constants.ADDRESS));
        }
        themeHomeBtn();
    }


    private void apiSendCompleteAddress() {
        S.E("LatLong-====" + latitudecur + " " + longitudecur);
        new JSONParser(mContext).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).AddCompleteAddress(
                "addAddress",
                UserDataHelper.getInstance().getData().get(0).getUserid(),
                ed_address.getText().toString(),
                ed_comple_address.getText().toString(),
                ed_landmark.getText().toString(), str_NickName,
                String.valueOf(latitudecur),
                String.valueOf(longitudecur)
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.setSharedPreference(mContext, Constants.ADDRESS, strAddress);
                Utility.E("address response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    S.T(mContext, jsonObject.getString("message"));
                    S.I_clear(mContext, AddressActivity.class, null);

//                    if (status.equals("Success")) {
//                        Intent intent = getIntent();
//                        setResult(RESULT_OK, intent);
//                        finish();
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 102)
            if (resultCode == RESULT_OK) {
//                Place place = PlaceAutocomplete.getPlace(this, data);
//                S.E("Place Message LATLNG== " + place.getLatLng());
//                strAddress = place.getAddress().toString();
//                LatLng lng = place.getLatLng();
                String result = data.getStringExtra(Constants.ADDRESS);
                latitudecur = Double.parseDouble(data.getStringExtra("lati"));
                longitudecur = Double.parseDouble(data.getStringExtra("longi"));
                S.E("Message==== " + latitudecur + " " + longitudecur);
                S.E("Place Message Address== " + result);
                ed_address.setText(result);
                Utility.setSharedPreference(mContext, Constants.ADDRESS, result);
            }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_sndCompleteAddress:
                if (Utility.isConnectingToInternet(mContext)) {
                    if (ed_address.getText().toString().isEmpty()) {
                        S.T(mContext, mContext.getResources().getString(R.string.txt_add_delivery_food));
                    } else if (ed_comple_address.getText().toString().isEmpty()) {
                        S.T(mContext, mContext.getResources().getString(R.string.txt_addcompleteaddress));
                    } else if (ed_landmark.getText().toString().isEmpty()) {
                        S.T(mContext, mContext.getResources().getString(R.string.txt_add_landmark));
                    } else if (str_NickName.equals("")) {
                        S.T(mContext, mContext.getResources().getString(R.string.nickname_address));
                    } else {

                        apiSendCompleteAddress();
                    }

                } else {
                    S.T(mContext, "");
                }
                break;

            case R.id.tv_change_address:
                try {
//                    Intent i = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(AddCompleteAddressActivity.this);
//                    startActivityForResult(i, 101);
                    Intent i = new Intent(mContext, SelectGoogleAddress.class);
                    startActivityForResult(i, 102);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tv_home:
                themeHomeBtn();
                break;

            case R.id.tv_work:
                themeWorkBtn();
                break;

            case R.id.tv_other:
                themeOtherBtn();
                break;
        }
    }

    private void themeHomeBtn() {
        tv_home.setTextColor(getResources().getColor(R.color.white));
        tv_work.setTextColor(getResources().getColor(R.color.green));
        tv_other.setTextColor(getResources().getColor(R.color.green));

        tv_home.setBackgroundResource(R.drawable.edit_btn_blue);
        tv_work.setBackgroundResource(R.drawable.btn_add_bg);
        tv_other.setBackgroundResource(R.drawable.btn_add_bg);

        str_NickName = "Home";
    }

    private void themeWorkBtn() {
        tv_home.setTextColor(getResources().getColor(R.color.green));
        tv_other.setTextColor(getResources().getColor(R.color.green));
        tv_work.setTextColor(getResources().getColor(R.color.white));

        tv_work.setBackgroundResource(R.drawable.edit_btn_blue);
        tv_other.setBackgroundResource(R.drawable.btn_add_bg);
        tv_home.setBackgroundResource(R.drawable.btn_add_bg);
        str_NickName = "Work";
    }

    private void themeOtherBtn() {
        tv_other.setTextColor(getResources().getColor(R.color.white));
        tv_home.setTextColor(getResources().getColor(R.color.green));
        tv_work.setTextColor(getResources().getColor(R.color.green));

        tv_other.setBackgroundResource(R.drawable.edit_btn_blue);
        tv_home.setBackgroundResource(R.drawable.btn_add_bg);
        tv_work.setBackgroundResource(R.drawable.btn_add_bg);

        str_NickName = "Other";
    }
}
