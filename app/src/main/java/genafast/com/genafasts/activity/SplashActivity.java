package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.iid.FirebaseInstanceId;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.datahelper.UserDataHelper;

public class SplashActivity extends AppCompatActivity {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mContext = this;
        methodForchanegeGet();

        CartDataHelper.getInstance().delete();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utility.getSharedPreferences(mContext, Constants.SPL_CHK).equals("1")) {
                    //  loadAnimation();
                        startActivity(new Intent(mContext, MainActivity.class));
                        finish();
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    } else {
                        startActivity(new Intent(mContext, LanguageChangeActivity.class));
                        finish();
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    }

            }
        }, 4000);
    }

    private void methodForchanegeGet() {

        if (Utility.getSharedPreferences(getApplication(), Constants.DEVICE_ID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is " + token);
            Utility.setSharedPreference(getApplication(), Constants.DEVICE_ID, token);
        } else {
            System.out.println("token is " + Utility.getSharedPreferences(getApplication(), Constants.DEVICE_ID));

        }
    }
}

