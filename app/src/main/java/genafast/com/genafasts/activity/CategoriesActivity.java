package genafast.com.genafasts.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.model.CategoryModel;
import genafast.com.genafasts.model.SubCategoryModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.ConnectionDetector;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

@SuppressWarnings("unchecked")
public class CategoriesActivity extends BaseActivity {
    public static Context mContext;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.listview)
    ListView rv_catList;
    @BindView(R.id.cat_search)
    Button catSearch;
    private String subCatId = "", subCatName = "";
    private LinkedHashMap<CategoryModel, ArrayList<SubCategoryModel>> map = new LinkedHashMap<>();

    public static boolean checkInternet(Context context) {
        // TODO Auto-generated method stub
        mContext = context;
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    public static void showErrorDialog(Context c) {
        final NiftyDialogBuilder material;
        material = NiftyDialogBuilder.getInstance(c);
        material.withTitle(c.getString(R.string.text_warning))
                .withMessage(c.getString(R.string.internet_check_error))
                .withDialogColor(c.getString(R.string.colorErrorDialog))
                .withButton1Text(mContext.getString(R.string.ok)).setButton1Click(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                material.cancel();
            }
        })
                .withDuration(1000)
                .withEffect(Effectstype.Fadein)
                .show();
    }

    @Override
    protected int getContentResId() {
        return R.layout.activity_categories;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Category");
        }else{
            setToolbarWithBackButton("Categoría");
        }

    //    setToolbarWithBackButton(getApplicationContext().getString(R.string.txt_category));

        if (checkInternet(CategoriesActivity.this))
            callApiForGetCategories();
        else showErrorDialog(CategoriesActivity.this);
    }

    private void callApiForGetCategories() {

        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getRestaurantCategories(), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("categories resposne : " + response);
                try {
                    final JSONObject jObject = new JSONObject(response);
                    if (jObject.getString("status").equals("Success")) {
                        JSONArray category_list = jObject.getJSONArray("Category_List");
                        for (int qw = 0; qw < category_list.length(); qw++) {
                            JSONObject cat_detail = category_list.getJSONObject(qw);
                            CategoryModel temp = new CategoryModel();
                            temp.setName(cat_detail.getString("name"));
                            temp.setId(cat_detail.getString("id"));
                            JSONArray sub_cat = cat_detail.getJSONArray("subcategory");
                            ArrayList<SubCategoryModel> subData = new ArrayList<>();
                            for (int i = 0; i < sub_cat.length(); i++) {
                                SubCategoryModel tempSub = new SubCategoryModel();
                                tempSub.setName(sub_cat.getJSONObject(i).getString("name"));
                                tempSub.setId(sub_cat.getJSONObject(i).getString("id"));
                                subData.add(tempSub);
                            }
                            map.put(temp, subData);
                        }

                        final ArrayList list = new ArrayList();
                        for (CategoryModel getSet : map.keySet()) {
                            list.add(getSet);
                            list.addAll(map.get(getSet));
                        }
                        final CategoryAdapter adap = new CategoryAdapter(CategoriesActivity.this, list);
                        rv_catList.setAdapter(adap);
                        rv_catList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                subCatId = ((SubCategoryModel) list.get(position)).getId();
                                subCatName = ((SubCategoryModel) list.get(position)).getName();
                                adap.notifyDataSetChanged();
                            }
                        });

                        Button cat_search = findViewById(R.id.cat_search);
                        cat_search.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent iv = new Intent(CategoriesActivity.this, MainActivity.class);
                                iv.putExtra("sub_category_name", "" + subCatName);
                                iv.putExtra("sub_category_id", "" + subCatId);
                                startActivity(iv);

                            }
                        });

                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(CategoriesActivity.this, jObject.getString("status"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    class CategoryAdapter extends BaseAdapter {
        private static final int TYPE_ITEM = 1;
        private static final int TYPE_HEADER = 2;
        private final ArrayList data1;
        private final Activity activity;
        private LayoutInflater inflater = null;


        CategoryAdapter(Activity a, ArrayList str) {
            activity = a;
            data1 = str;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data1.size();
        }

        @Override
        public Object getItem(int position) {
            return data1.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            if (getItem(position) instanceof CategoryModel) {
                return TYPE_HEADER;
            } else
                return TYPE_ITEM;

        }

        @Override
        public boolean isEnabled(int position) {
            return (getItemViewType(position) == TYPE_ITEM);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            int type = getItemViewType(position);
            switch (type) {
                case TYPE_ITEM:
                    vi = inflater.inflate(R.layout.cell_maincategory, parent, false);
                    break;
                case TYPE_HEADER:
                    vi = inflater.inflate(R.layout.header_category, parent, false);
                    break;
            }

            switch (type) {
                case TYPE_ITEM:
                    TextView txt_first = vi.findViewById(R.id.txt_first);
                    TextView txt_subcategory = vi.findViewById(R.id.txt_subcategory);
                    SubCategoryModel item = (SubCategoryModel) data1.get(position);
                    Log.e("namecat", "" + item.getName());
                    txt_subcategory.setText(item.getName());
                    String namefirst = (item.getName());

                    if(!namefirst.equals("")){
                        String s1 = String.valueOf(namefirst).substring(0, 1).toUpperCase();
                        txt_first.setText(s1);
                    }

                    final ImageView imageView = vi.findViewById(R.id.id_click);
                    if (subCatId.equals(item.getId()))
                        imageView.setVisibility(View.VISIBLE);
                    else imageView.setVisibility(View.GONE);
                    break;
                case TYPE_HEADER:
                    TextView title = vi.findViewById(R.id.txt_name);
                    CategoryModel head = (CategoryModel) data1.get(position);
                    title.setText(head.getName());
                    vi.setActivated(false);
                    vi.setFocusable(false);
                    break;
            }
            return vi;

        }
    }
}
