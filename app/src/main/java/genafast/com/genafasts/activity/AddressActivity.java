package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.AddressAdapter;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.model.AddressModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;


public class AddressActivity extends BaseActivity {
    public static final int REQUEST_CODE = 1;
    @BindView(R.id.tvaddress)
    TextView tvaddress;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tvanotheraddress)
    TextView tvanotheraddress;
    @BindView(R.id.btnAddAddress)
    Button btnAddAddress;
    @BindView(R.id.ll_use_currnt_loc)
    LinearLayout ll_use_currnt_loc;
    String address = "";
    ArrayList<AddressModel> arrayListAddress = new ArrayList<>();
    Context mContext;

    @Override
    protected int getContentResId() {
        return R.layout.activity_address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        mContext = this;
        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Choose Address");
        }else{
            setToolbarWithBackButton("Elegir direccion");
        }

       // setToolbarWithBackButton(getApplicationContext().getString(R.string.txt_choose_address));

        getAddress();

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressActivity.this, AddCompleteAddressActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });


    }


    private void getAddress() {
        S.E("Address Activity=== Restaurnt ID******* " + Utility.getSharedPreferences(mContext,Constants.RESTAURANT_ID));
        new JSONParser(this).parseRetrofitRequestWithautProgress(ApiClient.getClient().create(ApiInterface.class).getAddress(
                "getAddress",
                UserDataHelper.getInstance().getData().get(0).getUserid()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.E("get address reponse : " + response);
                try {
                    arrayListAddress.clear();
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = jsonArray.getJSONObject(i);

                        AddressModel addressModel = new AddressModel();
                        addressModel.setId(json.getString("id"));
                        addressModel.setUser_id(json.getString("user_id"));
                        addressModel.setAddress(json.getString("address"));
                        addressModel.setLat(json.getString("lat"));
                        addressModel.setAddress_nickName(json.getString("nickname_address"));
                        addressModel.setCompleteAddress(json.getString("complete_address"));
                        addressModel.setLng(json.getString("long"));
                        addressModel.setStatus(json.getString("status"));
                        addressModel.setCreated_at(json.getString("created_at"));
                        arrayListAddress.add(addressModel);
                    }
                    Collections.reverse(arrayListAddress);
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddressActivity.this));
                    AddressAdapter addressAdapter = new AddressAdapter(AddressActivity.this, arrayListAddress, Utility.getSharedPreferences(mContext,Constants.RESTAURANT_ID));
                    recyclerView.setAdapter(addressAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
                arrayListAddress.clear();
                getAddress();

            }
        } catch (Exception ex) {
            Toast.makeText(AddressActivity.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAddress();
    }


}
