package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.adapter.PlaceArrayAdapter;
import genafast.com.genafasts.utils.GPSTracker;

public class SelectGoogleAddress extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    public static final int REQUEST_CODE = 1;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(22.724355, 75.8838944), new LatLng(22.7532848, 75.8936962));
    private static String TAG = "SelectGoogleAddress ";
    @BindView(R.id.ll_use_currnt_loc)
    LinearLayout ll_use_currnt_loc;
    Context mContext;
    double currentLat = 0.0, currentLng = 0.0;
    Geocoder geocoder;
    List<Address> addresses;
    private AutoCompleteTextView edt_address;
    private String address;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private GoogleApiClient mGoogleApiClient;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            edt_address.setText(Html.fromHtml(place.getAddress() + ""));
            Intent returnIntent = new Intent(getApplication(), AddressActivity.class);
            returnIntent.putExtra(Constants.ADDRESS, place.getAddress());
            returnIntent.putExtra("lati", String.valueOf(place.getLatLng().latitude));
            returnIntent.putExtra("longi", String.valueOf(place.getLatLng().longitude));
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    };
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.e(TAG, "Fetching details for ID: " + item.placeId);


        }
    };

    @Override
    protected int getContentResId() {
        return R.layout.activity_selectgoogleaddress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mContext = this;
        edt_address = findViewById(R.id.edt_address);
        mGoogleApiClient = new GoogleApiClient.Builder(SelectGoogleAddress.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        edt_address.setOnItemClickListener(mAutocompleteClickListener);

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        edt_address.setAdapter(mPlaceArrayAdapter);


        gettingLocation();

        ll_use_currnt_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAddressLatLng(currentLat, currentLng);
            }
        });

    }

    private void getAddressLatLng(double lat, double lng) {
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            address = addresses.get(0).getAddressLine(0);
            System.out.println("Complete adressssss  " + address);

            Intent i = new Intent(mContext, AddCompleteAddressActivity.class);
            i.putExtra(Constants.ADDRESS, address);
            i.putExtra("lati", String.valueOf(lat));
            i.putExtra("longi", String.valueOf(lng));
            setResult(RESULT_OK, i);
            finish();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    private void gettingLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(SelectGoogleAddress.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                currentLat = gps.getLatitude();
                currentLng = gps.getLongitude();
            } catch (NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } else {
            gps.showSettingsAlert();
        }

    }


}
