package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.utils.RoundedImageView;

public class ProfileActivity extends BaseActivity {
    private static final String MY_PREFS_NAME = "Fooddelivery";
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.img12)
    ImageView img12;
    @BindView(R.id.img_user)
    RoundedImageView imgUser;
    @BindView(R.id.rel_image)
    RelativeLayout relImage;
    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;
    @BindView(R.id.input_email)
    EditText inputEmail;
    Context mContext;

    @Override
    protected int getContentResId() {
        return R.layout.activity_profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mContext = this;
        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Profile");
        } else {
            setToolbarWithBackButton("Perfil");
        }

        //  setToolbarWithBackButton(getApplication().getString(R.string.txt_profile));
        initView();
    }

    private void initView() {
        Log.d("profileimg", "" + getString(R.string.link) + getString(R.string.imagepath) + UserDataHelper.getInstance().getData().get(0).getImage());
        inputName.setText(UserDataHelper.getInstance().getData().get(0).getFullname());
        inputEmail.setText(UserDataHelper.getInstance().getData().get(0).getEmail());
        try {
            Picasso.with(getApplicationContext())
                    .load(getString(R.string.link) + getString(R.string.imagepath) + UserDataHelper.getInstance().getData().get(0).getImage())
                    .into(imgUser);
            Picasso.with(getApplicationContext())
                    .load(getString(R.string.link) + getString(R.string.imagepath) + UserDataHelper.getInstance().getData().get(0).getImage())
                    .into(img12);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, MainActivity.class));
        finish();
    }
}