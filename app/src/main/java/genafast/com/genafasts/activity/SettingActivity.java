package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.SettingAdapter;
import genafast.com.genafasts.listeners.CustomButtonListener_new;
import genafast.com.genafasts.model.SettingModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import genafast.com.genafasts.utils.SavedData;

public class SettingActivity extends AppCompatActivity implements CustomButtonListener_new {

    private final int defaultRadius = 100;
    private final int noRadius = 100000;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.Switch_radius_onoff)
    SwitchCompat SwitchRadiusOnoff;
    @BindView(R.id.rl_distance_on_title)
    RelativeLayout rlDistanceOnTitle;
    @BindView(R.id.tvDistance)
    TextView tvDistance;
    @BindView(R.id.tvRadius)
    TextView tvRadius;
    @BindView(R.id.rl_distancetitle)
    RelativeLayout rlDistancetitle;
    @BindView(R.id.tvRadiusseekbar)
    TextView tvRadiusseekbar;
    @BindView(R.id.SeekBar)
    android.widget.SeekBar SeekBar;
    @BindView(R.id.rl_distance)
    RelativeLayout rlDistance;
    @BindView(R.id.tvCurrentlocation)
    TextView tvCurrentlocation;
    @BindView(R.id.txt_city)
    TextView txtCity;
    @BindView(R.id.relativeLayout4)
    RelativeLayout relativeLayout4;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private int radius = 3;
    private String key;
    private ArrayList<SettingModel> arrayListCity = new ArrayList<>();
    private SettingAdapter settingAdapter;



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.e("Base Activity ====", "attachBaseContext");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(),Constants.LANGUAGE));

        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        Toolbar mToolbar = findViewById(R.id.tool_bar);
        //  setToolbarWithBackButton(getApplication().getString(R.string.txt_settings));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            ((TextView)mToolbar.findViewById(R.id.toolbar_title)).setText("Setting");
        }else{
            ((TextView)mToolbar.findViewById(R.id.toolbar_title)).setText("Ajustes");
        }

        //getSupportActionBar().setTitle(getApplication().getString(R.string.txt_settings));
        if (SavedData.getRadius() != null) {
            radius = Integer.parseInt(SavedData.getRadius());
        }

        getIntents();

        initView();
    }

    private void getIntents() {
        key = getIntent().getStringExtra("key");
    }

    private void initView() {

        callApiCity();

        if (radius == noRadius) {
            SeekBar.setProgress(defaultRadius);
            tvRadius.setText(defaultRadius + " KM");
            disableRadiusLayout();
        } else {
            SeekBar.setProgress(radius);
            tvRadius.setText(radius + " KM");
            SwitchRadiusOnoff.setChecked(true);
        }

        SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                tvRadius.setText(progress + " KM");
                radius = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                SavedData.saveRadius(String.valueOf(radius));
            }
        });

        SwitchRadiusOnoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SeekBar.setEnabled(true);
                    AlphaAnimation alpha = new AlphaAnimation(1.0F, 1.0F);
                    alpha.setDuration(0); // Make animation instant
                    alpha.setFillAfter(true); // Tell it to persist after the animation ends
                    rlDistance.startAnimation(alpha);
                    SavedData.saveRadius(String.valueOf(defaultRadius));
                } else {
                    disableRadiusLayout();
                }
            }
        });
    }

    private void callApiCity() {

        new JSONParser(SettingActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_city(

        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.E("cities response : " + response);

                try {
                    SettingModel temp;

                    JSONObject jo_response = new JSONObject(response);

                    arrayListCity = new ArrayList<>();
                    JSONArray ja_city = jo_response.getJSONArray("city");
                    for (int i = 0; i < ja_city.length(); i++) {
                        temp = new SettingModel();
                        JSONObject cityname = ja_city.getJSONObject(i);
                        String city = cityname.getString("city_name");
                        temp.setName(city);
                        temp.setId(String.valueOf(i));
                        arrayListCity.add(temp);
                    }
                    recyclerView.setLayoutManager(new LinearLayoutManager(SettingActivity.this));
                    settingAdapter = new SettingAdapter(SettingActivity.this, arrayListCity);
                    recyclerView.setAdapter(settingAdapter);
                    settingAdapter.setCustomButtonListener_new(SettingActivity.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void disableRadiusLayout() {

        AlphaAnimation alpha = new AlphaAnimation(0.3F, 0.3F);
        alpha.setDuration(0); // Make animation instant
        alpha.setFillAfter(true); // Tell it to persist after the animation ends
        rlDistance.startAnimation(alpha);

        int radius1;
        radius1 = defaultRadius;
        SeekBar.setProgress(radius1);
        SeekBar.setEnabled(false);
        tvRadius.setText(radius1 + " " + "KM");
        SavedData.saveRadius(String.valueOf(noRadius));
    }

    @Override
    public void onButtonClick(int position, String buttonText) {
        if (buttonText.equals("city_clicked")) {
            Utility.setSharedPreference(getApplication(), Constants.CITY_NAME, arrayListCity.get(position).getName());
            //SavedData.saveCityName(arrayListCity.get(position).getName());
            settingAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplication(), MainActivity.class));
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}