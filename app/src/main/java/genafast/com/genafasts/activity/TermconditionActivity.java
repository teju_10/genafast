package genafast.com.genafasts.activity;
import android.os.Bundle;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;

public class TermconditionActivity extends BaseActivity {
    @BindView(R.id.web)
    WebView web;
    @Override
    protected int getContentResId() {
        return R.layout.activity_termcondition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Terms and Conditions");
        } else {
            setToolbarWithBackButton("Términos y Condiciones");
        }

     //   setToolbarWithBackButton(getApplication().getString(R.string.terms_and_conditions));
        callApiTermscondition();
    }
    private void callApiTermscondition() {
        web.loadUrl("file:///android_asset/" + getString(R.string.terms_condition_filename));
    }
}