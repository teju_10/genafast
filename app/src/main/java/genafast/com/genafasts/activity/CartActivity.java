package genafast.com.genafasts.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.ListViewHolderCart;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.listeners.CustomButtonListener_new;

import static genafast.com.genafasts.activity.MyOrderActivity.mContext;

public class CartActivity extends BaseActivity implements CustomButtonListener_new {
    private static final String MyPREFERENCES = "Fooddelivery";
    private static ArrayList<CartDataModel> cartlist;
    ImageButton ib_back;
    ListView list_cart;
    private ProgressDialog progressDialog;
    private String menuid;
    private String s1;
    private float total = 0;
    private TextView txt_finalans;
    private int quantity;
    private cartadapter1 adapter;


    @Override
    protected int getContentResId() {
        return R.layout.activity_cart;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Cart");
        }else{
            setToolbarWithBackButton("Carro");
        }
       // setToolbarWithBackButton(getApplicationContext().getString(R.string.txt_cart));

        getData();
    }

    private void getData() {

        list_cart = findViewById(R.id.list_cart);
        txt_finalans = findViewById(R.id.txt_finalans);

        Log.e("Tejas  ", "CartDataHelper.getInstance().getUserDataModel() : : " + CartDataHelper.getInstance().getUserDataModel().size());

        cartlist = new ArrayList<>();
        cartlist = CartDataHelper.getInstance().getUserDataModel();

        Log.e("Tejas ", "cartlist : : " + cartlist.size());

        if (cartlist.size() == 0) {
            Utility.ShowToastMessage(getApplicationContext(), getString(R.string.norecord));
        } else {
            Button btncheckout = findViewById(R.id.btn_checkout);
            btncheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserDataHelper.getInstance().getData().size() > 0) {
                        if (CartDataHelper.getInstance().getUserDataModel().size() > 0) {
                            if (Utility.getSharedPreferences(mContext,Constants.RESTAURANT_ID).equals("")
                                    && Utility.getSharedPreferences(mContext,Constants.RESTAURANT_ID) == null) {
                            } else {
                                Intent iv = new Intent(CartActivity.this, AddressActivity.class);
                                iv.putExtra("order_price", "" + s1);
                                startActivity(iv);
                            }
                        } else {
                            Utility.ShowToastMessage(mContext, getApplication().getString(R.string.str_please_add_item));
                        }
                    } else {
                        Intent iv = new Intent(CartActivity.this, LoginActivity.class);
                        iv.putExtra("key", "PlaceOrder");
                        iv.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(iv);
                        Utility.ShowToastMessage(mContext, mContext.getString(R.string.loginmsg));
                    }
                }
            });
            adapter = new cartadapter1(CartActivity.this, cartlist, Utility.getSharedPreferences(mContext,Constants.RESTAURANT_ID), quantity);
            list_cart.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            adapter.setCustomeButtonListner(this);
            list_cart.getAdapter().getCount();
            String totalcartitem = String.valueOf(list_cart.getAdapter().getCount());
            if (totalcartitem.equals("0")) {
                btncheckout.setEnabled(false);
                Utility.ShowToastMessage(getApplicationContext(), mContext.getResources().getString(R.string.cart_empty));
            } else {
                btncheckout.setEnabled(true);
            }
            list_cart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                }
            });

            for (int i = 0; i < cartlist.size(); i++) {
                total = total + Float.parseFloat(cartlist.get(i).getTotalprice());
            }

            double roundOff = (double) Math.round(total * 100) / 100;
            txt_finalans.setText(getString(R.string.currency) + " " + roundOff);
        }
    }

    private boolean checkingSignIn() {
        //getting shared preference
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        Log.e("user", "" + prefs.getString("userid", null));
        // check user is created or not
        // if user is already logged in
        if (prefs.getString("userid", null) != null) {
            String userid = prefs.getString("userid", null);
            return !userid.equals("delete");
        } else {
            return false;
        }
    }

    @Override
    public void onButtonClick(int position, String buttonText) {
        ArrayList<CartDataModel> cartDataModelArrayList = CartDataHelper.getInstance().getUserDataModel();
        if (buttonText.equals("btn_plus")) {
            int quantity = Integer.parseInt(CartDataHelper.getInstance().getUserDataModel().get(position).getQuantity()) + 1;

            CartDataModel cartDataModel = new CartDataModel();
            cartDataModel.setRestaurantId(cartDataModelArrayList.get(position).getRestaurantId());
            cartDataModel.setFoodid(cartDataModelArrayList.get(position).getFoodid());
            cartDataModel.setFoodname(cartDataModelArrayList.get(position).getFoodname());
            cartDataModel.setFooddesc(cartDataModelArrayList.get(position).getFooddesc());
            cartDataModel.setFoodInstrction(cartDataModelArrayList.get(position).getFoodInstrction());
            cartDataModel.setFoodprice(cartDataModelArrayList.get(position).getFoodprice());
            cartDataModel.setTotalprice(String.valueOf(Float.parseFloat(cartDataModelArrayList.get(position).getFoodprice()) * quantity));

            cartDataModel.setQuantity(String.valueOf(quantity));
            CartDataHelper.getInstance().insertUserDataModel(cartDataModel);
            adapter.notifyDataSetChanged();

            adapter = new cartadapter1(CartActivity.this, CartDataHelper.getInstance().getUserDataModel(), menuid, quantity);
            list_cart.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            adapter.setCustomeButtonListner(this);

            total = 0;
            for (int i = 0; i < CartDataHelper.getInstance().getUserDataModel().size(); i++) {
                total = total + Float.parseFloat(CartDataHelper.getInstance().getUserDataModel().get(i).getTotalprice());
            }

            double roundOff = (double) Math.round(total * 100) / 100;
            txt_finalans.setText(getString(R.string.currency)+" "+roundOff);

        } else if (buttonText.equals("btn_minus")) {
            if (Integer.parseInt(CartDataHelper.getInstance().getUserDataModel().get(position).getQuantity()) > 0) {
                int quantity = Integer.parseInt(CartDataHelper.getInstance().getUserDataModel().get(position).getQuantity()) - 1;

                CartDataModel cartDataModel = new CartDataModel();
                cartDataModel.setRestaurantId(cartDataModelArrayList.get(position).getRestaurantId());
                cartDataModel.setFoodid(cartDataModelArrayList.get(position).getFoodid());
                cartDataModel.setFoodname(cartDataModelArrayList.get(position).getFoodname());
                cartDataModel.setFooddesc(cartDataModelArrayList.get(position).getFooddesc());
                cartDataModel.setFoodInstrction(cartDataModelArrayList.get(position).getFoodInstrction());
                cartDataModel.setFoodprice(cartDataModelArrayList.get(position).getFoodprice());
                cartDataModel.setTotalprice(String.valueOf(Float.parseFloat(cartDataModelArrayList.get(position).getFoodprice()) * quantity));
                cartDataModel.setQuantity(String.valueOf(quantity));
                CartDataHelper.getInstance().insertUserDataModel(cartDataModel);
                adapter.notifyDataSetChanged();

                adapter = new cartadapter1(CartActivity.this, CartDataHelper.getInstance().getUserDataModel(), menuid, quantity);
                list_cart.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                adapter.setCustomeButtonListner(this);

                total = 0;
                for (int i = 0; i < CartDataHelper.getInstance().getUserDataModel().size(); i++) {
                    total = total + Float.parseFloat(CartDataHelper.getInstance().getUserDataModel().get(i).getTotalprice());
                }

                double roundOff = (double) Math.round(total * 100) / 100;
                txt_finalans.setText(getString(R.string.currency) + " " + roundOff);

                if (Integer.parseInt(CartDataHelper.getInstance().getUserDataModel().get(position).getQuantity()) == 0) {
                    CartDataHelper.getInstance().deleteSingle(CartDataHelper.getInstance().getUserDataModel().get(position).getFoodid());
                    adapter = new cartadapter1(CartActivity.this, CartDataHelper.getInstance().getUserDataModel(), menuid, quantity);
                    list_cart.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    adapter.setCustomeButtonListner(this);

                    total = 0;
                    for (int i = 0; i < CartDataHelper.getInstance().getUserDataModel().size(); i++) {
                        total = total + Float.parseFloat(CartDataHelper.getInstance().getUserDataModel().get(i).getTotalprice());
                    }

                    double roundOff1 = (double) Math.round(total * 100) / 100;
                    txt_finalans.setText(getString(R.string.currency) + " " + roundOff1);
                }
            }
        }
    }


    class cartadapter1 extends BaseAdapter {
        final ArrayList<Integer> quantity = new ArrayList<>();
        final String menuid1;
        final int quen;
        private final ArrayList<CartDataModel> data1;
        private final Activity activity;
        SQLiteDatabase db;
        Cursor cur = null;
        String menuid321, foodprice321, restcurrency321;
        String foodid, foodname, fooddesc;
        float val1;
        float add, sub;
        private LayoutInflater inflater = null;
        private CustomButtonListener_new customButtonListener = null;

        cartadapter1(Activity a, ArrayList<CartDataModel> str, String menuid, int quantity) {
            activity = a;
            data1 = str;
            menuid1 = menuid;
            quen = quantity;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for (int i = 0; i < data1.size(); i++) {
                this.quantity.add(i);
            }
        }

        void setCustomeButtonListner(CustomButtonListener_new customeButtonListner) {
            this.customButtonListener = customeButtonListner;
        }

        @Override
        public int getCount() {
            return data1.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row;
            final ListViewHolderCart listViewHolder;
            if (convertView == null) {
                row = inflater.inflate(R.layout.item_cart, parent, false);
                listViewHolder = new ListViewHolderCart();
                listViewHolder.txt_name1 = row.findViewById(R.id.txt_name1);
                listViewHolder.txt_desc = row.findViewById(R.id.txt_desc);
                listViewHolder.txt_totalprice = row.findViewById(R.id.txt_totalprice);
                listViewHolder.txt_basic_price = row.findViewById(R.id.txt_basic_price);
                listViewHolder.txt_quantity = row.findViewById(R.id.txt_quantity);

                listViewHolder.btn_plus = row.findViewById(R.id.btn_plus);
                listViewHolder.btn_minus1 = row.findViewById(R.id.btn_minus1);
                listViewHolder.edTextQuantity = row.findViewById(R.id.edTextQuantity);
                listViewHolder.btn_plus.setTag(listViewHolder);
                listViewHolder.btn_minus1.setTag(listViewHolder);
                row.setTag(listViewHolder);
            } else {
                row = convertView;
                listViewHolder = (ListViewHolderCart) row.getTag();
            }

            listViewHolder.txt_name1.setText((data1.get(position).getFoodname()));
            listViewHolder.txt_desc.setText(data1.get(position).getFooddesc());
            listViewHolder.txt_totalprice.setText(data1.get(position).getTotalprice());
            listViewHolder.edTextQuantity.setText(data1.get(position).getQuantity());
            val1 = Float.parseFloat(data1.get(position).getFoodprice());
            listViewHolder.btn_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (customButtonListener != null)
                        customButtonListener.onButtonClick(position, "btn_plus");
                }
            });
            listViewHolder.btn_minus1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (customButtonListener != null)
                        customButtonListener.onButtonClick(position, "btn_minus");
                }
            });
            return row;
        }
    }
}





