package genafast.com.genafasts.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.model.myOrderGetSet;
import genafast.com.genafasts.model.placeordergetset;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

import static genafast.com.genafasts.activity.MainActivity.checkInternet;
import static genafast.com.genafasts.activity.MyOrderActivity.mContext;
import static genafast.com.genafasts.activity.MyOrderActivity.showErrorDialog;

public class PlaceOrder extends BaseActivity implements OnMapReadyCallback,DirectionCallback  {


    public static final int REQUEST_CODE = 1;
    private static final String MyPREFERENCES = "Fooddelivery";
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private static ArrayList<CartDataModel> cartlist;
    @BindView(R.id.tv_shippingPrice)
    TextView tv_shippingPrice;
    @BindView(R.id.tv_totalPrice)
    TextView tv_totalPrice;
    String Error;
    Context mContext;
    Double distance;
    private double latitudecur, longitudecur, destLat, destLng;
    private GoogleMap googleMap;
    private EditText edt_address;
    private ArrayList<placeordergetset> placeorderlist;
    private String status = "";
    private String userid,address,description,allresid;
    private View layout12;
    private EditText edt_note;
    private float total = 0;
    private myOrderGetSet data;
    LatLng origin, destination;
    AlertDialog alert;


    @Override
    protected int getContentResId() {
        return R.layout.activity_place_order;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Add delivery address");
        } else {
            setToolbarWithBackButton("Añadir dirección de entrega");
        }

        //     setToolbarWithBackButton(getApplication().getString(R.string.add_delivery_address));
        mContext = this;
        /*iv.putExtra("lat", model.getLat());
                iv.putExtra("lng", model.getLng());
                iv.putExtra("address", model.getAddress());*/

        try {

            allresid = getIntent().getStringExtra("res_id");
            address = getIntent().getStringExtra("address");
            destLat = Double.valueOf(getIntent().getStringExtra("lat"));
            destLng = Double.valueOf(getIntent().getStringExtra("lng"));
            S.E("PlaceOrder=== RestID" + allresid);
            S.E("PlaceOrder=== ADDRESS" + address);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (checkInternet(PlaceOrder.this)) {

            gettingLocation();

            getSharedPref();

            initialization();


        } else showErrorDialog(PlaceOrder.this);

    }

    private void getSharedPref() {
        SharedPreferences sp = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userid = sp.getString("userid", "");

    }


    private void gettingLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(PlaceOrder.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
            } catch (NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else {

            gps.showSettingsAlert();
        }

    }

    private void initialization() {
        TextView txt_header = findViewById(R.id.txt_title);
        edt_address = findViewById(R.id.edt_address);
        edt_note = findViewById(R.id.edt_note);
        RelativeLayout rel_main = findViewById(R.id.rel_main);
        String o = address;      // In address comma seprstor  data show using this ;
        String[] arr = o.split(" ", 2);
        edt_address.setText(arr[1]);

        Button btn_placeorder = findViewById(R.id.btn_placeorder);

        MapsInitializer.initialize(getApplicationContext());
        initilizeMap();

        btn_placeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getApplication(), CulqiPaymentActivity.class));
//                finish();
                validation();

            }
        });

        placeorderlist = new ArrayList<>();

        //getting Data
        cartlist = new ArrayList<>();
        cartlist = CartDataHelper.getInstance().getUserDataModel();
        System.out.println("CartDATAHelper====== ===" + cartlist.toString());

        origin = new LatLng(latitudecur,longitudecur);
        destination= new LatLng(destLat,destLng);

    }

    private void validation() {
        if (edt_address.getText().toString().trim().isEmpty()) {
            edt_address.setError(mContext.getString(R.string.txt_select_location));
        } else if (edt_note.getText().toString().trim().isEmpty()) {
            edt_note.setError(getApplication().getString(R.string.write_description));
        } else {
            address = address.replace(" ", "%20");
            description = edt_note.getText().toString().replace(" ", "%20");

            // new PostDataAsyncTask().execute();
            showDiag();

        }
    }

    private void initilizeMap() {
        SupportMapFragment supportMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment));
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            // check if map is created successfully or not
            if (googleMap == null) {
                Utility.ShowToastMessage(getApplicationContext(), getString(R.string.no_map));
                return;
            }
            this.googleMap = googleMap;
//            googleMap.setOnMarkerDragListener(this);


            initializeUiSettings();
            initializeMapLocationSettings();
            //  initializeMapTraffic();
            initializeMapType();
            initializeMapViewSettings();

            requestDirection();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            LatLng position = new LatLng(destLat, destLng);
            googleMap.addMarker(new MarkerOptions().position(position).draggable(true).title(address));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15.0f));

        } catch (NullPointerException | NumberFormatException e) {
            // TODO: handle exception
        }

    }

    private void initializeUiSettings() {
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
//        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    private void initializeMapLocationSettings() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    private void initializeMapTraffic() {
        googleMap.setTrafficEnabled(true);
    }

    private void initializeMapType() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    private void initializeMapViewSettings() {
//        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(false);
    }

    private void showDiag() {

        final View dialogView = View.inflate(this, R.layout.dialog_paymenttype, null);

        final Dialog dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        //  this.getWindow().setWindowAnimations(R.anim.paranimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.closeDialogImg);
        final Button btn_pay_cash = (Button) dialog.findViewById(R.id.btn_pay_cash);
        final Button btn_pay_PosMachine = (Button) dialog.findViewById(R.id.btn_pay_PosMachine);
        final Button btn_pay_Both = (Button) dialog.findViewById(R.id.btn_pay_Both);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing())
                    dialog.dismiss();
                else
                    dialog.dismiss();
            }
        });

        btn_pay_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // revealShow(dialogView, true, null);
//                btn_pay_cash.setTextColor(mContext.getResources().getColor(R.color.white));
//                btn_pay_other.setTextColor(mContext.getResources().getColor(R.color.black));
//                btn_pay_cash.setBackgroundColor(mContext.getResources().getColor(R.color.my_statusbar_color));
//                btn_pay_other.setBackground(mContext.getResources().getDrawable(R.drawable.bg_white));

                Intent in = new Intent(mContext, MakePaymentActivity.class);
                in.putExtra(Constants.PAYMENTTYPE, "COD");
                in.putExtra(Constants.RESTAURANT_ID, allresid);
                in.putExtra(Constants.ADDRESS, address);
                in.putExtra(Constants.LATITUDE, String.valueOf(destLat));
                in.putExtra(Constants.LONGITUDE, String.valueOf(destLng));
                in.putExtra(Constants.NOTES, description);
                if(!tv_shippingPrice.getText().toString().trim().isEmpty()){
                    in.putExtra(Constants.SHIPPINGPRICE, tv_shippingPrice.getText().toString().trim().
                            replace("S/ ",""));
                }
                startActivity(in);

//                startActivityForResult(new Intent(mContext, MakePaymentActivity.class).
//                        putExtra(Constants.PAYMENTTYPE, "Cash"), REQUEST_CODE);
//                paymentType = "Cash";

            }
        });

        btn_pay_PosMachine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(mContext, MakePaymentActivity.class);
                in.putExtra(Constants.PAYMENTTYPE, "POS");
                in.putExtra(Constants.RESTAURANT_ID, allresid);
                in.putExtra(Constants.ADDRESS, address);
                in.putExtra(Constants.LATITUDE, String.valueOf(latitudecur));
                in.putExtra(Constants.LONGITUDE, String.valueOf(longitudecur));
                in.putExtra(Constants.NOTES, description);
                if(!tv_shippingPrice.getText().toString().trim().isEmpty()){
                    in.putExtra(Constants.SHIPPINGPRICE, tv_shippingPrice.getText().toString().trim().
                            replace("S/ ",""));
                }
                startActivity(in);

//                paymentType = "POS";
//
//                startActivityForResult(new Intent(mContext, MakePaymentActivity.class).
//                        putExtra(Constants.PAYMENTTYPE, "POS"), REQUEST_CODE);

            }
        });


        btn_pay_Both.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(mContext, MakePaymentActivity.class);
                in.putExtra(Constants.PAYMENTTYPE, "COD/POS");
                in.putExtra(Constants.RESTAURANT_ID, allresid);
                in.putExtra(Constants.ADDRESS, address);
                in.putExtra(Constants.LATITUDE, String.valueOf(latitudecur));
                in.putExtra(Constants.LONGITUDE, String.valueOf(longitudecur));
                in.putExtra(Constants.NOTES, description);
                if(!tv_shippingPrice.getText().toString().trim().isEmpty()){
                    in.putExtra(Constants.SHIPPINGPRICE, tv_shippingPrice.getText().toString().trim().
                            replace("S/ ",""));
                }
                startActivity(in);
                //paymentType = "POS";
//
//                startActivityForResult(new Intent(mContext, MakePaymentActivity.class).
//                        putExtra(Constants.PAYMENTTYPE, "Both"), REQUEST_CODE);

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

                String requiredValue = data.getStringExtra("key");
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void requestDirection() {

        GoogleDirection.withServerKey(Constants.MAP_KEY)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(this);

    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (mContext != null) {
            if (direction.isOK()) {
                ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();

                calculateDistance(Double.valueOf(direction.getRouteList().get(0).getLegList().get(0).getDistance().getValue()) / 1000);

            } else {
               tv_shippingPrice.setText("S/ 2.90");
                for (int i = 0; i < cartlist.size(); i++) {
                    total = total + Float.parseFloat(cartlist.get(i).getTotalprice());
                }

                double roundOff = (double) Math.round(total * 100) / 100;
                tv_totalPrice.setText("S/ " + " " + roundOff);

            }
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        distanceAlert(t.getMessage() + "\n" + t.getLocalizedMessage() + "\n");
        //  calculateFare.setVisibility(View.GONE);
        // dismiss();
    }

    public void distanceAlert(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Invalid Distance");
        alertDialog.setMessage(message);
        alertDialog.setCancelable(true);
        Drawable drawable = ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.RED);
        alertDialog.setIcon(drawable);


        alertDialog.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alert.cancel();
            }
        });
        alert = alertDialog.create();
        alert.show();
    }

    public void calculateDistance(Double aDouble) {

        if (aDouble < 3) {
            tv_shippingPrice.setText("S/ 2.90");
        } else if (aDouble > 3 && aDouble < 5) {
             tv_shippingPrice.setText("S/ 5");
        }
         tv_totalPrice.setText(CartDataHelper.getInstance().getUserDataModel().get(0).getTotalprice());
        System.out.println("Distance  ====" + distance);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
