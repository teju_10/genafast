package genafast.com.genafasts.activity;


import android.os.Bundle;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;


public class AboutusActivity extends BaseActivity {


    @BindView(R.id.web)
    WebView web;

    @Override
    protected int getContentResId() {
        return R.layout.activity_aboutus;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        setToolbarWithBackButton(getApplicationContext().getString(R.string.txt_about_us));
        callApiAbout();
    }

    private void callApiAbout() {
        web.loadUrl("file:///android_asset/" + getString(R.string.aboutus_filename));


    }
}
