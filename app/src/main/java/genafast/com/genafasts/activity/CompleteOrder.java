package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.model.orderTimelineGetSet;
import genafast.com.genafasts.timeline.OrderStatus;
import genafast.com.genafasts.timeline.TimeLineAdapter;
import genafast.com.genafasts.timeline.TimeLineModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class CompleteOrder extends AppCompatActivity implements View.OnClickListener {
    private final List<TimeLineModel> mDataList = new ArrayList<>();
    String rejectedOrder;
    Context mContext;
    private RecyclerView mRecyclerView;
    private String orderId;
    private String[] ordertxt;
    private TextView txt_name;
    private TextView txt_contact;
    private TextView txt_address;
    private TextView txt_order_amount;
    private TextView txt_order_EstimatedTime;
    private TextView txt_order_time;
    private ImageView rest_image;
    private ArrayList<orderTimelineGetSet> orderGetSet;
    private orderTimelineGetSet oData;
    private String key = "", strRestLat = "", strRestLng = "", c_lat = "", c_lng = "", driver_id = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.e("CompleteOrder Activity ", "attachBaseContext");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE));

        setContentView(R.layout.activity_complete_order);
        mContext = this;
        Toolbar mToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Order Status");
        } else {
            ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Estado del pedido");
        }


        gettingIntents();
        ordertxt = new String[]{getString(R.string.timelineo1), getString(R.string.timelineo2), getString(R.string.timelineo3), getString(R.string.timelineo4), getString(R.string.timelineo5)};
        rejectedOrder = getString(R.string.timeline6);
        initView();
        getData();

    }

    @Override
    protected void onResume() {
        super.onResume();
//        getData();
    }

    private void gettingIntents() {
        try {
            Intent i = getIntent();
            if (!i.getStringExtra("orderid").equals("")) {
                orderId = i.getStringExtra("orderid");
                key = i.getStringExtra("key");
            } else {
                Bundle b = new Bundle();
                if (!b.equals("")) {
                    b = getIntent().getExtras();
                    orderId = b.getString(Constants.ORDER_ID);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initView() {

        findViewById(R.id.btn_trackorder).setOnClickListener(this);
        //settine time line adapter for order status
        mRecyclerView = findViewById(R.id.time_line);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);

        //initialization
        TextView txt_title = findViewById(R.id.txt_title);

        TextView txt_orderno = findViewById(R.id.txt_orderno);
        txt_orderno.setText("Order No : " + orderId);

        rest_image = findViewById(R.id.image);

        txt_name = findViewById(R.id.txt_name);

        txt_address = findViewById(R.id.txt_address);

        txt_contact = findViewById(R.id.txt_contact);

        TextView txt_order_title = findViewById(R.id.txt_order_title);

        TextView txt_order = findViewById(R.id.txt_order);
        txt_order.setText(orderId);

        TextView txt_order_amount_title = findViewById(R.id.txt_order_amount_title);

        txt_order_amount = findViewById(R.id.txt_order_amount);

        TextView txt_order_estimatedtime_title = findViewById(R.id.txt_order_estimatedtime_title);

        txt_order_EstimatedTime = findViewById(R.id.txt_order_estimatedtime);

        TextView txt_order_time_title = findViewById(R.id.txt_order_time_title);

        txt_order_time = findViewById(R.id.txt_order_time);

        RelativeLayout rel_order = findViewById(R.id.rel_order);
        rel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CompleteOrder.this, OrderSpecification.class);
                i.putExtra("OrderId", orderId);
                startActivity(i);
            }
        });

    }


    private void updateTimeline() {
        if (Objects.equals(orderGetSet.get(0).getStatus(), "Activate")) {
            mDataList.add(new TimeLineModel(ordertxt[1], orderGetSet.get(0).getOrder_date_time(), OrderStatus.ACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[2], orderGetSet.get(0).getOrder_date_time(), OrderStatus.ACTIVE));

            if (Objects.equals(orderGetSet.get(1).getStatus(), "Activate")) {
                mDataList.add(new TimeLineModel(ordertxt[3], orderGetSet.get(1).getOrder_date_time(), OrderStatus.ACTIVE));

                if (Objects.equals(orderGetSet.get(2).getStatus(), "Activate")) {
                        mDataList.add(new TimeLineModel(ordertxt[4], orderGetSet.get(2).getOrder_date_time(), OrderStatus.COMPLETED));
                        findViewById(R.id.btn_trackorder).setVisibility(View.GONE);
                } else {
                    //  btn_placeorder.setVisibility(View.GONE);
                    mDataList.add(new TimeLineModel(ordertxt[4], "", OrderStatus.INACTIVE));
                }
            } else {
                mDataList.add(new TimeLineModel(ordertxt[3], "", OrderStatus.INACTIVE));
                mDataList.add(new TimeLineModel(ordertxt[4], "", OrderStatus.INACTIVE));
            }
        } else {
            mDataList.add(new TimeLineModel(ordertxt[1], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[2], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[3], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[4], "", OrderStatus.INACTIVE));
        }

        boolean mWithLinePadding = false;
        TimeLineAdapter mTimeLineAdapter = new TimeLineAdapter(mDataList, mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);

    }

    private void updateRejectTimeLine() {

        if (Objects.equals(orderGetSet.get(0).getStatus(), "Activate")) {
            mDataList.add(new TimeLineModel(ordertxt[1], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[2], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(ordertxt[3], "", OrderStatus.INACTIVE));
            mDataList.add(new TimeLineModel(rejectedOrder, orderGetSet.get(0).getOrder_date_time(), OrderStatus.COMPLETED));
        }
        boolean mWithLinePadding = false;
        TimeLineAdapter mTimeLineAdapter = new TimeLineAdapter(mDataList, mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    private void getData() {
        S.E("ORDER ID=== CompleteOrderActivity" + orderId);
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).order_details(
                orderId
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("get order details response : " + response);
                try {

                    //getting the whole json object from the response
                    JSONObject obj = new JSONObject(response);
                    String status = obj.getString("success");
                    if (Objects.equals(status, "1")) {
                        orderGetSet = new ArrayList<>();
                        orderGetSet.clear();
                        JSONArray ja_orderDetail = obj.getJSONArray("order_details");
                        JSONObject jo_data = ja_orderDetail.getJSONObject(0);
                        String txt_restName = jo_data.getString("restaurant_name");
                        String txt_restaurant_address = jo_data.getString("restaurant_address");
                        String txt_restaurant_contact = jo_data.getString("restaurant_contact");
                        String txt_order_amount = jo_data.getString("order_amount");
                        String txt_order_time = jo_data.getString("order_time");
                        String txt_delivery_time = jo_data.getString("delivery_time");
                        String txt_order_id = jo_data.getString("order_id");
                        String image = jo_data.getString("restaurant_image");

                        strRestLat = jo_data.getString("lat");
                        strRestLng = jo_data.getString("long");
                        c_lat = jo_data.getString("clat");
                        c_lng = jo_data.getString("clong");
                        driver_id = jo_data.getString("driver_id");


                        c_lng = jo_data.getString("clong");
                        updateUI(txt_restName, txt_restaurant_address, txt_restaurant_contact, txt_order_amount, txt_order_time, txt_delivery_time, txt_order_id, image);
                        if (jo_data.getString("reject_date_time").equals("null")) {
                            oData = new orderTimelineGetSet();
                            oData.setOrder_date_time(jo_data.getString("order_verified_date"));
                            oData.setStatus(jo_data.getString("order_verified"));
                            orderGetSet.add(oData);
                            oData = new orderTimelineGetSet();
                            oData.setOrder_date_time(jo_data.getString("delivery_date_time"));
                            oData.setStatus(jo_data.getString("delivery_status"));
                            orderGetSet.add(oData);
                            oData = new orderTimelineGetSet();
                            oData.setOrder_date_time(jo_data.getString("delivered_date_time"));
                            oData.setStatus(jo_data.getString("delivered_status"));
                            orderGetSet.add(oData);
                            updateTimeline();
                        } else {
                            oData = new orderTimelineGetSet();
                            oData.setOrder_date_time(jo_data.getString("reject_date_time"));
                            oData.setStatus(jo_data.getString("reject_status"));
                            orderGetSet.add(oData);
                            updateRejectTimeLine();
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI(String txt_restName, String txt_restaurant_address, String txt_restaurant_contact, String txt_order_amo, String txt_order_tim, String txt_delivery_time, String txt_order_id, String image) {
        txt_name.setText(txt_restName);
        txt_address.setText(txt_restaurant_address);
        txt_contact.setText(txt_restaurant_contact);
        txt_order_EstimatedTime.setText(txt_delivery_time);
        txt_order_amount.setText(getString(R.string.currency) + " " + Utility.rounddecimalFormate(Float.valueOf(txt_order_amo)));
        txt_order_time.setText(txt_order_tim);
        Picasso.with(CompleteOrder.this).load(getResources().getString(R.string.link) + getString(R.string.imagepath) + image).into(rest_image);
        mDataList.add(new TimeLineModel(ordertxt[0], txt_order_tim, OrderStatus.ACTIVE));
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(CompleteOrder.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_trackorder:
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr="+"22.724355"+","+"75.8838944"+"&daddr="+"22.7532848"+","+"75.8936962"));
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.addCategory(Intent.CATEGORY_LAUNCHER );
//                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//                startActivity(intent);
                if (driver_id.equals(null) && driver_id == null && driver_id.equals("null")) {
                    Utility.Alert(mContext, "Alert", mContext.getResources().getString(R.string.ordernotaccept));
                } else if (!driver_id.equals("null") && driver_id != null) {
                    Intent i = new Intent(getApplication(), TrackOrderActivity.class);
                    i.putExtra(Constants.LATITUDE, strRestLat);
                    i.putExtra(Constants.LONGITUDE, strRestLng);
                    i.putExtra(Constants.CUSTOMER_LATITUDE, c_lat);
                    i.putExtra(Constants.CUSTOMER_LONGITUDE, c_lng);
                    i.putExtra(Constants.DRIVER_ID, driver_id);
                    startActivity(i);
                    finish();
                } else {
                    Utility.Alert(mContext, "Alert", mContext.getResources().getString(R.string.ordernotaccept));
                }

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

