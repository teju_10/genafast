package genafast.com.genafasts.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.culquiPayment.Card;
import genafast.com.genafasts.culquiPayment.Token;
import genafast.com.genafasts.culquiPayment.TokenCallback;
import genafast.com.genafasts.culquiPayment.Validation;

public class CulqiPaymentActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.txt_cardnumber)
    EditText txt_cardnumber;
    @BindView(R.id.txt_month)
    EditText txt_month;
    @BindView(R.id.txt_year)
    EditText txt_year;
    @BindView(R.id.txt_cvv)
    EditText txt_cvv;
    @BindView(R.id.btn_pay)
    Button btn_pay;
    ProgressDialog progress;

    Context mContext;
    Validation validation;
    TextView kind_card;

    @Override
    protected int getContentResId() {
        return R.layout.activity_payment;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mContext = this;

        init();
        cardtextChangeListner();
        yeartextChangeListner();
        monthtextChangeListner();


    }


    private void init() {
        validation = new Validation();
        kind_card = findViewById(R.id.kind_card);
        progress = new ProgressDialog(this);
        progress.setMessage("Validating card information");
        //progress.setMessage("Validando informacion de la tarjeta");
        progress.setCancelable(false);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        txt_cvv.setEnabled(false);

        btn_pay.setOnClickListener(this);
    }

    private void cardtextChangeListner() {
        txt_cardnumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    txt_cvv.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = txt_cardnumber.getText().toString();
                if (s.length() == 0) {
                    txt_cardnumber.setBackgroundResource(R.drawable.border_error);
                }

                if (validation.luhn(text)) {
                    txt_cardnumber.setBackgroundResource(R.drawable.border_sucess);
                } else {
                    txt_cardnumber.setBackgroundResource(R.drawable.border_error);
                }

                int cvv = validation.bin(text, kind_card);
                //int cvv = 4;
                if (cvv > 0) {
                    txt_cvv.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cvv)});
                    txt_cvv.setEnabled(true);
                } else {
                    txt_cvv.setEnabled(false);
                    txt_cvv.setText("");
                }
            }
        });
    }

    private void yeartextChangeListner() {
        txt_year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = txt_year.getText().toString();
                if (validation.year(text)) {
                    txt_year.setBackgroundResource(R.drawable.border_error);
                } else {
                    txt_year.setBackgroundResource(R.drawable.border_sucess);
                }
            }
        });
    }

    private void monthtextChangeListner() {
        txt_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = txt_month.getText().toString();
                if (validation.month(text)) {
                    txt_month.setBackgroundResource(R.drawable.border_error);
                } else {
                    txt_month.setBackgroundResource(R.drawable.border_sucess);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void getPaymentToken() {
        progress.show();
        int month = Integer.parseInt(txt_month.getText().toString());
        int year = Integer.parseInt(txt_year.getText().toString());
        Card card = new Card(txt_cardnumber.getText().toString(), txt_cvv.getText().toString(), month, year, "wm@wm.com");

        Token token = new Token("pk_test_E9eWnTieVjUw3CVd");
        token.createToken(mContext, card, new TokenCallback() {
            @Override
            public void onSuccess(JSONObject token) {
                try {
                    progress.hide();
                    S.E("Payment Token===== " + token.get("id").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception error) {
                progress.hide();
                S.E("Payment Token Error=== " + error);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pay:
                if (txt_cardnumber.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please add card number", Toast.LENGTH_LONG).show();
                } else if (txt_month.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please add month", Toast.LENGTH_LONG).show();
                } else if (txt_year.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please add validate year", Toast.LENGTH_LONG).show();
                } else if (txt_cvv.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please add cvv", Toast.LENGTH_LONG).show();
                } else {
                    getPaymentToken();
                }

                break;
        }
    }
}
