package genafast.com.genafasts.activity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import genafast.com.genafasts.R;


import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.DataParser;

import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import genafast.com.genafasts.utils.LocationUtil;

import static genafast.com.genafasts.utils.MapUtils.getBearing;


//https://github.com/suryamolly/Wonders-Work/blob/master/app/src/main/java/com/surya/
// smoothmarkermovement/MainActivity.java

//https://duncan99.wordpress.com/2013/02/28/google-maps-api-polylines-and-events/
public class TrackOrderActivity extends BaseActivity implements OnMapReadyCallback, LocationUtil.GetLocationListener {

    // Give your Server URL here >> where you get car location update
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;
    GoogleMap googleMap;
    Button button2;
    List<LatLng> polyLineList;
    double latitude = 0.0;
    double longitude = 0.0;
    Context mContext;
    ArrayList<LatLng> MarkerPoints;
    ArrayList<LatLng> polylines = new ArrayList<LatLng>();
    boolean firstCall = true;
    private boolean isDeninedRTPs = true;       // initially true to prevent anim(2)
    private boolean showRationaleRTPs = false;
    private LocationUtil locationUtilObj;
    private Projection projection;
    private PolylineOptions polylineOptions;
    private LocationManager mLocationManager;
    private Polyline greyPolyLine;
    private SupportMapFragment mapFragment;
    private Handler handler;
    private Marker carMarker;
    private int index;
    private int next;
    private Handler mHandler = new Handler();
    private LatLng startPosition;
    private LatLng endPosition;
    private float v;
    private double lat, lng;
    private String TAG = "TrackOrderActivity";

//
//    Runnable staticCarRunnable = new Runnable() {
//        @Override
//        public void run() {
//            Log.i(TAG, "staticCarRunnable handler called...");
//            if (index < (polyLineList.size() - 1)) {
//                index++;
//                next = index + 1;
//            } else {
//                index = -1;
//                next = 1;
//                stopRepeatingTask();
//                return;
//            }
//
//            if (index < (polyLineList.size() - 1)) {
////                startPosition = polyLineList.get(index);
//                startPosition = carMarker.getPosition();
//                endPosition = polyLineList.get(next);
//            }
//
//            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
//            valueAnimator.setDuration(3000);
//            valueAnimator.setInterpolator(new LinearInterpolator());
//            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                @Override
//                public void onAnimationUpdate(ValueAnimator valueAnimator) {
//
//                    v = valueAnimator.getAnimatedFraction();
//                    lng = v * endPosition.longitude + (1 - v)
//                            * startPosition.longitude;
//                    lat = v * endPosition.latitude + (1 - v)
//                            * startPosition.latitude;
//                    LatLng newPos = new LatLng(lat, lng);
//                    carMarker.setPosition(newPos);
//                    carMarker.setAnchor(0.5f, 0.5f);
//                    carMarker.setRotation(getBearing(startPosition, newPos));
//                    googleMap.moveCamera(CameraUpdateFactory
//                            .newCameraPosition
//                                    (new CameraPosition.Builder()
//                                            .target(newPos)
//                                            .zoom(15.5f)
//                                            .build()));
//
//
//                }
//            });
//            valueAnimator.start();
//            handler.postDelayed(this, 5000);
//
//        }
//    };
    private String strRestLat = "", strRestLng = "", C_Lat = "", C_Lng = "",driver_id = "";
    private Polyline finalPolyLine;
    private Timer timer;
    private TimerTask timerTask;

    @Override
    protected int getContentResId() {
        return R.layout.activity_track_order;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        MarkerPoints = new ArrayList<>();
        handler = new Handler();


        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                strRestLat = extras.getString(Constants.LATITUDE);
                strRestLng = extras.getString(Constants.LONGITUDE);
                C_Lat = extras.getString(Constants.CUSTOMER_LATITUDE);
                C_Lng = extras.getString(Constants.CUSTOMER_LONGITUDE);
                driver_id = extras.getString(Constants.DRIVER_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        startTimer();

    }

    public void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        S.E("location call ");
                        apiGetPolylines();

                    }
                });

            }
        };
        timer.schedule(timerTask, 10000, 10000);
    }

//    private void startCarAnimation(Double latitude, Double longitude) {
//        LatLng latLng = new LatLng(latitude, longitude);
//
//        carMarker = googleMap.addMarker(new MarkerOptions().position(latLng).
//                flat(true).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)));
//
//
//        index = -1;
//        next = 1;
//        handler.postDelayed(staticCarRunnable, 3000);
//    }
//
//    void stopRepeatingTask() {
//
//        if (staticCarRunnable != null) {
//            handler.removeCallbacks(staticCarRunnable);
//        }
//    }

    @Override
    protected void onPause() {
        super.onPause();
        //stopRepeatingTask();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            } else {
                checkAndRequestRunTimePermissions();
            }
        } else {
            getCurrentLocation();
            googleMap.setMyLocationEnabled(true);
        }


        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setTrafficEnabled(false);
        googleMap.setIndoorEnabled(false);
        googleMap.setBuildingsEnabled(false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LatLng source = new LatLng(Double.parseDouble(C_Lat), Double.parseDouble(C_Lng));
        LatLng desti = new LatLng(Double.parseDouble(strRestLat), Double.parseDouble(strRestLng));

        googleMap.addMarker(new MarkerOptions().position(source).title("My Location")
                .icon(bitmapDescriptorFromVector(R.drawable.ic_location_pin)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 15.0f));

        googleMap.addMarker(new MarkerOptions().position(desti).title("Restaurant Location")
                .icon(bitmapDescriptorFromVector(R.drawable.ic_dish)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(desti, 15.0f));

        String url = getUrl(source, desti);
        Log.d("onMapClick", url.toString());
        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);

    }

    private void checkAndRequestRunTimePermissions() {
        if (Build.VERSION.SDK_INT >= 23) {

            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_CODE_PERMISSION_MULTIPLE);

            }
        }

        onRunTimePermissionGranted();
    }


    private void onRunTimePermissionGranted() {
        isDeninedRTPs = false;
        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_MULTIPLE) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isDeninedRTPs = true;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                        }

                        break;
                    }

                }
                onRunTimePermissionDenied();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /******************************************************/

    private void onRunTimePermissionDenied() {
        if (isDeninedRTPs) {
            if (!showRationaleRTPs) {
                //goToSettings();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_CODE_PERMISSION_MULTIPLE);
                }
            }
        } else {
            onRunTimePermissionGranted();
        }
    }


    private void getCurrentLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(TrackOrderActivity.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            } catch (NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else {

            gps.showSettingsAlert();
        }

    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&key=" + Constants.MAP_KEY;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void location_Error(String error) {

    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void apiGetPolylines() {

googleMap.clear();
        S.E("userIdToSend : " + UserDataHelper.getInstance().getData().get(0).getUserid());
        new JSONParser(this).parseRetrofitRequestWithautProgress(ApiClient.getClient().create(ApiInterface.class).getLatLong("getOrderTracking",
                 //UserDataHelper.getInstance().getData().get(0).getUserid()
                driver_id

        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("get lat long resposne : " + response);
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("Success")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("response");

                        S.E("polylines size : " + polylines.size());
                        polylines.clear();
                        if (polylines.size() > 0) {

                            for (int i = polylines.size(); i < jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                polylines.add(new LatLng(Double.parseDouble(json.getString("lat")), Double.parseDouble(json.getString("long"))));
                            }
                        } else {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject json = jsonArray.getJSONObject(i);
                                polylines.add(new LatLng(Double.parseDouble(json.getString("lat")), Double.parseDouble(json.getString("long"))));
                            }
                        }

                        finalPolyLine = googleMap.addPolyline(new PolylineOptions().addAll(polylines).width(5.0f).color(Color.BLACK));
                        S.E("Updted Lat  " + polylines.get(0).latitude + " " + polylines.get(0).longitude);
                        LatLng start = new LatLng(polylines.get(0).latitude, polylines.get(0).longitude);
                        LatLng end = new LatLng(polylines.get(polylines.size() - 1).latitude, polylines.get(polylines.size() - 1).longitude);

                        Marker currentMarker = null;
                        if (currentMarker!=null) {
                            currentMarker.remove();
                            currentMarker=null;
                        }
                        if (currentMarker==null) {
                            currentMarker = googleMap.addMarker(new MarkerOptions().position(start).
                                    icon(bitmapDescriptorFromVector(R.drawable.ic_location)));
                        }

                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                        if (firstCall) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 15));
                            firstCall = false;
                        }
                        /*PolylineOptions opts = new PolylineOptions().addAll(polylines).color(Color.RED).width(5);
                        mMap.addPolyline(opts);*/
                    } else {
                        //  finalPolyLine.remove();
                        S.T(TrackOrderActivity.this, "Data not available!");
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private BitmapDescriptor bitmapDescriptorFromVector(int vectorResId) {
        int height = 100;
        int width = 100;
        Drawable vectorDrawable = ContextCompat.getDrawable(getApplication(), vectorResId);
        vectorDrawable.setBounds(0, 0, width, height);
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, MainActivity.class));
        finish();
        super.onBackPressed();
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray routes = jsonObject.getJSONArray("routes");

                JSONObject routes1 = routes.getJSONObject(0);

                JSONArray legs = routes1.getJSONArray("legs");

                JSONObject legs1 = legs.getJSONObject(0);

                JSONObject distance = legs1.getJSONObject("distance");

                JSONObject duration = legs1.getJSONObject("duration");
                S.E("distanceText  " + distance.getString("text"));
                S.E("durationText   " + duration.getString("text"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
            ParserTask parserTask = new ParserTask();

            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);
                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                googleMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }


}
