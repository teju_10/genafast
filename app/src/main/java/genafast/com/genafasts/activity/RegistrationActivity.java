package genafast.com.genafasts.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rilixtech.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.UserAccount;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.UserDataModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RegistrationActivity extends BaseActivity {

    private static final int RESULT_cam_IMAGE = 2;
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private static final String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.imgprofile)
    ImageView imgprofile;
    @BindView(R.id.btncamera)
    Button btncamera;
    @BindView(R.id.btngallery)
    Button btngallery;
    @BindView(R.id.rel_camara)
    LinearLayout relCamara;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etname)
    EditText etname;
    @BindView(R.id.etemail)
    EditText etemail;
    @BindView(R.id.etpassword)
    EditText etpassword;
    @BindView(R.id.chkbox)
    CheckBox chkboxPrivacy;
    @BindView(R.id.tvtermscondition)
    TextView tvtermscondition;
    @BindView(R.id.tvsignup)
    Button tvsignup;
    @BindView(R.id.imgClear)
    ImageView imgClear;
    @BindView(R.id.imgLayout)
    RelativeLayout imgLayout;
    UserAccount userAccount;
    Context mContext;
    private ImageView img_user;
    private String picturepath = "";
    private String key, res_id = "";
    private String strSuccess, privacyChk = "";

    private static void verifyStoragePermissions(final Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            new AlertDialog.Builder(activity)
                    .setTitle(R.string.msg)
                    .setMessage(R.string.mssg)
                    .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(activity,
                                    PERMISSIONS_STORAGE,
                                    REQUEST_EXTERNAL_STORAGE);
                        }
                    })
                    .create()
                    .show();
        } else {
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected int getContentResId() {
        return R.layout.activity_register;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Utility.ChangeLang(getApplication(), Utility.getLanSharedPreferences(getApplication(), Constants.LANGUAGE));

        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

        ButterKnife.bind(this);
        mContext = this;

        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Register");
        } else {
            setToolbarWithBackButton("Registro");
        }

        //  setToolbarWithBackButton(getApplication().getString(R.string.register));

        userAccount = new UserAccount(getApplication(), etemail, etpassword);

        gettingIntents();

        tvsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userAccount.isEmpty(etname, etemail, etpassword)) {
                    if (userAccount.isEmailValid(etemail)) {
                        if (userAccount.isPasswordValid(etpassword)) {
                            if (picturepath.equals("")) {
                                Utility.Alert(mContext, "Alert", "Please select profile");
                            } else if (privacyChk.equals("")) {
                                Utility.Alert(mContext, "Alert", "Please Check Privacy Policy");
                            } else {
                                callApiForSignUp();
                            }
                        }
                    }
                }
            }
        });

        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picturepath = "";
                imgprofile.setImageResource(R.drawable.ic_user);
            }
        });

        btncamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permission = ActivityCompat.checkSelfPermission(RegistrationActivity.this, Manifest.permission.CAMERA);

                if (permission != PackageManager.PERMISSION_GRANTED) {
                    // We don't have permission so prompt the user
                    verifyStoragePermissions(RegistrationActivity.this);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    startActivityForResult(intent, RESULT_cam_IMAGE);
                }
            }
        });

        btngallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permission = ActivityCompat.checkSelfPermission(RegistrationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    verifyStoragePermissions(RegistrationActivity.this);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
            }
        });


        chkboxPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    privacyChk = "1";
                }else{
                    privacyChk = "";
                }
            }
        });


    }

    private void gettingIntents() {
        //getting intent
        if (Utility.getSharedPreferences(getApplication(), Constants.DEVICE_ID).equals("")) {
            String fcm_id = FirebaseInstanceId.getInstance().getToken();
            Utility.setSharedPreference(getApplication(), Constants.DEVICE_ID, fcm_id);
        }
        Intent iv = getIntent();
        res_id = getIntent().getStringExtra("res_id");
        key = iv.getStringExtra("key");
        if (key == null) {
            key = "";
        }
    }


    private void callApiForSignUp() {

        S.E("ccp.getSelectedCountryCode() + etMobileNumber.getText().toString()) : : " + "+" + ccp.getSelectedCountryCode() + etMobileNumber.getText().toString());
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_registration(
                RequestBody.create(MediaType.parse("text/plain"), etemail.getText().toString()),
                RequestBody.create(MediaType.parse("text/plain"), etname.getText().toString()),
                RequestBody.create(MediaType.parse("text/plain"), etpassword.getText().toString()),
                RequestBody.create(MediaType.parse("text/plain"), Utility.getSharedPreferences(getApplicationContext(), Constants.DEVICE_ID)),
                RequestBody.create(MediaType.parse("text/plain"), "+" + ccp.getSelectedCountryCode() + etMobileNumber.getText().toString()),
                RequestBody.create(MediaType.parse("text/plain"), "1234"),
                RequestBody.create(MediaType.parse("image/*"), new File(picturepath))
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("registration response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    final JSONObject Obj = jsonArray.getJSONObject(0);
                    strSuccess = Obj.getString("status");
                    if (Obj.getString("status").equals("Success")) {
                        JSONObject data = Obj.getJSONObject("user_detail");
                        UserDataModel userDataModel = new UserDataModel();
                        userDataModel.setUserid(data.getString("id"));
                        userDataModel.setFullname(data.getString("fullname"));
                        userDataModel.setEmail(data.getString("email"));
                        userDataModel.setPhone_no(data.getString("phone_no"));
                        userDataModel.setDeviceID(data.getString("device_id"));
                        userDataModel.setReferal_code(data.getString("referal_code"));
                        userDataModel.setImage(data.getString("image"));
                        userDataModel.setCreated_at(data.getString("created_at"));
                        userDataModel.setLogin_with(data.getString("login_with"));
                        UserDataHelper.getInstance().insertDataModel(userDataModel);

                        Utility.setSharedPreference(getApplication(), Constants.SPL_CHK, "1");
                        if (key.equals("PlaceOrder")) {
                            Intent iv = new Intent(RegistrationActivity.this, AddressActivity.class);
                            iv.putExtra(Constants.RESTAURANT_ID, res_id);
                            iv.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                            startActivity(iv);
                            finish();
                        } else {
                            S.I_clear(RegistrationActivity.this, MainActivity.class, null);
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(RegistrationActivity.this, Obj.getString("error"), Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            Uri selectedImage = data.getData();
            Bitmap photo;
            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
                String[] filePathColumn = {MediaStore.MediaColumns.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturepath = cursor.getString(columnIndex);

                Log.d("picturepath", "" + picturepath);
                cursor.close();
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    imgprofile.setImageBitmap(decodeFile(picturepath));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (requestCode == RESULT_cam_IMAGE && resultCode == RESULT_OK && null != data) {
                Log.d("photo", "" + data.getExtras().get("data"));
                photo = (Bitmap) data.getExtras().get("data");
                imgprofile.setImageBitmap(photo);
                imgprofile.setImageBitmap(photo);
                Uri tempUri = getImageUri(getApplicationContext(), photo);
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                picturepath = String.valueOf(finalFile);

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String temp = cursor.getString(idx);
        cursor.close();
        return temp;
    }

    private Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
}

