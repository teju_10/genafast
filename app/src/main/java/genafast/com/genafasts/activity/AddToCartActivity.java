package genafast.com.genafasts.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;

import static genafast.com.genafasts.Utility.Constants.RESTAURANT_ID;

public class AddToCartActivity extends AppCompatActivity {

    String food_id;
    String food_name;
    String food_price;
    String food_desc, RestID = "";
    int quantity = 0;

    TextView tvTitle, tvDesc, tvTextQuantity, tvPrice;
    RelativeLayout btnAddtoCart;
    ImageButton btn_minus;
    ImageButton btn_plus;
    private double price;
    private ImageView btnBack;
    private EditText etinstruction;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.e("AddToCartActivity ====", "attachBaseContext");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE));

        setContentView(R.layout.activity_add_to_cart);

//        changeStatsBarColor(AddToCartActivity.this);

        /*Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Add to cart");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        tvTextQuantity = (TextView) findViewById(R.id.tvTextQuantity);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        btnAddtoCart = (RelativeLayout) findViewById(R.id.btnAddtoCart);
        btn_minus = (ImageButton) findViewById(R.id.btn_minus);
        btn_plus = (ImageButton) findViewById(R.id.btn_plus);
        btnBack = (ImageView) findViewById(R.id.btnBack);
        etinstruction = (EditText) findViewById(R.id.etinstruction);

        food_id = getIntent().getStringExtra("food_id");
        food_name = getIntent().getStringExtra("food_name");
        food_price = getIntent().getStringExtra("food_price");
        food_desc = getIntent().getStringExtra("food_desc");
        RestID = getIntent().getStringExtra(RESTAURANT_ID);

        tvTitle.setText(food_name);
        tvDesc.setText(food_desc);
        tvPrice.setText(getString(R.string.currency) + " " + "0");

        if (CartDataHelper.getInstance().getUserDataModelSingle(food_id).size() > 0) {
            if (CartDataHelper.getInstance().getUserDataModelSingle(food_id).get(0).getQuantity().equals("0")) {
                tvTextQuantity.setText(quantity + "");
            } else {
                quantity = Integer.parseInt(CartDataHelper.getInstance().getUserDataModelSingle(food_id).get(0).getQuantity());
                tvTextQuantity.setText(quantity + "");
                price = Double.parseDouble(food_price) * Integer.parseInt(CartDataHelper.getInstance().getUserDataModelSingle(food_id).get(0).getQuantity());
                tvPrice.setText(getString(R.string.currency) + " " + price);
                etinstruction.setText(CartDataHelper.getInstance().getUserDataModel().get(0).getFoodInstrction());
            }
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = quantity + 1;

                tvTextQuantity.setText(quantity + "");

                price = Double.parseDouble(food_price) * quantity;
                tvPrice.setText(getString(R.string.currency) + " " + price);
                if (quantity > 0) {
                    btnAddtoCart.setVisibility(View.VISIBLE);
                } else {
                    btnAddtoCart.setVisibility(View.GONE);
                }
            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!tvTextQuantity.getText().toString().equals("0")) {
                    quantity = quantity - 1;

                    tvTextQuantity.setText(quantity + "");

                    price = Double.parseDouble(food_price) * quantity;
                    tvPrice.setText(getString(R.string.currency) + " " + price);
                    if (quantity > 0) {
                        btnAddtoCart.setVisibility(View.VISIBLE);
                    } else {
                        btnAddtoCart.setVisibility(View.GONE);
                    }
                }
            }
        });

        btnAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etinstruction.getText().toString().isEmpty()) {
                    S.T(getApplication(), getApplication().getString(R.string.fill_foodinstraction));
                } else {
                    CartDataModel cartDataModel = new CartDataModel();
                    cartDataModel.setRestaurantId(RestID);
                    cartDataModel.setFoodid(food_id);
                    cartDataModel.setFoodname(food_name);
                    cartDataModel.setFooddesc(food_desc);
                    cartDataModel.setFoodprice(food_price);
                    cartDataModel.setFoodInstrction(etinstruction.getText().toString());
                    cartDataModel.setTotalprice(String.valueOf(price));
                    cartDataModel.setQuantity(String.valueOf(quantity));
                    CartDataHelper.getInstance().insertUserDataModel(cartDataModel);

                  //  Utility.ShowToastMessage(getApplication(), getApplication().getString(R.string.str_item_addedd_cart));
                    finish();
                }
            }
        });
    }
}
