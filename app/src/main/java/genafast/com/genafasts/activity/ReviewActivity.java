package genafast.com.genafasts.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.UserDataModel;
import genafast.com.genafasts.model.ReviewModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class ReviewActivity extends BaseActivity {

    @BindView(R.id.rb_star)
    RatingBar rb_star;

    @BindView(R.id.ed_Feedback)
    EditText ed_Feedback;

    @BindView(R.id.btn_submitFeed)
    Button btn_submitFeed;
//
//    @BindView(R.id.recycler_view)
//    Button recycler_view;
//
//    @BindView(R.id.imgplusicon)
//    Button imgplusicon;

    Dialog dialog;


    float rateValue = 0;
    String restID = "";
    private ArrayList<ReviewModel> reviewlist;

    @Override
    protected int getContentResId() {
        return R.layout.dialog_rattingrastaurant;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Review");
        } else {
            setToolbarWithBackButton("revisión");
        }

        try {
            Bundle b = new Bundle();
            b = getIntent().getExtras();
            restID = b.getString(Constants.RESTAURANT_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        rb_star.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rateValue = rating;
            }
        });

        btn_submitFeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callApiPostRestaurent(rateValue, ed_Feedback.getText().toString().trim());

            }
        });
    }

    private void callApiGetRestaurent() {
        new JSONParser(ReviewActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).restaurent_review(

        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.E("restaurent response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response.toString());
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equals("Success")) {
                        JSONArray data = jsonObject.getJSONArray("Reviews");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject rev_detail = data.getJSONObject(i);
                            ReviewModel temp = new ReviewModel();
                            temp.setId(rev_detail.getString("id"));
                            temp.setUsername(rev_detail.getString("username"));
                            temp.setImage(rev_detail.getString("image"));
                            temp.setReview_text(rev_detail.getString("review_text"));
                            temp.setRatting(rev_detail.getString("ratting"));
                            reviewlist.add(temp);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void callApiPostRestaurent(float ratting, String feed) {
        new JSONParser(ReviewActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).Postreview(
                restID,
                String.valueOf(ratting),
                UserDataHelper.getInstance().getData().get(0).getUserid(),
                feed
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.E("restaurent response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response.toString());
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equals("Success")) {
                        S.I_clear(getApplication(), MainActivity.class, null);
                        finish();
//                        JSONArray data = jsonObject.getJSONArray("Reviews");
//                        for (int i = 0; i < data.length(); i++) {
//                            JSONObject rev_detail = data.getJSONObject(i);
//                            ReviewModel temp = new ReviewModel();
//                            temp.setId(rev_detail.getString("id"));
//                            temp.setUsername(rev_detail.getString("username"));
//                            temp.setImage(rev_detail.getString("image"));
//                            temp.setReview_text(rev_detail.getString("review_text"));
//                            temp.setRatting(rev_detail.getString("ratting"));
//                            reviewlist.add(temp);
//                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    private void FeedbackDialog() {
//        dialog = new Dialog(getApplication(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        dialog.setContentView(R.layout.dialog_rattingrastaurant);
//        Window window = dialog.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//
//        wlp.gravity = Gravity.CENTER;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
//        window.setAttributes(wlp);
//        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//
//        RatingBar rattBar = dialog.findViewById(R.id.rb_star);
//        final EditText edFeed = dialog.findViewById(R.id.ed_Feedback);
//
//        rattBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                rateValue = rating;
//            }
//        });
//
//        dialog.findViewById(R.id.btn_submitFeed).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (edFeed.getText().toString().trim().isEmpty()) {
//                    Utility.ShowToastMessage(getApplication(), "Please add your feedback");
//                } else {
//                }
//            }
//        });
//
//        dialog.show();
//    }

}
