package genafast.com.genafasts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.oob.SignUp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.UserAccount;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.UserDataModel;
import genafast.com.genafasts.model.RegisterModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.etemail)
    EditText etemail;
    @BindView(R.id.etpassword)
    EditText etpassword;
    @BindView(R.id.btnregistration)
    Button btnregistration;
    @BindView(R.id.tvaccount)
    TextView tvaccount;
    @BindView(R.id.tvor)
    TextView tvor;
    @BindView(R.id.btn_fb)
    Button btnFb;
    @BindView(R.id.btn_google)
    Button btnGoogle;
    ArrayList<RegisterModel> login = new ArrayList<>();
    private String key = "";
    private String res_id = "";


    @Override
    protected int getContentResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Utility.ChangeLang(getApplication(), Utility.getLanSharedPreferences(getApplication(), Constants.LANGUAGE));

        ButterKnife.bind(this);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Login");
        }else{
            setToolbarWithBackButton("Iniciar sesión");
        }
        //setToolbarWithBackButton(getApplication().getString(R.string.str_login));

        gettingIntents();

        tvaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!key.equals("")) {
                    Intent iv = new Intent(LoginActivity.this, RegistrationActivity.class);
                    iv.putExtra("key", "PlaceOrder");
                    iv.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(iv);
                } else {
                    S.I_clear(getApplication(), RegistrationActivity.class, null);
                }
            }
        });

        btnregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserAccount userAccount = new UserAccount(getApplication(), etemail, etpassword);
                if (userAccount.isEmpty(etemail, etpassword)) {
                    if (userAccount.isEmailValid(etemail)) {
                        if (userAccount.isPasswordValid(etpassword)) {
                            callApiForLogin();
                        }
                    }
                }
            }
        });
        btnFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                callApiFacebook();
            }
        });
    }

    private void gettingIntents() {
        //getting intent

        if (Utility.getSharedPreferences(getApplication(), Constants.DEVICE_ID).equals("")) {
            String fcm_id = FirebaseInstanceId.getInstance().getToken();
            Utility.setSharedPreference(getApplication(), Constants.DEVICE_ID, fcm_id);
        }
        Intent iv = getIntent();
        res_id = getIntent().getStringExtra("res_id");
        key = iv.getStringExtra("key");
        if (key == null) {
            key = "";
        }
    }

    private void callApiForLogin() {
        new JSONParser(LoginActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_login(
                etemail.getText().toString(),
                etpassword.getText().toString(), Utility.getSharedPreferences(getApplicationContext(), Constants.DEVICE_ID)
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("login response : " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    final JSONObject Obj = jsonArray.getJSONObject(0);
                    Log.e("Obj", Obj.toString());
                    if (Obj.getString("status").equals("Success")) {
                        JSONObject data = Obj.getJSONObject("user_detail");
                        UserDataModel userDataModel = new UserDataModel();
                        userDataModel.setUserid(data.getString("id"));
                        userDataModel.setFullname(data.getString("fullname"));
                        userDataModel.setEmail(data.getString("email"));
                        userDataModel.setPhone_no(data.getString("phone_no"));
                        userDataModel.setDeviceID(data.getString("device_id"));
                        userDataModel.setReferal_code(data.getString("referal_code"));
                        userDataModel.setImage(data.getString("image"));
                        userDataModel.setCreated_at(data.getString("created_at"));
                        userDataModel.setLogin_with(data.getString("login_with"));
                        UserDataHelper.getInstance().insertDataModel(userDataModel);
                        Utility.setSharedPreference(getApplication(), Constants.SPL_CHK, "1");
                        if (key.equals("PlaceOrder")) {
                            Intent iv = new Intent(LoginActivity.this, AddressActivity.class);
                            iv.putExtra(Constants.RESTAURANT_ID, res_id);
                            iv.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
                            startActivity(iv);
                            finish();
                        } else {
                            S.I_clear(LoginActivity.this, MainActivity.class, null);
                        }
                    } else if (Obj.getString("status").equals("Failed")) {
                        S.T(LoginActivity.this, Obj.getString("error"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callApiFacebook() {
        new JSONParser(LoginActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_loginfacebook(
                UserDataHelper.getInstance().getData().get(0).getFullname(),
                UserDataHelper.getInstance().getData().get(0).getEmail(),
                UserDataHelper.getInstance().getData().get(0).getPhone_no(),
                UserDataHelper.getInstance().getData().get(0).getReferal_code(),
                UserDataHelper.getInstance().getData().get(0).getImage()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("login response : " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    final JSONObject Obj = jsonArray.getJSONObject(0);
                    Log.e("Obj", Obj.toString());
                    if (Obj.getString("status").equals("Success")) {
                        JSONObject data = Obj.getJSONObject("user_detail");
                        UserDataModel userDataModel = new UserDataModel();
                        userDataModel.setUserid(data.getString("id"));
                        userDataModel.setFullname(data.getString("fullname"));
                        userDataModel.setEmail(data.getString("email"));
                        userDataModel.setPhone_no(data.getString("phone_no"));
                        userDataModel.setReferal_code(data.getString("referal_code"));
                        userDataModel.setImage(data.getString("image"));
                        userDataModel.setCreated_at(data.getString("created_at"));
                        userDataModel.setLogin_with(data.getString("login_with"));
                        UserDataHelper.getInstance().insertDataModel(userDataModel);
                        Utility.setSharedPreference(getApplication(), Constants.USER_ID, data.getString("id"));
                    } else if (Obj.getString("status").equals("Failed")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(LoginActivity.this, Obj.getString("error"), Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                callApiforgoogle();
            }
        });
    }

    private void callApiforgoogle() {
        new JSONParser(LoginActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).user_google(
                UserDataHelper.getInstance().getData().get(0).getFullname(),
                UserDataHelper.getInstance().getData().get(0).getEmail(),
                UserDataHelper.getInstance().getData().get(0).getPhone_no(),
                UserDataHelper.getInstance().getData().get(0).getReferal_code(),
                UserDataHelper.getInstance().getData().get(0).getImage()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("login response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    final JSONObject Obj = jsonArray.getJSONObject(0);
                    Log.e("Obj", Obj.toString());
                    if (Obj.getString("status").equals("Success")) {
                        JSONObject data = Obj.getJSONObject("user_detail");
                        UserDataModel userDataModel = new UserDataModel();
                        userDataModel.setUserid(data.getString("id"));
                        userDataModel.setFullname(data.getString("fullname"));
                        userDataModel.setEmail(data.getString("email"));
                        userDataModel.setPhone_no(data.getString("phone_no"));
                        userDataModel.setReferal_code(data.getString("referal_code"));
                        userDataModel.setImage(data.getString("image"));
                        userDataModel.setCreated_at(data.getString("created_at"));
                        userDataModel.setLogin_with(data.getString("login_with"));
                        UserDataHelper.getInstance().insertDataModel(userDataModel);
                    } else if (Obj.getString("status").equals("Failed")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(LoginActivity.this, Obj.getString("error"), Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
