package genafast.com.genafasts.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.PlaceArrayAdapter;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.model.myOrderGetSet;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class UpdateAddressActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerDragListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    private static final String MyPREFERENCES = "Fooddelivery";
    private static final String MY_PREFS_NAME = "Fooddelivery";
    private static final String TAG = UpdateAddressActivity.class.getName();
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(22.724355, 75.8838944), new LatLng(22.7532848, 75.8936962));
    private static ArrayList<CartDataModel> cartlist;
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.btnAddAddress)
    Button btnAddAddress;
    String Error;
    Context mContext;
    private double latitudecur;
    private double longitudecur;
    private GoogleMap googleMap;
    private String address;
    private AutoCompleteTextView edt_address;
    private String status = "";
    private String allresid;
    private String description, str_address = "";
    //    private SQLiteDatabase db;
    private String userid;
    private View layout12;
    private EditText edt_note;
    private float total = 0;
    private myOrderGetSet data;
    private String res_id;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();


            edt_address.setText(Html.fromHtml(place.getAddress() + ""));

            latitudecur = place.getLatLng().latitude;
            longitudecur = place.getLatLng().longitude;
            System.out.println("LatLong====" + latitudecur + "LongiiFataakaa " + longitudecur);
        }
    };
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.e(TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    @Override
    protected int getContentResId() {
        return R.layout.activity_update_address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        mContext = this;

        if (Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")) {
            setToolbarWithBackButton("Update Address");
        } else {
            setToolbarWithBackButton("Actualizar dirección");
        }


       // setToolbarWithBackButton(getApplication().getString(R.string.update_adress));

        mGoogleApiClient = new GoogleApiClient.Builder(UpdateAddressActivity.this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();

        initialization();

        gettingLocation();

        edt_address.setOnItemClickListener(mAutocompleteClickListener);

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_address.getText().toString().isEmpty()) {
                    Utility.ShowToastMessage(mContext, getApplication().getString(R.string.fill_delivery_address));
                } else {
                    callApiForAddAddress();
                }
            }
        });
    }

    private void gettingLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(UpdateAddressActivity.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
            } catch (NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } else {

            gps.showSettingsAlert();
        }

    }

    private void initialization() {
        TextView txt_header = findViewById(R.id.txt_title);
        edt_address = findViewById(R.id.edt_address);

        Button btn_selectlocation = findViewById(R.id.btn_selectlocation);

        MapsInitializer.initialize(getApplicationContext());
        initilizeMap();

        btn_selectlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetDataAsyncTask().execute();
            }
        });

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        edt_address.setAdapter(mPlaceArrayAdapter);
    }

    private void initilizeMap() {

        SupportMapFragment supportMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment));
        supportMapFragment.getMapAsync(this);
        (findViewById(R.id.mapFragment)).getViewTreeObserver()
                .addOnGlobalLayoutListener(new android.view.ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {

                        (findViewById(R.id.mapFragment)).getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.no_map), Toast.LENGTH_SHORT).show();
                return;
            }
            this.googleMap = googleMap;
            googleMap.setOnMarkerDragListener(this);


            initializeUiSettings();
            initializeMapLocationSettings();
          //  initializeMapTraffic();
            initializeMapType();
            initializeMapViewSettings();

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            LatLng position = new LatLng(latitudecur, longitudecur);
            googleMap.addMarker(new MarkerOptions().position(position).draggable(true).title(address));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18.0f));


        } catch (NullPointerException | NumberFormatException e) {
            // TODO: handle exception
        }

    }

    private void initializeUiSettings() {
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    private void initializeMapLocationSettings() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    private void initializeMapTraffic() {
        googleMap.setTrafficEnabled(true);
    }

    private void initializeMapType() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    private void initializeMapViewSettings() {
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(false);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        LatLng field = marker.getPosition();
        System.out.println("LatitudenLongitude:" + field.latitude + " " + field.longitude);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng field = marker.getPosition();

        latitudecur = field.latitude;
        longitudecur = field.longitude;

    }

    private List<Address> getAddress() {
        if (latitudecur != 0 && longitudecur != 0) {
            try {
                Geocoder geocoder = new Geocoder(UpdateAddressActivity.this);
                List<Address> addresses = geocoder.getFromLocation(latitudecur, longitudecur, 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);
                Log.d("TAG", "address = " + address + ", city = " + city + ", country = " + country);
                return addresses;

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Toast.makeText(PlaceOrder.this, R.string.error_lat_long, Toast.LENGTH_LONG).show();
        }
        return null;
    }

    private void callApiForAddAddress() {
        S.E("LatLong-====" + latitudecur + " " + longitudecur);
        new JSONParser(UpdateAddressActivity.this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).AddAddress(
                "addAddress",
                UserDataHelper.getInstance().getData().get(0).getUserid(),
                edt_address.getText().toString(),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur)
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                Utility.E("address response : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String status = jsonObject.getString("status");

                    if (status.equals("Success")) {
                        Intent intent = getIntent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    //PICKUP ADDRESS THROUGH AUTOTYPE


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();//finishing activity
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    class GetDataAsyncTask extends AsyncTask<Void, Void, Void> {
        ProgressDialog pd;
        double latii,longi;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(UpdateAddressActivity.this);
            pd.setMessage(getString(R.string.txt_load));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (getAddress() != null)
                address = getAddress().get(0).getAddressLine(0);
            latii = getAddress().get(0).getLatitude();
            longi = getAddress().get(0).getLongitude();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pd.isShowing()) {
                pd.dismiss();
            }
            edt_address.setText(address);

            LatLng position = new LatLng(latii, longi);

            googleMap.addMarker(new MarkerOptions().position(position).draggable(true).title(address));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 15.0f));

        }

    }

}
