package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.MoneyValueFilter;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.model.myOrderGetSet;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class MakePaymentActivity extends AppCompatActivity {

    private static ArrayList<CartDataModel> cartlist;
    Context mContext;
    @BindView(R.id.ll_payByCash)
    LinearLayout ll_payByCash;
    @BindView(R.id.ll_BOTH)
    LinearLayout ll_BOTH;
    @BindView(R.id.laypaymenttypeImage)
    LinearLayout laypaymenttypeImage;
    @BindView(R.id.ll_paymnt_baleto)
    LinearLayout ll_paymnt_baleto;
    @BindView(R.id.ll_paymnt_factor)
    LinearLayout ll_paymnt_factor;
    @BindView(R.id.rb_boleto)
    RadioButton rb_boleto;
    @BindView(R.id.rg_paymntProof)
    RadioGroup rg_paymntProof;
    @BindView(R.id.rb_factor)
    RadioButton rb_factor;
    @BindView(R.id.tv_weeAccept)
    TextView tv_weeAccept;
    @BindView(R.id.tv_totalAmount)
    TextView tv_totalAmount;
    @BindView(R.id.etcash)
    EditText etcash;

    @BindView(R.id.Both_etcash)
    EditText Both_etcash;
    @BindView(R.id.etpos)
    EditText etpos;
    @BindView(R.id.etDNI)
    EditText etDNI;
    @BindView(R.id.etNumber)
    EditText etNumber;
    @BindView(R.id.etruc)
    EditText etruc;
    @BindView(R.id.etReason)
    EditText etReason;
    @BindView(R.id.etcomments)
    EditText etcomments;
    boolean CLICKDNITYPE = true;
    boolean CLICKRUCTYPE = false;
    String paymentType = "", latitudedest= "", longitudedest= "",
            address = "", allresid = "", description = "", proof_type = "0",
            strCashtxt = "", formattedString,strshippingPrice = "";
    // int  = 0;
    JSONObject sendDetail;
    float roundOff = 0;
    private float total = 0, BothAmt = 0, cashtotalAmount = 0, calculateValue = 0, intTotalMoney;
    private myOrderGetSet data;
    private String status = "";
    private View layout12;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.w("MakePaymentActivity ===", "attachBaseContext");
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE));

        setContentView(R.layout.activity_makepaymenttype);

        ButterKnife.bind(this);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        rb_boleto.setChecked(true);
        init();
        RadioG();

        callApiForPlaceOrder();
        //CashPaymentValidation();
        PaymentValidation();


        findViewById(R.id.btnmakeOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (proof_type.equals("")) {
                        Utility.ShowToastMessage(mContext, "Please select proof type");

                    } else if (CLICKDNITYPE && etDNI.getText().toString().length() < 8) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.dnilength));
                    } else if (CLICKRUCTYPE && etruc.getText().toString().length() < 11) {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.ruclength));
                    } else if (paymentType.equals("COD")) {
                        if (etcash.getText().toString().equals("")) {
                            Utility.Alert(mContext, "Alert", "Enter Full or valid amount");
                        } else {

                            cashtotalAmount = Float.valueOf(etcash.getText().toString().trim());

                            if (cashtotalAmount < roundOff) {
                                Utility.Alert(mContext, "Alert", "Enter Full or valid amount");
                            } else {
                                apiPlacedOrder(sendDetail, roundOff, etcash.getText().toString());
                            }
                        }

                    } else if (paymentType.equals("POS")) {
                        apiPlacedOrder(sendDetail, roundOff, "");

                    } else if (paymentType.equals("COD/POS")) {
                        if (Both_etcash.getText().toString().trim().isEmpty()) {
                            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.fillcash));
                        } else if (Both_etcash.getText().toString().trim().isEmpty()) {
                            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.fillcash));

                        } else {
                            float both_cash = Float.valueOf(Both_etcash.getText().toString().trim());
                            float both_pos = Float.valueOf(etpos.getText().toString().trim().replace(",","."));
                            BothAmt = both_cash + both_pos;
                            if (BothAmt == intTotalMoney) {
                                apiPlacedOrder(sendDetail, roundOff, Both_etcash.getText().toString());
                            } else {

                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void init() {

        try {
            Bundle b = new Bundle();
            b = getIntent().getExtras();
            paymentType = b.getString(Constants.PAYMENTTYPE);
            address = b.getString(Constants.ADDRESS);
            allresid = b.getString(Constants.RESTAURANT_ID);
            latitudedest = b.getString(Constants.LATITUDE);
            longitudedest= b.getString(Constants.LONGITUDE);
            paymentType = b.getString(Constants.PAYMENTTYPE);
            strshippingPrice = b.getString(Constants.SHIPPINGPRICE);
            if (paymentType.equals("COD")) {
                ((TextView) findViewById(R.id.toolbar_title)).setText
                        (mContext.getResources().getString(R.string.pay_cash));
                ll_BOTH.setVisibility(View.GONE);
                ll_payByCash.setVisibility(View.VISIBLE);
                laypaymenttypeImage.setVisibility(View.GONE);
            } else if (paymentType.equals("POS")) {
                ((TextView) findViewById(R.id.toolbar_title)).setText
                        (mContext.getResources().getString(R.string.pay_other));
                tv_weeAccept.setVisibility(View.VISIBLE);
                ll_BOTH.setVisibility(View.GONE);
                ll_payByCash.setVisibility(View.GONE);
                laypaymenttypeImage.setVisibility(View.VISIBLE);
            } else if (paymentType.equals("COD/POS")) {
                ((TextView) findViewById(R.id.toolbar_title)).setText
                        (mContext.getResources().getString(R.string.paybyboth));
                ll_payByCash.setVisibility(View.GONE);
                ll_BOTH.setVisibility(View.VISIBLE);
                laypaymenttypeImage.setVisibility(View.GONE);
            }


            etcash.setFilters(new InputFilter[]{new MoneyValueFilter()});
            Both_etcash.setFilters(new InputFilter[]{new MoneyValueFilter()});

        } catch (Exception e) {

        }
    }


    private void PaymentValidation() {

        Both_etcash.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    // CommaText();
                    if (editable.length() > 0) {
                        strCashtxt = Both_etcash.getText().toString();

                        calculateValue = intTotalMoney - Float.valueOf(strCashtxt);

                        if (calculateValue >= 0) {
                            //  etpos.setText(new DecimalFormat("##.##").format(calculateValue));

                            if (editable.toString().contains(".")) {
                                etpos.setText(new DecimalFormat("##.##").format(calculateValue));
                                CommaText();
                            } else {
                                etpos.setText(new DecimalFormat("##.##").format(calculateValue));
                            }
//                            longval = Long.parseLong(originalString);
//
//                            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
//                            formatter.applyPattern("##.##");
//                            formattedString = formatter.format(longval);
//                            String value = (Float.valueOf(calculateValue).toString().replace(",", "."));
//                            S.E("Value " + value);
//                            //etpos.setText(new DecimalFormat("##.##").format(Float.valueOf(value)));
//

                        } else {
                            Both_etcash.setText("");
                            etpos.setText("");
                            Utility.Alert(mContext, "Alert", "Enter valid amount");
                            //Utility.ShowToastMessage(mContext,"Enter valid amount");
                        }

                    } else {
                        etpos.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void CommaText() {
        etpos.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() > 0) {
                        String originalString = s.toString();
                        double longval;
                        if (originalString.contains(",")) {
                            originalString = originalString.replaceAll(",", ".");
                            longval = Double.parseDouble(originalString);

                            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                            formatter.applyPattern("##.##");
                            formattedString = formatter.format(longval);
                            etpos.setText(formattedString);
                        }

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    S.E("Exception " + e);
                }
            }
        });
    }

//    private void CashPaymentValidation() {
//        etcash.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                try {
//                    if (editable.length() > 0) {
//                        float cashValue = Float.valueOf(etcash.getText().toString());
//                        if (cashValue > intTotalMoney) {
//                            etcash.setText("");
//                            Utility.Alert(mContext, "Alert", "Enter valid amount");
//
//                        } else {
//                            // etcash.setText("");
//                            //   Utility.Alert(mContext, "Alert", "Enter valid amount");
//                        }
//
//                    } else {
//                        etpos.setText("");
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void RadioG() {
        rg_paymntProof.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (rb_boleto.isChecked()) {
                    CLICKDNITYPE = true;
                    CLICKRUCTYPE = false;
                    ll_paymnt_baleto.setVisibility(View.VISIBLE);
                    ll_paymnt_factor.setVisibility(View.GONE);
                    etcomments.setVisibility(View.GONE);
                    proof_type = "0";
                    etDNI.setText("");
                    etruc.setText("");
                    etcomments.setText("");
                } else if (rb_factor.isChecked()) {
                    CLICKDNITYPE = false;
                    CLICKRUCTYPE = true;
                    etDNI.setText("");
                    etruc.setText("");
                    etReason.setText("");
                    etcomments.setText("");
                    ll_paymnt_baleto.setVisibility(View.GONE);
                    ll_paymnt_factor.setVisibility(View.VISIBLE);
                    etcomments.setVisibility(View.VISIBLE);
                    proof_type = "1";
                } else {
                    CLICKDNITYPE = true;
                    CLICKRUCTYPE = false;
                    ll_paymnt_baleto.setVisibility(View.VISIBLE);
                    ll_paymnt_factor.setVisibility(View.GONE);
                    etcomments.setVisibility(View.GONE);
                    proof_type = "0";
                }
            }
        });
    }

    private void callApiForPlaceOrder() {
        sendDetail = new JSONObject();
        JSONObject jo_itemId = new JSONObject();
        JSONObject jo_item;
        JSONObject jo_itemQuantity = new JSONObject();
        JSONObject jo_itemPrice = new JSONObject();
        JSONArray jo_detail = null;
        jo_detail = new JSONArray();
        StringBuilder totalOrderInfo = new StringBuilder();
        StringBuilder totalvalue = new StringBuilder();
        StringBuilder totalDetailOrder = new StringBuilder();
        cartlist = CartDataHelper.getInstance().getUserDataModel();
        for (int i = 0; i < cartlist.size(); i++) {
            jo_item = new JSONObject();
            String tempItemName = cartlist.get(i).getFoodname();
            totalvalue.append(tempItemName).append(",");
            String menu_id = "ItemId=" + cartlist.get(i).getFoodid();
            String qty = "/ItemQty=" + cartlist.get(i).getQuantity();
            float ans1 = Float.parseFloat(cartlist.get(i).getFoodprice());
            float ans2 = Float.valueOf(cartlist.get(i).getTotalprice().replace("$", ""));
            float ans3 = ans1 * ans2;
            String sprice = "/ItemAmt=" + String.valueOf(ans3);
            String SingleOrderInfo = menu_id + qty + sprice;
            totalDetailOrder.append(" Item Name: ").append(tempItemName).append(" Item Price: ").append(ans1).append(" Item Quantity: ").append(ans2).append(" Total: ").append(ans3).append(";");
            totalOrderInfo.append(SingleOrderInfo).append(",");
            S.E("Multiple Restaurant ID==== " + cartlist.get(i).getRestaurantId());
            try {
                S.E("FOOD INSTRCTION===== " + cartlist.get(i).getFoodInstrction());
                jo_itemId.put("ItemId", cartlist.get(i).getFoodid());
                jo_itemQuantity.put("ItemQty", cartlist.get(i).getQuantity());
                jo_itemPrice.put("ItemAmt", cartlist.get(i).getTotalprice());
                jo_item.put("ItemId", cartlist.get(i).getFoodid());
                jo_item.put("food_instruction", cartlist.get(i).getFoodInstrction());
                jo_item.put("ItemQty", cartlist.get(i).getQuantity());
                jo_item.put("ItemAmt", cartlist.get(i).getTotalprice());
                jo_detail.put(jo_item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            sendDetail.put("Order", jo_detail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < cartlist.size(); i++) {
            total = total + Float.parseFloat(cartlist.get(i).getTotalprice());
        }
        roundOff = total+Float.valueOf(strshippingPrice);
        Utility.rounddecimalFormate(roundOff);
        // roundOff = (double) Math.round(total * 100) / 100;

        S.E("UserDataHelper.getInstance().getData().get(0).getUserid() : : " + UserDataHelper.getInstance().getData().get(0).getUserid());
        S.E("jo_detail.toString() : : " + sendDetail.toString());
        S.E("allresid : : " + allresid);
        S.E("address : : " + address);
        S.E("String.valueOf(latitudecur) : : " + String.valueOf(latitudedest));
        S.E("String.valueOf(longitudecur) : : " + String.valueOf(longitudedest));
        S.E("description : : " + description);
        S.E("String.valueOf(roundOff) : : " + String.valueOf(roundOff));
        //intTotalMoney = (float) Math.round(roundOff);
        intTotalMoney = roundOff;
        tv_totalAmount.setText("S/ " + Utility.rounddecimalFormate(roundOff));
        etcash.setText(String.valueOf(roundOff));
    }

    private void apiPlacedOrder(JSONObject sendDetail, double roundOff, String _cash) {

        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).placeOrder(
                UserDataHelper.getInstance().getData().get(0).getUserid(),
                allresid,
                address,
                String.valueOf(latitudedest),
                String.valueOf(longitudedest),
                sendDetail.toString(),
                description,
                String.valueOf(roundOff), paymentType, _cash,
                etpos.getText().toString().trim(), proof_type, etDNI.getText().toString().trim(),
                etNumber.getText().toString().trim(),
                etruc.getText().toString().trim(), etReason.getText().toString().trim(),
                etcomments.getText().toString().trim()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("place order response : " + response);
                try {
                    Utility.removepreference(mContext, Constants.RESTAURANT_ID);
                    Utility.setSharedPreference(mContext, Constants.RESTAURANT_ID, "");
                    data = new myOrderGetSet();
                    JSONObject responsedat = new JSONObject(response);
                    String txt_success = responsedat.getString("success");
                    if (txt_success.equals("Order Book Successfully")) {
                        status = txt_success;
                        JSONArray jA_category = responsedat.getJSONArray("order_details");
                        JSONObject cat_detail = jA_category.getJSONObject(0);
                        data.setResName(cat_detail.getString("restaurant_name"));
                        data.setResAddress(cat_detail.getString("restaurant_address"));
                        data.setOrder_total(cat_detail.getString("order_amount"));
                        data.setOrder_id(cat_detail.getString("order_id"));
                        data.setOrder_dateTime(cat_detail.getString("order_date"));
                        CartDataHelper.getInstance().delete();
                    } else {
                        status = txt_success;
                    }
                    updateUI();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI() {

        if (status.equals("Order Book Successfully")) {
            Intent iv = new Intent(MakePaymentActivity.this, CompleteOrder.class);
            iv.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            iv.putExtra("orderid", data.getOrder_id());
            iv.putExtra("restName", data.getResName());
            iv.putExtra("resAddress", data.getResAddress());
            iv.putExtra("ordertime", data.getOrder_dateTime());
            iv.putExtra("order_amount", data.getOrder_total());
            iv.putExtra("key", "orderplace");
            startActivity(iv);
            finish();

        } else {
            RelativeLayout rl_back = findViewById(R.id.rl_back);
            if (rl_back == null) {
                Log.d("second", "second");
                RelativeLayout rl_dialoguser = findViewById(R.id.rl_infodialog);
                try {
                    layout12 = getLayoutInflater().inflate(R.layout.addcart, rl_dialoguser, false);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                rl_dialoguser.addView(layout12);
                ImageView img = layout12.findViewById(R.id.imageView);
                img.setImageResource(R.drawable.cancel_icon);
                TextView txt_dia = layout12.findViewById(R.id.txt_dia);
                txt_dia.setText("" + getString(R.string.notbooked));
                Button btn_yes = layout12.findViewById(R.id.btn_yes);
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Intent iv = new Intent(MakePaymentActivity.this, MainActivity.class);
                        startActivity(iv);
                    }
                });
            }
        }

    }


}
