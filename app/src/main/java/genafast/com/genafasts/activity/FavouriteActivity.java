package genafast.com.genafasts.activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.twotoasters.jazzylistview.JazzyListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;

public class FavouriteActivity extends BaseActivity {
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.listview)
    JazzyListView listview;

    @Override
    protected int getContentResId() {
        return R.layout.activity_favourite;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        setToolbarWithBackButton(getApplication().getString(R.string.txt_favourite));
    }
}
