package genafast.com.genafasts.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.AppController;
import genafast.com.genafasts.BuildConfig;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.model.CustomMarker;
import genafast.com.genafasts.model.DetailModel;
import genafast.com.genafasts.model.menugetset;
import genafast.com.genafasts.model.ordergetset;
import genafast.com.genafasts.sticky.ItemHeader;
import genafast.com.genafasts.sticky.ItemHeaderViewBinder;
import genafast.com.genafasts.sticky.UserItemViewBinder;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.ConnectionDetector;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import tellh.com.stickyheaderview_rv.StickyHeaderView;
import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

import static genafast.com.genafasts.activity.MyOrderActivity.mContext;
import static genafast.com.genafasts.activity.MyOrderActivity.showErrorDialog;

public class RestaurantDetailActivity extends AppCompatActivity implements OnMapReadyCallback{

    private static ArrayList<DetailModel> detaillist = new ArrayList<>();
    private static ArrayList<menugetset> menulist = new ArrayList<>();
    private final int start = 0;
    private final String tag = "fb";
    @BindView(R.id.img_detail)
    ImageView imgDetail;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_share)
    Button btnShare;
    @BindView(R.id.tvRestoName)
    TextView tvRestoName;
    @BindView(R.id.rel_bottom)
    RelativeLayout relBottom;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.txt_category)
    TextView txtCategory;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.btn_fav)
    ImageButton btnFav;
    @BindView(R.id.btn_fav1)
    ImageButton btnFav1;
    @BindView(R.id.rel_title)
    RelativeLayout relTitle;
    @BindView(R.id.txt_ratenumber)
    TextView txtRatenumber;
    @BindView(R.id.rate)
    RatingBar rate;
    @BindView(R.id.txt_totalReviews)
    TextView txtTotalReviews;
    @BindView(R.id.btn_call)
    Button btnCall;
    @BindView(R.id.btn_map)
    Button btnMap;
    @BindView(R.id.layoutRating)
    RelativeLayout layoutRating;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.txt_timingtitle)
    TextView txtTimingtitle;
    @BindView(R.id.txt_timingdesc)
    TextView txtTimingdesc;
    @BindView(R.id.txt_deliverytitle)
    TextView txtDeliverytitle;
    @BindView(R.id.txt_deliverydesc)
    TextView txtDeliverydesc;
    @BindView(R.id.layoutAddress)
    LinearLayout layoutAddress;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.stickyHeaderView)
    StickyHeaderView stickyHeaderView;
    @BindView(R.id.stickyLayout)
    LinearLayout stickyLayout;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.txt_addresstitle)
    TextView txtAddresstitle;
    @BindView(R.id.txt_distance)
    TextView txtDistance;
    @BindView(R.id.txt_addressdesc)
    TextView txtAddressdesc;
    @BindView(R.id.rl_address)
    RelativeLayout rlAddress;
    @BindView(R.id.txt_phonenumber)
    TextView txtPhonenumber;
    @BindView(R.id.txt_descnumber)
    TextView txtDescnumber;
    @BindView(R.id.rl_number)
    RelativeLayout rlNumber;
    @BindView(R.id.rl_timing)
    RelativeLayout rlTiming;
    @BindView(R.id.txt_foodtitle)
    TextView txtFoodtitle;
    @BindView(R.id.txt_fooddesc)
    TextView txtFooddesc;
    @BindView(R.id.food_desc)
    TextView food_desc;
    @BindView(R.id.rl_foodtype)
    RelativeLayout rlFoodtype;
    @BindView(R.id.rl_delivery_time)
    RelativeLayout rlDeliveryTime;
    @BindView(R.id.txt_deliverytypetitle)
    TextView txtDeliverytypetitle;
    @BindView(R.id.txt_deliverytypedesc)
    TextView txtDeliverytypedesc;
    @BindView(R.id.rl_delivery_type)
    RelativeLayout rlDeliveryType;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;
    @BindView(R.id.txt_description)
    TextView txtDescription;
    @BindView(R.id.tv_delivery_address)
    TextView tv_delivery_address;
    @BindView(R.id.addresslayout)
    RelativeLayout addresslayout;
    @BindView(R.id.cordinateLayout)
    CoordinatorLayout cordinateLayout;
    @BindView(R.id.tvTotalCartItems)
    TextView tvTotalCartItems;
    @BindView(R.id.tvTotalAmount)
    TextView tvTotalAmount;
    @BindView(R.id.tvCheckOut)
    LinearLayout tvCheckOut;
    @BindView(R.id.layoutCart)
    RelativeLayout layoutCart;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.btn_menu)
    Button btnMenu;
    @BindView(R.id.btn_review)
    Button btnReview;
    @BindView(R.id.tl_bar)
    TableLayout tlBar;
    View layout;
    String id;
    List<DataBean> userListBak = new ArrayList<>();
    Geocoder geocoder;
    List<Address> addresses = new ArrayList<>();
    Double finalfare, fare, distance;
    LatLng origin, destination;
    private double latitudecur, longitudecur;
    private ProgressDialog progressDialog;
    private SQLiteDatabase db;
    private Cursor cur = null;
    private String CategoryTotal = "";
    private String distancenew;
    private GoogleMap googleMap;
    private HashMap<CustomMarker, Marker> markersHashMap;
    private StickyHeaderViewAdapter adapter;
    private Random random = new Random(System.currentTimeMillis());
    private ArrayList<ordergetset> orderlist = new ArrayList<>();
    private int j;
    private String restaurent_name, resid = "";

    private static double roundMyData(double Rval, int numberOfDigitsAfterDecimal) {
        double p = (float) Math.pow(10, numberOfDigitsAfterDecimal);
        Rval = Rval * p;
        double tmp = Math.floor(Rval);
        System.out.println("~~~~~~tmp~~~~~" + tmp);
        return tmp / p;
    }

    public static boolean checkInternet(Context context) {
        // TODO Auto-generated method stub
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE));
        setContentView(R.layout.activity_new_main);
        ButterKnife.bind(this);
        mContext = this;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        tvCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID).equals("") &&
                        Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID) == null) {

                } else {
                    Intent iv = new Intent(RestaurantDetailActivity.this, CartActivity.class);
                    iv.putExtra("restaurent_name", restaurent_name);
                    startActivity(iv);
                }
            }
        });

        Intent iv = getIntent();
        distancenew = iv.getStringExtra("distance");
        restaurent_name = iv.getStringExtra("restaurent_name");
//        if (iv.getStringExtra("res_id").equals(null)) {
//            resid = Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID);
//        } else {
//            resid = iv.getStringExtra("res_id");
//        }
        if (checkInternet(RestaurantDetailActivity.this)) {
            gettingGPSLocation();
        } else
            showErrorDialog(RestaurantDetailActivity.this);
    }

    private void setAddress(double lati, double loni) {
        try {
            geocoder = new Geocoder(mContext);
            addresses = geocoder.getFromLocation(lati, loni, 1);
            String address = addresses.get(0).getAddressLine(0);
            tv_delivery_address.setText(address);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.e("Main Activity ====", "MainContext");
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(RestaurantDetailActivity.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);

                callApiForGetRestaurantDetails();

                callApiForGetMenu();

                origin = new LatLng(latitudecur, longitudecur);
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
                Log.e("Error", e.getMessage());
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    private Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap numberOfRecords ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(RestaurantDetailActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        double latitude = 0, longitude = 0;
        try {
            String lat = detaillist.get(0).getLat();
            String lon = detaillist.get(0).getLon();
            latitude = Double.parseDouble(lat);
            longitude = Double.parseDouble(lon);
        } catch (NumberFormatException e) {
            Log.e("Error", e.getMessage());
            Utility.ShowToastMessage(mContext, getString(R.string.later_txt));
            // TODO: handle exception
        }
        afterMapReady(latitude, longitude);

    }

    private void afterMapReady(double latitude, double longitude) {
        LatLng position = new LatLng(latitude, longitude);
        CustomMarker customMarkerOne = new CustomMarker("markerOne", latitude, longitude);
        try {
            MarkerOptions markerOption = new MarkerOptions().position(

                    new LatLng(customMarkerOne.getCustomMarkerLatitude(), customMarkerOne.getCustomMarkerLongitude()))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                    .title(detaillist.get(0).getName());

            Marker newMark = googleMap.addMarker(markerOption);

            addMarkerToHashMap(customMarkerOne, newMark);

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 17));

        } catch (Exception e1) {
            Utility.ShowToastMessage(mContext, getString(R.string.later_txt));

        }
    }

    private void addMarkerToHashMap(CustomMarker customMarker, Marker marker) {
        setUpMarkersHashMap();
        markersHashMap.put(customMarker, marker);
    }

    private void setUpMarkersHashMap() {
        if (markersHashMap == null) {
            markersHashMap = new HashMap<>();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void callApiForGetRestaurantDetails() {
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getRestaurantDetail(
                Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur)
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("restaurant details response : " + response);
                try {
                    JSONArray jObject = new JSONArray(response);
                    JSONObject Obj1;
                    Obj1 = jObject.getJSONObject(0);
                    switch (Obj1.getString("status")) {
                        case "Success":
                            detaillist.clear();
                            JSONObject jsonO = Obj1.getJSONObject("Restaurant_Detail");
                            DetailModel temp = new DetailModel();
                            temp.setId(jsonO.getString("id"));
                            temp.setName(jsonO.getString("name"));
                            temp.setAddress(jsonO.getString("address"));
                            temp.setTime(jsonO.getString("time"));
                            temp.setDelivery_time(jsonO.getString("delivery_time"));
                            temp.setCurrency(jsonO.getString("currency"));
                            temp.setPhoto(jsonO.getString("photo"));
                            temp.setPhone(jsonO.getString("phone"));
                            temp.setLat(jsonO.getString("lat"));
                            temp.setLon(jsonO.getString("lon"));
                            temp.setDesc(jsonO.getString("desc"));
                            temp.setEmail(jsonO.getString("email"));
                            temp.setLocation(jsonO.getString("address"));
                            temp.setRatting(jsonO.getString("ratting"));
                            temp.setRes_status(jsonO.getString("res_status"));
                            temp.setDelivery_charg(jsonO.getString("delivery_charg"));
                            temp.setDistance(jsonO.getString("distance"));
                            temp.setCategory(jsonO.getString("Category"));
                            String catname = jsonO.getString("Category");
                            CategoryTotal = CategoryTotal + catname;
                            detaillist.add(temp);
                            S.E("restaurant details name : " + detaillist.get(0).getName());

                            setRestaurantData();

                            break;
                        case "Failed":
                            final String error = Obj1.getString("Error");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utility.ShowToastMessage(mContext, error);
                                }
                            });
                            break;
                        default:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utility.ShowToastMessage(mContext, getApplication().getString(R.string.str_please_try_again));
                                }
                            });
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setRestaurantData() {
        collapsingToolbar.setTitle("");
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);

        S.E("detaillist.size() : : : " + detaillist.size());

        if (detaillist.size() > 0) {
            txtTitle.setText((detaillist.get(0).getName()));
            tvRestoName.setText((detaillist.get(0).getName()));
            //  food_desc.setText((detaillist.get(0).getCategory()));
            txtAddressdesc.setText((detaillist.get(0).getAddress()));
            txtDescnumber.setText((detaillist.get(0).getPhone()));
            txtTimingdesc.setText((detaillist.get(0).getTime()));

            setAddress(Double.parseDouble(detaillist.get(0).getLat()), Double.parseDouble(detaillist.get(0).getLon()));

            if (CategoryTotal != null) {
                String category = CategoryTotal.replace("[", "").replace("]", "").replace("\"", "").replace(",", ", ");
                food_desc.setText(category);
            }
            txtDeliverydesc.setText("Delivery in " + (detaillist.get(0).getDelivery_time()) + " minutes. Live tracking available");
            txtDeliverytypedesc.setText((detaillist.get(0).getDelivery_charg()));
            rate.setRating(Float.parseFloat(detaillist.get(0).getRatting()));
            rate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    // Auto-generated
                    Log.d("rate", "" + rating);
                }
            });
            txtRatenumber.setText("" + Float.parseFloat(detaillist.get(0).getRatting()));
            final String image = detaillist.get(0).getPhoto().replace(" ", "%20");
            Picasso.with(RestaurantDetailActivity.this)
                    .load(getString(R.string.link) + getString(R.string.imagepath) + image)
                    .into(imgDetail);
            Log.e("Image", getString(R.string.link) + getString(R.string.imagepath) + image);

            appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (collapsingToolbar.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsingToolbar)) {
                        //      btn_share.animate().alpha(1).setDuration(600);
//                            txt_ratenumber.animate().alpha(0).setDuration(600);
//                            rb.animate().alpha(0).setDuration(600);
                        //       txt_name.animate().alpha(0).setDuration(600);
                    } else {
//                            txt_ratenumber.animate().alpha(1).setDuration(600);
//                            rb.animate().alpha(1).setDuration(600);
                        //      txt_name.animate().alpha(1).setDuration(600);
                        //   btn_share.animate().alpha(0).setDuration(600);
                        txtTitle.animate().alpha(1).setDuration(600);
                        collapsingToolbar.setTitle("");
                    }
                }
            });
            try {
                double numbar = roundMyData(Double.parseDouble(distancenew), 1);
                txtDistance.setText("" + numbar + " " + getResources().getString(R.string.km));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            txtDescription.setText(detaillist.get(0).getDesc());

        }
        //adding map support
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment));
        mapFragment.getMapAsync(RestaurantDetailActivity.this);

        //listeners after getting detail

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String call = detaillist.get(0).getPhone();
                String uri = "tel:" + call;
                Intent i = new Intent(Intent.ACTION_DIAL);
                i.setData(Uri.parse(uri));
                startActivity(i);
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DetailModel temp_Obj3 = detaillist.get(start);

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + latitudecur + "," + longitudecur + "&daddr=" + temp_Obj3.getLat() + "," + temp_Obj3.getLon()));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*final Intent iv = new Intent(RestaurantDetailActivity.this, Review.class);
iv.putExtra("detail_id", "" + detaillist.get(0).getId());
iv.putExtra("name", "" + detaillist.get(0).getName());
iv.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
startActivity(iv);*/
            }
        });
    }

    private void callApiForGetMenu() {

        System.out.println("New MAIN Activity**** RestaurantID " + Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID));

        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getRestaurantMenu(
                Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID)
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("restaurant menu response : " + response);
                try {
                    JSONArray jObject = new JSONArray(response);
                    for (int i = 0; i < jObject.length(); i++) {
                        final JSONObject Obj1;
                        Obj1 = jObject.getJSONObject(i);

                        if (Obj1.getString("status").equals("Success")) {
                            JSONArray data = Obj1.getJSONArray("Menu_Category");
                            menulist.clear();
                            for (int iq = 0; iq < data.length(); iq++) {
                                JSONObject jdat = data.getJSONObject(iq);
                                menugetset temp = new menugetset();
                                temp.setId(jdat.getString("id"));
                                temp.setName(jdat.getString("name"));
                                temp.setCreated_at(jdat.getString("created_at"));
                                menulist.add(temp);
                            }

                            for (int j = 0; j < menulist.size(); j++) {
                                if (j == 0) {
                                    orderlist.clear();
                                }
                                callApiForGetSubMenu(menulist.get(j).getId(), menulist.get(j).getName());
                            }

                            S.E("menulist : " + menulist.get(0).getId());
                        } else {
                            S.E("success : " + "Failed:No data available ");
                            S.T(RestaurantDetailActivity.this, Obj1.getString("error"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callApiForGetSubMenu(String id, String name) {
        final String menuid = id;
        final String menuname = name;
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getRestaurantSubMenu(
                menuid
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("sub menu response  : " + response);
                try {
                    JSONArray jObject = new JSONArray(response);
                    JSONObject Obj;
                    Obj = jObject.getJSONObject(0);
                    ordergetset temp1 = new ordergetset();
                    temp1.setStatus(Obj.getString("status"));
                    JSONArray jarr = Obj.getJSONArray("Menu_List");
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject Obj1;
                        Obj1 = jarr.getJSONObject(i);
                        ordergetset temp = new ordergetset();
                        temp.setId(Obj1.getString("id"));
                        temp.setName(Obj1.getString("name"));
                        temp.setPrice(Obj1.getString("price"));
                        temp.setDesc(Obj1.getString("desc"));
                        temp.setCreated_at(Obj1.getString("created_at"));
                        String value = "0";
                        temp.setCounting(value);
                        orderlist.add(temp);
                    }

                    updateSubMenuList(menuid, menuname);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateSubMenuList(String menuid, String menuname) {
        S.E("call sub menu set method");
        if (menulist.get(0).getId().equals(menuid)) {
            j = 0;
        }

        userListBak.add(new ItemHeader(menuname));

        for (int i = j; i < orderlist.size(); i++) {
            j = i + 1;
            userListBak.add(orderlist.get(i));
        }

        Log.e("sohel", "menulist.get(menulist.size() - 1).getId() : : " + menulist.get(menulist.size() - 1).getId());
        Log.e("sohel", "menuid : : " + menuid);
        if (menulist.get(menulist.size() - 1).getId().equals(menuid)) {

            if (progressDialog != null)
                progressDialog.dismiss();

            adapter = new StickyHeaderViewAdapter(userListBak)
                    .RegisterItemType(new UserItemViewBinder(orderlist, Utility.getSharedPreferences(mContext, Constants.RESTAURANT_ID), RestaurantDetailActivity.this))
                    .RegisterItemType(new ItemHeaderViewBinder());
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<CartDataModel> arrayListCart = CartDataHelper.getInstance().getUserDataModel();
        if (arrayListCart.size() > 0) {
            layoutCart.setVisibility(View.VISIBLE);

            int totalItems = 0;
            float total = 0;
            for (int i = 0; i < arrayListCart.size(); i++) {
                totalItems = totalItems + Integer.parseInt(arrayListCart.get(i).getQuantity());
                total = total + Float.parseFloat(arrayListCart.get(i).getTotalprice());
            }
            tvTotalCartItems.setText(": " + totalItems);
            tvTotalAmount.setText("S/ " + Utility.rounddecimalFormate(total));
        } else {
            layoutCart.setVisibility(View.GONE);
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}