package genafast.com.genafasts.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.RestaurentAdapter;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.model.RestaurentModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.ConnectionDetector;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import genafast.com.genafasts.utils.SavedData;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final int PERMISSION_REQUEST_CODE = 1001;
    private final int PERMISSION_REQUEST_CODE1 = 10011;
    private final String[] permission_location = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private final String[] permission_location1 = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    @BindView(R.id.imgsetting)
    ImageView imgsetting;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.edit_search)
    EditText editSearch;
    @BindView(R.id.imgfilter)
    ImageView imgfilter;
    @BindView(R.id.rel_search)
    LinearLayout relSearch;
    @BindView(R.id.listview)
    RecyclerView listview;
    // Effectstype effectstype;
    boolean doubleBackToExitPressedOnce = false;
    Context cx;
    private double latitudecur;
    private double longitudecur;
    private String CategoryTotal = "";
    private ArrayList<RestaurentModel> arrayListRestaurant = new ArrayList<>();
    private String search = "";
    private String subCategoryName = "";
    private NavigationView navigationView;
    RestaurentAdapter adapter;
    public static boolean checkInternet(Context context) {
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppController.localeManager.setLocale(base));
        Log.e("Main Activity ====", "MainContext");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.LangChange(getApplication(), Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE));

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        cx = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LinearLayout headerLayout = (LinearLayout) navigationView.getHeaderView(0).findViewById(R.id.headerLayout);
        ImageView imgprofile = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imgprofile);
        TextView tvName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvName);

        if (UserDataHelper.getInstance().getData().size() > 0) {
            tvName.setText(UserDataHelper.getInstance().getData().get(0).getFullname());
            if (!UserDataHelper.getInstance().getData().get(0).getImage().equals(""))
                S.setImageByPicasoProfile(MainActivity.this, "http://genafast.com/uploads/restaurant/" + UserDataHelper.getInstance().getData().get(0).getImage(), imgprofile, null);
            else
                imgprofile.setImageResource(R.drawable.ic_user);
        }

        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UserDataHelper.getInstance().getData().size() > 0) {
                    S.I(MainActivity.this, ProfileActivity.class, null);
                } else {
                    S.I(MainActivity.this, LoginActivity.class, null);
                }
            }
        });

//        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH && checkInternet(MainActivity.this)) {
//                    reset();
//                    arrayListRestaurant.clear();
//                    callApiForGetSearchRestaurants(search);
//                    return true;
//                }
//                return false;
//            }
//        });

        filterRestaurantList();

//        editSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) { // TODO
//                search = s.toString();
//                if (search.length() == 0) {
//                    if (checkInternet(MainActivity.this)) {
//                        reset();
//                        arrayListRestaurant.clear();
//                        callApiForGetAllRestaurants();
//                    }
//                }
//            }
//        });
    }

    private void reset() {
        CategoryTotal = "";
    }

    private void gettingGPSLocation() {

        GPSTracker gps = new GPSTracker();
        gps.init(MainActivity.this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);

                gettingIntent();
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
            }

        } else {
            gps.showSettingsAlert();
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, permission_location, PERMISSION_REQUEST_CODE);
        ActivityCompat.requestPermissions(this, permission_location1, PERMISSION_REQUEST_CODE1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                    finish();
                } else requestPermission();
            }
        }
        if (requestCode == PERMISSION_REQUEST_CODE1) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                    finish();
                } else requestPermission();
            }
        }
    }

    private void callApiForGetSearchRestaurants(String res_name) {
        S.E("res_name : " + res_name);
        new JSONParser(this).parseRetrofitRequestWithautProgress(ApiClient.getClient().create(ApiInterface.class).getSearchRestaurant(
                Utility.getSharedPreferences(cx,Constants.CITY_NAME),
                TimeZone.getDefault().getID(),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur),
                res_name,
                "50",
                "1",
                SavedData.getRadius()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("get search restaurant resposne : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equals("Success")) {
                        arrayListRestaurant.clear();
                        JSONArray Restaurant_list = jsonObject.getJSONArray("Restaurant_list");
                        for (int i = 0; i < Restaurant_list.length(); i++) {
                            JSONObject json = Restaurant_list.getJSONObject(i);
                            RestaurentModel restaurentModel = new RestaurentModel();
                            restaurentModel.setId(json.getString("id"));
                            restaurentModel.setName(json.getString("name"));
                            restaurentModel.setLat(json.getString("lat"));
                            restaurentModel.setLon(json.getString("lon"));
                            restaurentModel.setDistance(json.getString("distance"));
                            restaurentModel.setOpen_time(json.getString("open_time"));
                            restaurentModel.setClose_time(json.getString("close_time"));
                            restaurentModel.setCurrency(json.getString("currency"));
                            restaurentModel.setDelivery_time(json.getString("delivery_time"));
                            restaurentModel.setImage(json.getString("image"));
                            restaurentModel.setRatting(json.getString("ratting"));
                            restaurentModel.setRes_status(json.getString("res_status"));
                            /*try {
                                JSONArray jCategory = json.getJSONArray("Category");
                                String[] temprory = new String[jCategory.length()];
                                for (int j = 0; j < jCategory.length(); j++) {
                                    temprory[j] = jCategory.getString(j);
                                    CategoryTotal += temprory[j];
                                    Log.e("catname12121", "" + CategoryTotal);
                                    restaurentModel.setCategory(temprory);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/
                            arrayListRestaurant.add(restaurentModel);
                        }

                        listview.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        RestaurentAdapter adapter = new RestaurentAdapter(MainActivity.this, arrayListRestaurant);
                        /*adapter.setOnLoadMoreListener(new RestaurentAdapter().OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                pageCount = pageCount + 1;
                                new LoadMoreData().execute();
                            }
                        });*/
                        listview.setAdapter(adapter);
                    } else {
                        arrayListRestaurant.clear();
                        showAlertDialog(getApplication().getString(R.string.no_restaurant));
                        //  S.showAlertDialog(getApplication(),getApplication().getString(R.string.no_restaurant));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callApiForGetAllRestaurants() {

        new JSONParser(this).parseRetrofitRequestWithautProgress(ApiClient.getClient().create(ApiInterface.class).getAllRestaurant(
                TimeZone.getDefault().getID(),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur),
                Utility.getSharedPreferences(cx,Constants.CITY_NAME),
                "100",
                "1",
                SavedData.getRadius()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("get all restaurant resposne : " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.getString("status").equals("Success")) {
                        JSONArray Restaurant_list = jsonObject.getJSONArray("Restaurant_list");

                        arrayListRestaurant.clear();

                        for (int i = 0; i < Restaurant_list.length(); i++) {
                            JSONObject json = Restaurant_list.getJSONObject(i);
                            RestaurentModel restaurentModel = new RestaurentModel();
                            restaurentModel.setId(json.getString("id"));
                            restaurentModel.setName(json.getString("name"));
                            restaurentModel.setLat(json.getString("lat"));
                            restaurentModel.setLon(json.getString("lon"));
                            restaurentModel.setDistance(json.getString("distance"));
                            restaurentModel.setOpen_time(json.getString("open_time"));
                            restaurentModel.setClose_time(json.getString("close_time"));
                            restaurentModel.setCurrency(json.getString("currency"));
                            restaurentModel.setDelivery_time(json.getString("delivery_time"));
                            restaurentModel.setImage(json.getString("image"));
                            restaurentModel.setRatting(json.getString("ratting"));
                            restaurentModel.setRes_status(json.getString("res_status"));

                            try {
                                JSONArray jCategory = json.getJSONArray("Category");
                                String[] temprory = new String[jCategory.length()];
                                for (int j = 0; j < jCategory.length(); j++) {
                                    temprory[j] = jCategory.getString(j);
                                    String CategoryTotal = temprory[j];
                                    Log.e("catname12121", "" + CategoryTotal);
                                    restaurentModel.setCategory(temprory);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            arrayListRestaurant.add(restaurentModel);
                        }

                        listview.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                         adapter = new RestaurentAdapter(MainActivity.this, arrayListRestaurant);
                        listview.setAdapter(adapter);

                    } else {
                        arrayListRestaurant.clear();
                        // S.showAlertDialog(getApplication(),getApplication().getString(R.string.no_restaurant));
                        showAlertDialog(getApplication().getString(R.string.no_restaurant));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void gettingIntent() {
        Intent i = getIntent();
        String subCategoryId = "";
        if (i.getStringExtra("sub_category_name") != null) {
            subCategoryName = i.getStringExtra("sub_category_name");
        }

        if (i.getStringExtra("sub_category_id") != null) {
            subCategoryId = i.getStringExtra("sub_category_id");
        }

        if (subCategoryName.equals("")) {
            arrayListRestaurant.clear();
            callApiForGetAllRestaurants();
        } else {
            arrayListRestaurant.clear();
           // filter(subCategoryName);
            callApiForGetSearchRestaurants(subCategoryId);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utility.ShowToastMessage(cx, cx.getResources().getString(R.string.str_bk_pressed));

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_setting) {
            S.I(MainActivity.this, SettingActivity.class, null);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_search) {
            S.I_clear(MainActivity.this, MainActivity.class, null);
        } else if (id == R.id.nav_map) {
            S.I(MainActivity.this, MapActivity.class, null);
        } else if (id == R.id.nav_restaurent) {
            S.I(MainActivity.this, MostRatedRestaurant.class, null);
        } else if (id == R.id.nav_cusine) {
            S.I(MainActivity.this, CategoriesActivity.class, null);
        } else if (id == R.id.nav_fav) {
            S.I(MainActivity.this, FavouriteActivity.class, null);
        } else if (id == R.id.nav_share) {
            String url1 = "https://play.google.com/store/apps/details?id=" + getPackageName();
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Food Delivery");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, url1);
            startActivity(Intent.createChooser(intent, "Share Food Delivery with"));
        } else if (id == R.id.nav_myaddress) {
            S.I(MainActivity.this, AddressActivity.class, null);
        } else if (id == R.id.nav_myorder) {
            S.I(MainActivity.this, MyOrderActivity.class, null);
        } else if (id == R.id.nav_terms) {
            S.I(MainActivity.this, TermconditionActivity.class, null);
        } else if (id == R.id.nav_cng_lang) {
            S.I(MainActivity.this, LanguageChangeActivity.class, null);
        } else if (id == R.id.nav_about) {
            S.I(MainActivity.this, AboutusActivity.class, null);
        } else if (id == R.id.nav_signout) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create(); // Read
            // Update
            alertDialog.setTitle(getString(R.string.log_out));
            alertDialog.setMessage(getString(R.string.error_logout));
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.conti), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utility.clearsharedpreference(getApplication());
                    UserDataHelper.getInstance().delete();
                    SavedData.clear();
                    S.I_clear(getApplication(), MainActivity.class, null);
                }
            });
            alertDialog.show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideItem();
        if (Utility.getSharedPreferences(getApplication(), Constants.CITY_NAME) != null && !
                Utility.getSharedPreferences(getApplication(), Constants.CITY_NAME).equals("")) {
            if (checkPermission()) {
                gettingGPSLocation();
            } else {
                requestPermission();
            }
        } else {
            S.T(MainActivity.this, getApplication().getString(R.string.txt_select_city));
            S.I(MainActivity.this, SettingActivity.class, null);
        }
    }

    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();
        if (UserDataHelper.getInstance().getData().size() > 0) {
            nav_Menu.findItem(R.id.nav_myorder).setVisible(true);
            nav_Menu.findItem(R.id.nav_signout).setVisible(true);
            nav_Menu.findItem(R.id.nav_myaddress).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.nav_myorder).setVisible(false);
            nav_Menu.findItem(R.id.nav_signout).setVisible(false);
        }
    }

    public void showAlertDialog(String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(cx).create();
        alertDialog.setTitle(cx.getResources().getString(R.string.alert));
        alertDialog.setMessage(message);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, cx.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }


    private void filterRestaurantList(){

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<RestaurentModel> filterdNames = new ArrayList<>();
        for (RestaurentModel s : arrayListRestaurant) {
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterdNames);
    }

//    private void EffectDialog() {
//        final NiftyDialogBuilder db = NiftyDialogBuilder.getInstance(this);
//        effectstype = Effectstype.SlideBottom;
//        db.withTitle(getApplication().getResources().getString(R.string.alert))
//                .withMessage(getApplication().getResources().getString(R.string.no_restaurant))
//                .withDialogColor(R.color.colorPrimary)
//                .withMessageColor(R.color.white)
//                .isCancelableOnTouchOutside(false)
//                .withButton1Text(getApplication().getString(R.string.ok))
//                .withEffect(effectstype)
//                .setButton1Click(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        db.dismiss();
//                    }
//                })
//                .show();
//
//
//    }
}
