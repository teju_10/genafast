package genafast.com.genafasts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.OrderDetailAdapter;
import genafast.com.genafasts.model.orderDetailGetSet;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;


public class OrderSpecification extends BaseActivity {
    private ListView list_order;
    private ArrayList<orderDetailGetSet> data;
    private TextView txt_FinalAns;
    String Order_ID = "";

    @Override
    protected int getContentResId() {
        return R.layout.activity_order_specification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Order Specifications");
        }else{
            setToolbarWithBackButton("Especificaciones del pedido");
        }

       // setToolbarWithBackButton(getApplication().getString(R.string.txt_order_specifications));

        initViews();

        Intent i = getIntent();

        String tempOrderID = i.getStringExtra("OrderId");

        settingData(tempOrderID);
    }

    private void initViews() {
        list_order = findViewById(R.id.list_order);
        TextView txt_total_tittle = findViewById(R.id.txt_total_tittle);
        TextView txt_title = findViewById(R.id.txt_title);
        txt_FinalAns = findViewById(R.id.txt_finalans);
    }


    private void settingData(String orderID) {
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).order_item_details(
                orderID
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("order specification response : " + response);
                try {
                    //getting the whole json object from the response
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("success").equals("1")) {
                        data = new ArrayList<>();
                        orderDetailGetSet getSet;
                        JSONObject jo_order = obj.getJSONObject("Order");
                        String total_amount = jo_order.getString("order_amount");

                        JSONArray ja_item = jo_order.getJSONArray("item_name");
                        for (int i = 0; i < ja_item.length(); i++) {
                            getSet = new orderDetailGetSet();
                            JSONObject jo_item = ja_item.getJSONObject(i);
                            getSet.setItemName(jo_item.getString("name"));
                            getSet.setItemPrice(Float.valueOf(jo_item.getString("amount")));
                            getSet.setItemQuantity(jo_item.getString("qty"));
                            data.add(getSet);
                        }

                        updateUI(total_amount);
                    } else {
                        Toast.makeText(OrderSpecification.this, obj.getString("order"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI(String total) {
        OrderDetailAdapter adapter = new OrderDetailAdapter(data, OrderSpecification.this);
        list_order.setAdapter(adapter);
        float roundTotal = Utility.rounddecimalFormate(Float.valueOf(total));
        txt_FinalAns.setText("S/ "+String.valueOf(roundTotal));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
