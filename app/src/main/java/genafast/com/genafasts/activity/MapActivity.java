package genafast.com.genafasts.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.model.CustomMarker;
import genafast.com.genafasts.model.RestaurentModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.ConnectionDetector;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import genafast.com.genafasts.utils.SavedData;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    private final String[] permission_location = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.imgsetting)
    ImageView imgsetting;
    @BindView(R.id.btn_more)
    ImageButton btnMore;
    private HashMap<CustomMarker, Marker> markersHashMap;
    private double latitudecur;
    private double longitudecur;
    private GoogleMap googleMap;
    private ArrayList<RestaurentModel> arrayListRestaurant = new ArrayList<>();
    private Iterator<Map.Entry<CustomMarker, Marker>> iter;
    private String[] separateddata;
    private double destlat;
    private double destlng;

    @Override
    protected int getContentResId() {
        return R.layout.activity_map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);

        setToolbarWithBackButton(getApplication().getString(R.string.txt_search_on_map));

        MapsInitializer.initialize(getApplicationContext());

        initilizeMap();

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            markersHashMap = new HashMap<>();
            GPSTracker gps = new GPSTracker();
            gps.init(MapActivity.this);
            if (gps.canGetLocation()) {
                try {
                    latitudecur = gps.getLatitude();
                    longitudecur = gps.getLongitude();
                } catch (NumberFormatException e) {
                    // TODO: handle exception
                }
            } else {
                gps.showSettingsAlert();
            }
        } else {
            S.T(MapActivity.this, getString(R.string.no_internet));
        }
    }

    private void initilizeMap() {

        SupportMapFragment supportMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment));
        supportMapFragment.getMapAsync(this);
        SharedPreferences sharedPreferences = getSharedPreferences("location", 0);

        try {
            int locationCount = sharedPreferences.getInt("locationCount", 0);
        } catch (NumberFormatException e) {
            // TODO: handle exception
        }

        (findViewById(R.id.mapFragment)).getViewTreeObserver()
                .addOnGlobalLayoutListener(new android.view.ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        if (android.os.Build.VERSION.SDK_INT >= 16) {
                            (findViewById(R.id.mapFragment)).getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            (findViewById(R.id.mapFragment)).getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }

                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(), R.string.no_map, Toast.LENGTH_SHORT).show();
            }
            this.googleMap = googleMap;
            initializeUiSettings();
            initializeMapLocationSettings();
            initializeMapTraffic();
            initializeMapType();
            initializeMapViewSettings();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            callApiForGetAllReataurant();
        } catch (NullPointerException | NumberFormatException e) {
            // TODO: handle exception
        }
    }

    private void callApiForGetAllReataurant() {
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getAllRestaurant(
                TimeZone.getDefault().getID(),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur),
                Utility.getSharedPreferences(getApplication(), Constants.CITY_NAME),
                "50",
                "1",
                SavedData.getRadius()
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("get all restaurant response : " + response);
                try {
                    JSONArray jObject = new JSONArray(response);
                    JSONObject obj = jObject.getJSONObject(0);
                    if (obj.getString("status").equals("Success")) {
                        JSONArray jsonArray = obj.getJSONArray("Restaurant_list");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            RestaurentModel temp = new RestaurentModel();
                            JSONObject Obj = jsonArray.getJSONObject(i);
                            temp.setId(Obj.getString("id"));
                            temp.setName(Obj.getString("name"));
                            temp.setLat(Obj.getString("lat"));
                            temp.setLon(Obj.getString("lon"));
                            temp.setDistance(Obj.getString("distance"));
                            temp.setOpen_time(Obj.getString("open_time"));
                            temp.setClose_time(Obj.getString("close_time"));
                            temp.setCurrency(Obj.getString("currency"));
                            temp.setDelivery_time(Obj.getString("delivery_time"));
                            temp.setImage(Obj.getString("image"));
                            temp.setRatting(Obj.getString("ratting"));
                            temp.setRes_status(Obj.getString("res_status"));
                            try {
                                JSONArray jCategory = Obj.getJSONArray("Category");
                                String[] temprory = new String[jCategory.length()];
                                for (int j = 0; j < jCategory.length(); j++) {
                                    temprory[j] = jCategory.getString(j);
                                    String CategoryTotal = temprory[j];
                                    Log.e("catname12121", "" + CategoryTotal);
                                }
                                temp.setCategory(temprory);
                                arrayListRestaurant.add(temp);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            arrayListRestaurant.add(temp);
                        }
                        setCustomMarkerOnePosition();
                    } else if (obj.getString("status").equals("Failed")) {
                        final String error = obj.getString("error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MapActivity.this, error, Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MapActivity.this, getString(R.string.txt_try_later), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setCustomMarkerOnePosition() {
        Log.d("sizerest", "" + arrayListRestaurant.size());
        for (int i = 0; i < arrayListRestaurant.size(); i++) {

            try {
                String lat1 = arrayListRestaurant.get(i).getLat();
                String lng1 = arrayListRestaurant.get(i).getLon();
                String name = arrayListRestaurant.get(i).getName() + ":" + arrayListRestaurant.get(i).getId() + ":" + arrayListRestaurant.get(i).getRatting();
                String idf = arrayListRestaurant.get(i).getId();
                String ratting = arrayListRestaurant.get(i).getRatting();
                Log.d("lat1", "" + lat1);
                Log.d("lng1", "" + lng1);
                S.E("lat1 : " + lat1);
                S.E("lng1 : " + lng1);
                CustomMarker customMarkerOne = new CustomMarker("markerOne", Double.parseDouble(lat1), Double.parseDouble(lng1));

                if (arrayListRestaurant.get(i).getCategory() != null) {
                    StringBuilder sb = new StringBuilder();
                    for (String s : arrayListRestaurant.get(i).getCategory()) {
                        sb.append(s).append(",");
                    }

                    String cat = sb.toString().replace("\"", "").replace("[", "").replace("]", "");
                    if (cat.endsWith(",")) {
                        cat = cat.substring(0, cat.length() - 1);
                    }


                    // addMarker(customMarkerOne);
                    MarkerOptions markerOptions = new MarkerOptions().position(

                            new LatLng(customMarkerOne.getCustomMarkerLatitude(), customMarkerOne.getCustomMarkerLongitude()))
                            .icon(BitmapDescriptorFactory.defaultMarker())
                            .snippet(arrayListRestaurant.get(i).getName() + "-:" +
                                    cat + "-:" +
                                    arrayListRestaurant.get(i).getDistance() + "-:" +
                                    arrayListRestaurant.get(i).getOpen_time() + "-:" +
                                    arrayListRestaurant.get(i).getClose_time() + "-:" +
                                    arrayListRestaurant.get(i).getDelivery_time() + "-:" +
                                    arrayListRestaurant.get(i).getRatting() + "-:" +
                                    arrayListRestaurant.get(i).getImage() + "-:" +
                                    arrayListRestaurant.get(i).getRes_status())
                            .title(arrayListRestaurant.get(i).getName() + ":" + arrayListRestaurant.get(i).getId() + ":"
                                    + arrayListRestaurant.get(i).getDistance() + ":" + arrayListRestaurant.get(i).getAddress() + ":"
                                    + arrayListRestaurant.get(i).getLat() + ":" + arrayListRestaurant.get(i).getLon());

                    final Marker newMark = googleMap.addMarker(markerOptions);
                    //newMark.showInfoWindow();

                    addMarkerToHashMap(customMarkerOne, newMark);
                    zoomToMarkers(btnMore);
                }
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }

            //sohelkhan837@gmail
            //9131020207

            googleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());

            // on click of google map marker
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    try {
                        String title = marker.getTitle();
                        separateddata = title.split(":");
                        S.E("separateddata[0] : : : " + separateddata[0]);
                        S.E("separateddata[1] : : : " + separateddata[1]);
                        S.E("separateddata[2] : : : " + separateddata[2]);

                        String addressmap = separateddata[3].toLowerCase();
                        destlat = Double.parseDouble(separateddata[4]);
                        destlng = Double.parseDouble(separateddata[5]);
                        /*Intent intent = new Intent(MapActivity.this, MainActivity.class);
                        intent.putExtra("res_id", "" + separateddata[1]);
                        intent.putExtra("distance", "" + separateddata[2]);
                        startActivity(intent);*/
                        Utility.setSharedPreference(getApplication(), Constants.RESTAURANT_ID, separateddata[1]);
                        Bundle bundle = new Bundle();
                        bundle.putString("restaurent_name", separateddata[0]);
                        bundle.putString("distance", separateddata[2]);
                        S.I(MapActivity.this, RestaurantDetailActivity.class, bundle);

                    } catch (NullPointerException | NumberFormatException e) {
                        e.printStackTrace();
                        // TODO: handle exception
                    }

                }
            });


        }


    }

    private void zoomAnimateLevelToFitMarkers(int padding) {
        iter = markersHashMap.entrySet().iterator();
        LatLngBounds.Builder b = new LatLngBounds.Builder();

        LatLng ll = null;
        while (iter.hasNext()) {
            Map.Entry mEntry = iter.next();
            CustomMarker key = (CustomMarker) mEntry.getKey();
            ll = new LatLng(key.getCustomMarkerLatitude(), key.getCustomMarkerLongitude());
            b.include(ll);
        }
        LatLngBounds bounds = b.build();
        Log.d("bounds", "" + bounds);

        // Change the padding as per needed
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 400, 400, 12);

        googleMap.animateCamera(cu);
        googleMap.moveCamera(cu);

    }

    private void zoomToMarkers(View v) {
        zoomAnimateLevelToFitMarkers(120);
    }

    private void addMarkerToHashMap(CustomMarker customMarker, Marker marker) {
        setUpMarkersHashMap();
        markersHashMap.put(customMarker, marker);
    }

    private void setUpMarkersHashMap() {
        if (markersHashMap == null) {
            markersHashMap = new HashMap<>();
        }
    }

    private void initializeUiSettings() {
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    private void initializeMapLocationSettings() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermission();
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    private void initializeMapTraffic() {
        googleMap.setTrafficEnabled(true);
    }

    private void initializeMapType() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    private void initializeMapViewSettings() {
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(false);
    }

    private void requestPermission() {

        int PERMISSION_REQUEST_CODE = 999;
        ActivityCompat.requestPermissions(this, permission_location, PERMISSION_REQUEST_CODE);

    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;
        boolean not_first_time_showing_info_window;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.item_restaurant, null);
            myContentsView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public View getInfoWindow(Marker marker) {


            String named = marker.getSnippet();
            String tu = marker.getTitle();
            S.E("marker.getTitle() : " + marker.getTitle());
            if (tu.equals("Current Location")) {
                myContentsView.setVisibility(View.INVISIBLE);
                return null;
            } else {

                String[] sept = named.split("-:");
                Log.e("data", sept[0] + "::" + sept[1] + "::" + sept[2] + "::" + sept[3] + "::" + sept[4] + "::" + sept[5] + "::" + sept[6] + "::" + sept[7] + "::" + sept[8]);
                TextView txt_name = myContentsView.findViewById(R.id.txt_name);
                txt_name.setText("" + sept[0]);

                TextView txt_address = myContentsView.findViewById(R.id.txt_category);
                txt_address.setText("" + sept[1]);


                TextView txt_status = myContentsView.findViewById(R.id.txt_status);
                TextView txt_time = myContentsView.findViewById(R.id.time);
                String status = sept[8];
                Log.e("res_status", "" + status);


                if (status.equals("open")) {

                    txt_status.setText(R.string.opentill);
                    txt_time.setText("" + sept[3]);

                } else if (status.equals("closed")) {
                    Log.e("dataimage", "" + sept[4]);
                    txt_status.setText(R.string.closetill);
                    txt_time.setText("" + sept[4]);

                }


                TextView txt_deltime = myContentsView.findViewById(R.id.txt_distance);
                txt_deltime.setText("" + sept[5]);

                TextView txt_rating = myContentsView.findViewById(R.id.txt_rating);
                txt_rating.setText("" + sept[6]);

                ImageView img_near = myContentsView.findViewById(R.id.image);
                Picasso.with(MapActivity.this).load(getString(R.string.link) + getString(R.string.imagepath) + sept[7].replace(" ", "%20")).into(img_near);

                return myContentsView;
            }
        }

        //		Use getInfoWindow instead of getInfoContents to provide full info window.
        @Override
        public View getInfoContents(Marker marker) {


            // TODO Auto-generated method stub
            return null;
        }

    }
}
