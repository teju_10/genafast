package genafast.com.genafasts.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.adapter.RestaurentAdapter;
import genafast.com.genafasts.model.RestaurentModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.GPSTracker;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;
import genafast.com.genafasts.utils.SavedData;

public class MostRatedRestaurant extends BaseActivity {

    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private double latitudecur;
    private double longitudecur;
    private ArrayList<RestaurentModel> arrayListRestaurant = new ArrayList<>();

    @Override
    protected int getContentResId() {
        return R.layout.activity_mostrated_restaurant;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);


        if(Utility.getSharedPreferences(getApplication(), Constants.LANGUAGE).equals("en")){
            setToolbarWithBackButton("Most Rated Restaurant");
        }else{
            setToolbarWithBackButton("Restaurante más calificado");
        }
       // setToolbarWithBackButton(getApplication().getString(R.string.txt_most_rated_restaurant));
    }

    private void gettingGPSLocation() {
        GPSTracker gps = new GPSTracker();
        gps.init(MostRatedRestaurant.this);        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {
                latitudecur = gps.getLatitude();
                longitudecur = gps.getLongitude();
                Log.w("Current Location", "Lat: " + latitudecur + "Long: " + longitudecur);

                callApiForGetMostRatedRestaurant();
            } catch (NullPointerException | NumberFormatException e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } else {
            gps.showSettingsAlert();
        }


    }

    private void callApiForGetMostRatedRestaurant() {
        new JSONParser(this).parseRetrofitRequest(ApiClient.getClient().create(ApiInterface.class).getMostRatedRestaurant(
                TimeZone.getDefault().getID(),
                String.valueOf(latitudecur),
                String.valueOf(longitudecur),
                Utility.getSharedPreferences(getApplication(),Constants.CITY_NAME),
                "50",
                "1",
                SavedData.getRadius(),
                "ratting"
        ), new Helper() {
            @Override
            public void backResponse(String response) {
                S.E("most rated restaurant response : " + response);

                try {
                    arrayListRestaurant.clear();
                    JSONArray jObject = new JSONArray(response);
                    JSONObject obj = jObject.getJSONObject(0);
                    if (obj.getString("status").equals("Success")) {
                        JSONArray jsonArray = obj.getJSONArray("Restaurant_list");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            RestaurentModel temp = new RestaurentModel();
                            JSONObject Obj = jsonArray.getJSONObject(i);
                            temp.setId(Obj.getString("id"));
                            temp.setName(Obj.getString("name"));
                            temp.setLat(Obj.getString("lat"));
                            temp.setLon(Obj.getString("lon"));
                            temp.setDistance(Obj.getString("distance"));
                            temp.setOpen_time(Obj.getString("open_time"));
                            temp.setClose_time(Obj.getString("close_time"));
                            temp.setCurrency(Obj.getString("currency"));
                            temp.setDelivery_time(Obj.getString("delivery_time"));
                            temp.setImage(Obj.getString("image"));
                            temp.setRatting(Obj.getString("ratting"));
                            temp.setRes_status(Obj.getString("res_status"));
                            try {
                                JSONArray jCategory = Obj.getJSONArray("Category");
                                String[] temprory = new String[jCategory.length()];
                                for (int j = 0; j < jCategory.length(); j++) {
                                    temprory[j] = jCategory.getString(j);
                                    String CategoryTotal = temprory[j];
                                    Log.e("catname12121", "" + CategoryTotal);
                                    temp.setCategory(temprory);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            arrayListRestaurant.add(temp);
                        }
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        RestaurentAdapter restaurentAdapter = new RestaurentAdapter(MostRatedRestaurant.this, arrayListRestaurant);
                        /*restaurentAdapter.setOnLoadMoreListener(new restaurentadapter.OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                pageCount = pageCount + 1;
                                //Load more data for reyclerview
                                new LoadMoreData().execute();

                            }
                        });*/
                        recyclerView.setAdapter(restaurentAdapter);

                    } else {
                        S.E("Please try after sometime.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        gettingGPSLocation();
    }
}

