package genafast.com.genafasts.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;

public class LanguageChangeActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    LinearLayout btn_spanish, btn_english;
    TextView tv_span, tv_eng;


    @Override
    protected int getContentResId() {
        return R.layout.activity_change_language;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Init();
    }

    //    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_change_language);
//        context = this;
//        Init();
//    }

    private void Init() {
        btn_spanish = (LinearLayout) findViewById(R.id.btn_spanish);
        btn_english = (LinearLayout) findViewById(R.id.btn_english);
        tv_span = (TextView) findViewById(R.id.tv_span);
        tv_eng = (TextView) findViewById(R.id.tv_eng);

        btn_spanish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_span.setTextColor(getResources().getColor(R.color.black));
                tv_eng.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_spanish.setBackgroundResource(R.drawable.edit_btn_blue);
                btn_english.setBackgroundResource(R.drawable.button_blueboarder);

                Utility.setSharedPreference(context, Constants.LANGUAGE, "es");
               // Utility.setSharedPreference(context, Constants.SPL_CHK, "1");
            }
        });
        btn_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_eng.setTextColor(getResources().getColor(R.color.black));
                tv_span.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_english.setBackgroundResource(R.drawable.edit_btn_blue);
                btn_spanish.setBackgroundResource(R.drawable.button_blueboarder);
                Utility.setSharedPreference(context, Constants.LANGUAGE, "en");
              //  Utility.setSharedPreference(context, Constants.SPL_CHK, "1");
            }
        });

        findViewById(R.id.btn_sl).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sl:
                if(Utility.getSharedPreferences(context,Constants.SPL_CHK).equals("1")){
                    startActivity(new Intent(context, MainActivity.class));
                }else{
                    startActivity(new Intent(context, LoginActivity.class));
                }

                finish();
                Utility.activityTransition(context);
                break;
        }

    }
}
