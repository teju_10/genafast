package genafast.com.genafasts.data.datahelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import genafast.com.genafasts.data.DataManager;
import genafast.com.genafasts.data.model.CartDataModel;

public class CartDataHelper {
    private static CartDataHelper instance;
    private SQLiteDatabase db;
    private DataManager dm;
    Context cx;

    public CartDataHelper(Context cx) {
        instance = this;
        this.cx = cx;
        dm = new DataManager(cx, DataManager.DATABASE_NAME, null, DataManager.DATABASE_VERSION);
    }

    private boolean isExist(CartDataModel model) {
        read();
        Cursor cur = db.rawQuery("select * from " + CartDataModel.TABLE_NAME + " where " + CartDataModel.KEY_FOOD_ID + "='" + model.getFoodid() + "'", null);
        if (cur.moveToFirst()) {
            return true;
        }
        return false;
    }

    public static CartDataHelper getInstance() {
        return instance;
    }

    public void open() {
        db = dm.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public void read() {
        db = dm.getReadableDatabase();
    }

    public void insertUserDataModel(CartDataModel model) {
        open();
        ContentValues values = new ContentValues();
        values.put(CartDataModel.KEY_RESTAURANT_ID, model.getRestaurantId());
        values.put(CartDataModel.KEY_FOOD_ID, model.getFoodid());
        values.put(CartDataModel.KEY_FOOD_NAME, model.getFoodname());
        values.put(CartDataModel.KEY_FOOD_PRICE, model.getFoodprice());
        values.put(CartDataModel.KEY_TOTAL_PRICE, model.getTotalprice());
        values.put(CartDataModel.KEY_FOOD_DESC, model.getFooddesc());
        values.put(CartDataModel.KEY_FOOD_INSTRUCTION, model.getFoodInstrction());
        values.put(CartDataModel.KEY_QUANTITY, model.getQuantity());

        if (!isExist(model)) {
            Log.e("sohel ", "insert successfully");
            db.insert(CartDataModel.TABLE_NAME, null, values);
        } else {
            Log.e("sohel ", "update successfully" + model.getFoodid());
            db.update(CartDataModel.TABLE_NAME, values, CartDataModel.KEY_FOOD_ID + "=" + model.getFoodid(), null);
        }
        close();
    }

    public ArrayList<CartDataModel> getUserDataModel() {
        ArrayList<CartDataModel> userItem = new ArrayList<CartDataModel>();
        read();
        Cursor clientCur = db.rawQuery("select * from " + CartDataModel.TABLE_NAME, null);

        if (clientCur != null && clientCur.getCount() > 0) {
            clientCur.moveToFirst();
            do {
                CartDataModel model = new CartDataModel();
                model.setRestaurantId(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_RESTAURANT_ID)));
                model.setFoodid(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_ID)));
                model.setFoodname(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_NAME)));
                model.setFoodprice(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_PRICE)));
                model.setTotalprice(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_TOTAL_PRICE)));
                model.setFooddesc(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_DESC)));
                model.setFoodInstrction(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_INSTRUCTION)));
                model.setQuantity(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_QUANTITY)));
                userItem.add(model);
            } while ((clientCur.moveToNext()));
            clientCur.close();
        }
        close();
        return userItem;
    }

    public ArrayList<CartDataModel> getUserDataModelSingle(String cartId) {
        ArrayList<CartDataModel> userItem = new ArrayList<CartDataModel>();
        read();
        Cursor clientCur = db.rawQuery("select * from " + CartDataModel.TABLE_NAME + " where " + CartDataModel.KEY_FOOD_ID + "='" + cartId + "'", null);

        if (clientCur != null && clientCur.getCount() > 0) {
            clientCur.moveToFirst();
            do {
                CartDataModel model = new CartDataModel();
                model.setFoodid(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_ID)));
                model.setFoodname(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_NAME)));
                model.setFoodprice(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_PRICE)));
                model.setTotalprice(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_TOTAL_PRICE)));
                model.setFooddesc(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_DESC)));
                model.setQuantity(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_QUANTITY)));
                model.setFoodInstrction(clientCur.getString(clientCur.getColumnIndex(CartDataModel.KEY_FOOD_INSTRUCTION)));
                userItem.add(model);
            } while ((clientCur.moveToNext()));
            clientCur.close();
        }
        close();
        return userItem;
    }

    public void delete() {
        open();
        db.delete(CartDataModel.TABLE_NAME, null, null);
        close();
    }

    public void deleteSingle(String foodId) {
        open();
        db.delete(CartDataModel.TABLE_NAME, CartDataModel.KEY_FOOD_ID + "=" + foodId, null);
        close();
    }
}