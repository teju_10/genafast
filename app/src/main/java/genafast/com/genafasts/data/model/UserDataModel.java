package genafast.com.genafasts.data.model;

import android.database.sqlite.SQLiteDatabase;

public class UserDataModel {
    public static final String TABLE_NAME = "user_data_model";
    public static final String KEY_ID = "_id";
    public static final String KEY_UserID = "UserID";
    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_UserEmail = "UserEmail";
    public static final String KEY_DEVICEID = "device_id";
    public static final String KEY_Phone = "UserPhone";
    public static final String KEY_Referalcode = "UserReferalCode";
    public static final String KEY_UserProfile = "UserProfile";
    public static final String KEY_CreatedAt = "CreatedAt";
    public static final String KEY_LoginWith = "LoginWith";

    public static void creteTable(SQLiteDatabase db) {
        String CREATE_TABLE = "create table " + TABLE_NAME + " ("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_UserID + " text,"
                + KEY_USERNAME + " text, "
                + KEY_UserEmail + " text, "
                + KEY_Phone + " text, "
                + KEY_DEVICEID + " text, "
                + KEY_Referalcode + " text, "
                + KEY_UserProfile + " text, "
                + KEY_CreatedAt + " text, "
                + KEY_LoginWith + " text " +
                ")";
        db.execSQL(CREATE_TABLE);
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public static void dropTable(SQLiteDatabase db) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    private String userid, fullname, email, phone_no, referal_code, image, created_at, login_with,deviceID;


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getReferal_code() {
        return referal_code;
    }

    public void setReferal_code(String referal_code) {
        this.referal_code = referal_code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getLogin_with() {
        return login_with;
    }

    public void setLogin_with(String login_with) {
        this.login_with = login_with;
    }
}
