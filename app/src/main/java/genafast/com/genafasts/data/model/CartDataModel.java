package genafast.com.genafasts.data.model;

import android.database.sqlite.SQLiteDatabase;

public class CartDataModel {
    public static final String TABLE_NAME = "CartDataModel";
    public static final String KEY_ID = "_id";
    public static final String KEY_FOOD_ID = "foodid";
    public static final String KEY_RESTAURANT_ID = "restaurantId";
    public static final String KEY_FOOD_NAME = "foodname";
    public static final String KEY_FOOD_PRICE = "foodprice";
    public static final String KEY_TOTAL_PRICE = "totalprice";
    public static final String KEY_FOOD_DESC = "fooddesc";
    public static final String KEY_FOOD_INSTRUCTION = "foodinst";
    public static final String KEY_QUANTITY = "quantity";

    public String getFoodInstrction() {
        return foodInstrction;
    }

    public void setFoodInstrction(String foodInstrction) {
        this.foodInstrction = foodInstrction;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public static void creteTable(SQLiteDatabase db) {
        String CREATE_STUDENTTABLE = "create table " + TABLE_NAME + " ("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_RESTAURANT_ID + " text, "
                + KEY_FOOD_ID + " text, "
                + KEY_FOOD_NAME + " text, "
                + KEY_FOOD_PRICE + " text, "
                + KEY_TOTAL_PRICE + " text, "
                + KEY_FOOD_DESC + " text, "
                + KEY_FOOD_INSTRUCTION + " text, "
                + KEY_QUANTITY + " text " +
                ")";

        db.execSQL(CREATE_STUDENTTABLE);

    }

    public static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    String foodid, foodname, foodprice, totalprice, fooddesc, quantity,foodInstrction,restaurantId;

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFoodid() {
        return foodid;
    }

    public void setFoodid(String foodid) {
        this.foodid = foodid;
    }

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public String getFoodprice() {
        return foodprice;
    }

    public void setFoodprice(String foodprice) {
        this.foodprice = foodprice;
    }

    public String getFooddesc() {
        return fooddesc;
    }

    public void setFooddesc(String fooddesc) {
        this.fooddesc = fooddesc;
    }

}
