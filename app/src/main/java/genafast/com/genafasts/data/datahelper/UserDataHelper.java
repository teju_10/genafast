package genafast.com.genafasts.data.datahelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import genafast.com.genafasts.data.DataManager;
import genafast.com.genafasts.data.model.UserDataModel;

import static genafast.com.genafasts.data.model.UserDataModel.KEY_UserEmail;
import static genafast.com.genafasts.data.model.UserDataModel.KEY_UserID;


public class UserDataHelper {
    private static UserDataHelper instance;
    private SQLiteDatabase db;
    private DataManager dm;
    Context cx;

    public UserDataHelper(Context cx) {
        instance = this;
        this.cx = cx;
        dm = new DataManager(cx, DataManager.DATABASE_NAME, null, DataManager.DATABASE_VERSION);
    }

    private boolean isExist(UserDataModel dataModel) {
        read();
        Cursor cur = db.rawQuery("select * from " + UserDataModel.TABLE_NAME + " where " + KEY_UserID + "='" + dataModel.getUserid() + "'", null);
        if (cur.moveToFirst()) {
            return true;
        }
        return false;
    }

    public static UserDataHelper getInstance() {
        return instance;
    }

    public void open() {
        db = dm.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    public void read() {
        db = dm.getReadableDatabase();
    }

    public void insertDataModel(UserDataModel userDataModel) {
        open();
        ContentValues values = new ContentValues();
        values.put(KEY_UserID, userDataModel.getUserid());
        values.put(UserDataModel.KEY_USERNAME, userDataModel.getFullname());
        values.put(UserDataModel.KEY_UserEmail, userDataModel.getEmail());
        values.put(UserDataModel.KEY_Phone, userDataModel.getPhone_no());
        values.put(UserDataModel.KEY_DEVICEID, userDataModel.getDeviceID());
        values.put(UserDataModel.KEY_Referalcode, userDataModel.getReferal_code());
        values.put(UserDataModel.KEY_UserProfile, userDataModel.getImage());
        values.put(UserDataModel.KEY_CreatedAt, userDataModel.getCreated_at());
        values.put(UserDataModel.KEY_LoginWith, userDataModel.getLogin_with());


        if (!isExist(userDataModel)) {
            db.insert(UserDataModel.TABLE_NAME, null, values);
            Log.e("sohel", "insert successfully");
        } else {
            db.update(UserDataModel.TABLE_NAME, values, KEY_UserID + " = '" + userDataModel.getUserid() + "'", null);
            Log.e("sohel", "update successfully : " + userDataModel.getUserid());

        }
        close();
    }

    public ArrayList<UserDataModel> getData() {
        ArrayList<UserDataModel> userItem = new ArrayList<>();
        read();
        Cursor clientCur = db.rawQuery("select * from " + UserDataModel.TABLE_NAME, null);

        if (clientCur != null && clientCur.getCount() > 0) {
            clientCur.moveToFirst();
            do {
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.setUserid(clientCur.getString(clientCur.getColumnIndex(KEY_UserID)));
                userDataModel.setFullname(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_USERNAME)));
                userDataModel.setEmail(clientCur.getString(clientCur.getColumnIndex(KEY_UserEmail)));
                userDataModel.setPhone_no(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_Phone)));
                userDataModel.setDeviceID(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_DEVICEID)));
                userDataModel.setReferal_code(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_Referalcode)));
                userDataModel.setImage(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_UserProfile)));
                userDataModel.setCreated_at(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_CreatedAt)));
                userDataModel.setLogin_with(clientCur.getString(clientCur.getColumnIndex(UserDataModel.KEY_LoginWith)));

                userItem.add(userDataModel);
            } while ((clientCur.moveToNext()));
            clientCur.close();
        }
        close();
        return userItem;
    }

    public void delete() {
        open();
        db.delete(UserDataModel.TABLE_NAME, null, null);
        close();
    }

    public void delete(String userId) {   // delete query of SQL they pass to the
        open();
        db.delete(UserDataModel.TABLE_NAME, KEY_UserID + "='" + userId + "'", null);
        close();


    }

}