package genafast.com.genafasts.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import genafast.com.genafasts.data.model.CartDataModel;
import genafast.com.genafasts.data.model.UserDataModel;

public class DataManager extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 8;
    public static final String DATABASE_NAME = "FoodStation";

    public DataManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        UserDataModel.creteTable(db);
        CartDataModel.creteTable(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        UserDataModel.dropTable(db);
        CartDataModel.dropTable(db);
        onCreate(db);
    }


}