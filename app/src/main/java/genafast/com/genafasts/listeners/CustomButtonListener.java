package genafast.com.genafasts.listeners;

public interface CustomButtonListener {
	public abstract void onButtonClick(int position, String buttonText);
}