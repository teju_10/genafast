package genafast.com.genafasts.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.activity.MainActivity;
import genafast.com.genafasts.activity.PlaceOrder;
import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.model.AddressModel;
import genafast.com.genafasts.utils.ApiClient;
import genafast.com.genafasts.utils.ApiInterface;
import genafast.com.genafasts.utils.Helper;
import genafast.com.genafasts.utils.JSONParser;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    List<AddressModel> arrayList;
    Context context;
    String allresid;
    int selectedPosition = -1;

    public AddressAdapter(Context context, List<AddressModel> arrayList, String allresid) {
        this.context = context;
        this.arrayList = arrayList;
        this.allresid = allresid;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_addresses, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {

            final AddressModel model = arrayList.get(position);
            holder.tvaddress.setText(model.getAddress());
            holder.tv_address_nickname.setText(model.getAddress_nickName());
            holder.tv_cmplete_address.setText(model.getCompleteAddress());
            List<String> addressList = new ArrayList<>();
            addressList.add(model.getAddress_nickName());
            addressList.add(model.getCompleteAddress());
            addressList.add(model.getAddress());
            final String s = TextUtils.join(", ", addressList);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    S.E("Address Adapter===RestarntID" + allresid);
                    if (CartDataHelper.getInstance().getUserDataModel().size() == 0) {
                        Utility.setSharedPreference(context, Constants.RESTAURANT_ID, "");
                        context.startActivity(new Intent(context, MainActivity.class));
                    } else {
                        Intent iv = new Intent(context, PlaceOrder.class);
                        iv.putExtra("order_price", "");
                        iv.putExtra(Constants.RESTAURANT_ID, allresid);
                        iv.putExtra("lat", model.getLat());
                        iv.putExtra("lng", model.getLng());
                        iv.putExtra("address", s);
                        context.startActivity(iv);
                    }
                }
            });

            holder.address_imgdelete.setOnClickListener(new DeleteClick(position));
//            if (selectedPosition == position) {
//                holder.imgright.setVisibility(View.VISIBLE);
//            } else {
//                holder.imgright.setVisibility(View.GONE);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void dialoDeleteAddress(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                context);
        builder.setTitle(context.getResources().getString(R.string.alert));
        builder.setMessage(context.getResources().getString(R.string.alert_delete_address));
        builder.setPositiveButton(context.getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        apiCallDeleteAddress(pos);
                    }
                });
        builder.show();
    }

    private void apiCallDeleteAddress(final int pos) {
        new JSONParser(context).parseRetrofitRequestWithautProgress(ApiClient.getClient().create(ApiInterface.class).
                        apiDeleteAddress("deleteAddress", arrayList.get(pos).getId()),
                new Helper() {
                    @Override
                    public void backResponse(String response) {
                        try {
                            JSONObject result  = new JSONObject(response);
                            if(result.getString("status").equals("Success")){
                                Utility.ShowToastMessage(context, context.getResources().getString(R.string.success_delete));
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (arrayList.size() > 0) {
                            arrayList.remove(pos);
                            notifyDataSetChanged();
                        }
                    }
                });
    }

    private class DeleteClick implements View.OnClickListener {
        int pos;

        DeleteClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            dialoDeleteAddress(pos);

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvaddress, tv_address_nickname, tv_cmplete_address;
        ImageView address_imgdelete;

        public MyViewHolder(View view) {
            super(view);
            tvaddress = (TextView) view.findViewById(R.id.tvaddress);
            tv_address_nickname = (TextView) view.findViewById(R.id.tv_address_nickname);
            tv_cmplete_address = (TextView) view.findViewById(R.id.tv_cmplete_address);
            address_imgdelete = (ImageView) view.findViewById(R.id.address_imgdelete);
        }
    }

}

