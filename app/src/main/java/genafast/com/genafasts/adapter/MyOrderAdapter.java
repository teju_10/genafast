package genafast.com.genafasts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.activity.ReviewActivity;
import genafast.com.genafasts.model.MyOrderModel;

public class MyOrderAdapter extends BaseAdapter {
    final ArrayList<MyOrderModel> dat;
    Context cx;
    private LayoutInflater inflater = null;

    public MyOrderAdapter(Context cx, ArrayList<MyOrderModel> dat) {
        this.dat = dat;
        this.cx = cx;
        inflater = (LayoutInflater) cx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return dat.size();
    }

    @Override
    public Object getItem(int position) {
        return dat.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.item_my_order, parent, false);


        Button btnRate = vi.findViewById(R.id.btnRate);
        TextView txt_name = vi.findViewById(R.id.txt_name);
        ImageView star1 = vi.findViewById(R.id.star1);
        ImageView star2 = vi.findViewById(R.id.star2);
        ImageView star3 = vi.findViewById(R.id.star3);
        ImageView star4 = vi.findViewById(R.id.star4);
        ImageView star5 = vi.findViewById(R.id.star5);
        txt_name.setText(dat.get(position).getRestaurant_name());
        TextView txt_address = vi.findViewById(R.id.txt_address);
        txt_address.setText(dat.get(position).getRestaurant_address());

        TextView txt_res_name = vi.findViewById(R.id.txt_res_name);
        txt_res_name.setText(dat.get(position).getRestaurant_name().substring(0, 1));

        TextView txt_order_date = vi.findViewById(R.id.txt_order_date);
        TextView txt_order_time = vi.findViewById(R.id.txt_order_time);


        String[] datetime = dat.get(position).getDate().split(" ");
        txt_order_date.setText("Order Date: " + datetime[0]);
        txt_order_time.setText("Order Time: " + datetime[1]);

        TextView txt_total_tittle = vi.findViewById(R.id.txt_total_tittle);
        TextView txt_total = vi.findViewById(R.id.txt_total);
        txt_total.setText("S/ " + Utility.rounddecimalFormate(
                Float.valueOf(dat.get(position).getTotal_amount())));
        if (dat.get(position).getRestaurantRatting().equals("0") &&
                dat.get(position).getStatus().equals("Order is Delivered")) {
            btnRate.setVisibility(View.VISIBLE);
        } else {
            btnRate.setVisibility(View.GONE);

        }

        Utility.Rating(Float.parseFloat(dat.get(position).getRestaurantRatting()), star1, star2, star3, star4, star5, R.drawable.ic_star_b_empty,
                R.drawable.ic_star_b_half, R.drawable.ic_star_b_filled);
        btnRate.setOnClickListener(new rateClick(position));
        return vi;
    }


    private class rateClick implements View.OnClickListener {
        int pos;

        rateClick(int _pos) {
            this.pos = _pos;
        }

        @Override
        public void onClick(View view) {
            cx.startActivity(new Intent(cx, ReviewActivity.class).putExtra(Constants.RESTAURANT_ID, dat.get(pos).getRestuarantId()));
        }
    }
}