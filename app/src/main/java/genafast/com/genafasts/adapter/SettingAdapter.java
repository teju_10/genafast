package genafast.com.genafasts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.listeners.CustomButtonListener_new;
import genafast.com.genafasts.model.SettingModel;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder> {
    private List<SettingModel> moviesList;
    Context cx;
    private String cityName;
    private CustomButtonListener_new customButtonListener_new;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvcity;
        ImageView imgright;

        public MyViewHolder(View view) {
            super(view);
            tvcity = (TextView) view.findViewById(R.id.tvcity);
            imgright = (ImageView) view.findViewById(R.id.imgright);
        }
    }

    public SettingAdapter(Context cx, List<SettingModel> moviesList) {
        this.cx = cx;
        this.moviesList = moviesList;
    }

    public void setCustomButtonListener_new(CustomButtonListener_new customButtonListener_new) {
        this.customButtonListener_new = customButtonListener_new;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_setting, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SettingModel settingModel = moviesList.get(position);
        holder.tvcity.setText(settingModel.getName());

        if (Utility.getSharedPreferences(cx,Constants.CITY_NAME) != null) {
            cityName =Utility.getSharedPreferences(cx,Constants.CITY_NAME);
            if (cityName != null && !cityName.isEmpty() && !cityName.equals("null") && cityName.equals(settingModel.getName())) {
                holder.imgright.setVisibility(View.VISIBLE);
            } else {
                holder.imgright.setVisibility(View.GONE);
            }
        } else {
            holder.imgright.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customButtonListener_new != null)
                    customButtonListener_new.onButtonClick(position, "city_clicked");
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}


