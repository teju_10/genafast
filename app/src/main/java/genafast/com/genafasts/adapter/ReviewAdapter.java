package genafast.com.genafasts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.model.ReviewModel;
public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {


    private List<ReviewModel> moviesList;
    Context cx;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvtitle, tvratio, tvsubtitle;
        ImageView circleView;

        public MyViewHolder(View view) {
            super(view);
            tvtitle = (TextView) view.findViewById(R.id.tvtitle);
            tvratio = (TextView) view.findViewById(R.id.tvratio);
            tvsubtitle = (TextView) view.findViewById(R.id.tvsubtitle);
            circleView = (ImageView) view.findViewById(R.id.circleView);
        }
    }
    public ReviewAdapter(Context cx, List<ReviewModel> moviesList) {
        this.cx = cx;
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ReviewModel reviewModel = moviesList.get(position);
        holder.tvtitle.setText(reviewModel.getUsername());
        holder.tvratio.setText(reviewModel.getRatting());
        holder.tvsubtitle.setText(reviewModel.getReview_text());
        //holder.circleView.setImageResource(reviewModel.getImage());
        S.setImageByPicasoNormal(cx, reviewModel.getImage(), holder.circleView, null);

/*
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                Utility.I(cx, SubCategoryTab.class, bundle);
            }
        });
*/
    }

    @Override

    public int getItemCount() {
        return moviesList.size();
    }

}


