package genafast.com.genafasts.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.Utility.S;
import genafast.com.genafasts.Utility.Utility;
import genafast.com.genafasts.activity.RestaurantDetailActivity;
import genafast.com.genafasts.model.RestaurentModel;

public class RestaurentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    public  ArrayList<RestaurentModel> moviesList;
    private final Activity activity;

    public RestaurentAdapter(Activity a, ArrayList<RestaurentModel> moviesList) {
        activity = a;
        this.moviesList = moviesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant, parent, false);
            return new MyViewHolder(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            final RestaurentModel data = moviesList.get(position);
            ((MyViewHolder) holder).txt_name.setText((data.getName()));
            if (data.getCategory() != null) {
                StringBuilder sb = new StringBuilder();
                for (String s : data.getCategory()) {
                    sb.append(s).append(", ");
                }
                String cat = sb.toString().replace("\"", "").replace("[", "").replace("]", "");
                if (cat.endsWith(", ")) {
                    cat = cat.substring(0, cat.length() - 2);
                }
                ((MyViewHolder) holder).txt_category.setText(cat);
            }
            ((MyViewHolder) holder).txt_distance.setText((data.getDelivery_time()).replace("MIN", "").replace("MIn", "").replace("Min", "").replace(" ", ""));
            ((MyViewHolder) holder).txt_rating.setText("" + Float.parseFloat(data.getRatting()));

            double distance = Double.parseDouble(data.getDistance());

            String status = data.getRes_status();
            Log.e("res_status", "" + status);
            if (status.equals("open")) {
                ((MyViewHolder) holder).txt_status.setText(activity.getString(R.string.open_till));
                ((MyViewHolder) holder).txt_status.setTextColor(Color.parseColor("#14B457"));
                ((MyViewHolder) holder).time.setText((data.getOpen_time()));
                ((MyViewHolder) holder).time.setTextColor(Color.parseColor("#14B457"));
            } else if (status.equals("closed")) {
                ((MyViewHolder) holder).txt_status.setText(activity.getString(R.string.closetill));
                ((MyViewHolder) holder).txt_status.setTextColor(Color.parseColor("#F73E46"));
                ((MyViewHolder) holder).time.setTextColor(Color.parseColor("#F73E46"));
                ((MyViewHolder) holder).time.setText((data.getClose_time()));
            }

            holder.itemView.setOnClickListener(new MyClick(position));

            String image = data.getImage().replace(" ", "%20");

            AlphaAnimation anim = new AlphaAnimation(0, 1);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.ABSOLUTE);
            anim.setDuration(1000);
            Picasso.with(activity).load(activity.getResources().getString(R.string.link) + activity.getString(R.string.imagepath) + image).into(((MyViewHolder) holder).imageview);
            ((MyViewHolder) holder).imageview.startAnimation(anim);
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    private class MyClick implements View.OnClickListener {
        int pos;

        public MyClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            S.E("RestaurantAdap===RestID " + moviesList.get(pos).getId());
            if (!moviesList.get(pos).getId().equals("") && moviesList.get(pos).getId() != null) {
                Utility.setSharedPreference(activity, Constants.RESTAURANT_ID, moviesList.get(pos).getId());
                Bundle bundle = new Bundle();
                bundle.putString("distance", moviesList.get(pos).getDistance());
                bundle.putString("restaurent_name", moviesList.get(pos).getName());

                S.I(activity, RestaurantDetailActivity.class, bundle);
            } else {
                S.T(activity, activity.getResources().getString(R.string.servererror));
            }
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView txt_name;
        final TextView txt_category;
        final TextView txt_distance;
        final TextView txt_rating;
        final TextView txt_status;
        final TextView time;
        final ImageView imageview;

        MyViewHolder(View view) {
            super(view);
            txt_name = view.findViewById(R.id.txt_name);
            txt_category = view.findViewById(R.id.txt_category);
            txt_distance = view.findViewById(R.id.txt_distance);
            txt_rating = view.findViewById(R.id.txt_rating);
            txt_status = view.findViewById(R.id.txt_status);
            imageview = view.findViewById(R.id.image);
            time = view.findViewById(R.id.time);
        }
    }

    public void filterList(ArrayList<RestaurentModel> filterdNames) {
        this.moviesList = filterdNames;
        notifyDataSetChanged();
    }
}