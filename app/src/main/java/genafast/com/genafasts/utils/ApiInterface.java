package genafast.com.genafasts.utils;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @Multipart
    @POST("userregister.php")
    Call<ResponseBody> user_registration(
            @Part("email") RequestBody email,
            @Part("fullname") RequestBody fullname,
            @Part("password") RequestBody password,
            @Part("device_id") RequestBody device_id,
            @Part("phone_no") RequestBody phone_no,
            @Part("referral_code") RequestBody referral_code,
            @Part("file\"; filename=\"pp.png\" ") RequestBody file
    );

    @GET("userlogin.php")
    Call<ResponseBody> user_login(
            @Query("email") String email,
            @Query("password") String password,
            @Query("device_id") String device_id

    );

    @GET("userlogin.php")
    Call<ResponseBody> user_loginfacebook(
            @Query("fullname") String fullname,
            @Query("email") String email,
            @Query("phone_no") String phone_no,
            @Query("referral_code") String referral_code,
            @Query("image") String image
    );

    @GET("userlogin.php")
    Call<ResponseBody> user_google(
            @Query("fullname") String fullname,
            @Query("email") String email,
            @Query("phone_no") String phone_no,
            @Query("referral_code") String referral_code,
            @Query("image") String image
    );

    @GET("restaurant_city.php")
    Call<ResponseBody> user_city();

    @GET("getrestaurant_review.php")
    Call<ResponseBody> restaurent_review();

    @FormUrlEncoded
    @POST("postrestaurant_review.php")
    Call<ResponseBody> Postreview(
            @Field("res_id") String res_id,
            @Field("ratting") String ratting,
            @Field("user_id") String user_id,
            @Field("review_text") String review_text
    );

    @GET("restaurantlist.php")
    Call<ResponseBody> getAllRestaurant(
            @Query("timezone") String timezone,
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("location") String location,
            @Query("noofrecords") String noofrecords,
            @Query("pageno") String pageno,
            @Query("radius") String radius
    );

    @GET("restaurantlist.php")
    Call<ResponseBody> getSearchRestaurant(
            @Query("location") String location,
            @Query("timezone") String timezone,
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("search") String search,
            @Query("noofrecords") String noofrecords,
            @Query("pageno") String pageno,
            @Query("radius") String radius
    );

    @GET("restaurant_byrate.php")
    Call<ResponseBody> getMostRatedRestaurant(
            @Query("timezone") String timezone,
            @Query("lat") String lat,
            @Query("lon") String lon,
            @Query("location") String location,
            @Query("noofrecords") String noofrecords,
            @Query("pageno") String pageno,
            @Query("radius") String radius,
            @Query("short_by") String short_by
    );

    @GET("restaurant_category.php")
    Call<ResponseBody> getRestaurantCategories();

    @GET("restaurant_menu.php")
    Call<ResponseBody> getRestaurantMenu(
            @Query("res_id") String res_id
    );

    @GET("restaurant_submenu.php")
    Call<ResponseBody> getRestaurantSubMenu(
            @Query("menucategory_id") String menucategory_id
    );

    @GET("getrestaurantdetail.php")
    Call<ResponseBody> getRestaurantDetail(
            @Query("res_id") String res_id,
            @Query("lat") String lat,
            @Query("lon") String lon
    );

    @GET("order_details.php")
    Call<ResponseBody> order_details(
            @Query("order_id") String order_id
    );

    @GET("order_item_details.php")
    Call<ResponseBody> order_item_details(
            @Query("order_id") String order_id
    );

    @GET("user_order_history.php")
    Call<ResponseBody> user_order_history(
            @Query("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("bookorder.php")
    Call<ResponseBody> placeOrder(
            @Field("user_id") String user_id,
            @Field("res_id") String res_id,
            @Field("address") String address,
            @Field("lat") String lat,
            @Field("long") String lng,
            @Field("food_desc") String food_desc,
            @Field("notes") String notes,
            @Field("total_price") String total_price,
            @Field("payment_mode") String payment_mode,
            @Field("cod") String cod,
            @Field("pos") String pos,
            @Field("proof_type") String proof_type,
            @Field("dni") String dni,
            @Field("number") String number,
            @Field("ruc") String ruc,
            @Field("social_reason") String social_reason,
            @Field("comment") String comment
    );


    @FormUrlEncoded
    @POST("common.php")
    Call<ResponseBody> AddAddress(
            @Field("action") String action,
            @Field("user_id") String user_id,
            @Field("address") String address,
            @Field("lat") String lat,
            @Field("long") String lng
    );

    @FormUrlEncoded
    @POST("common.php")
    Call<ResponseBody> AddCompleteAddress(
            @Field("action") String action,
            @Field("user_id") String user_id,
            @Field("address") String address,
            @Field("complete_address") String complete_address,
            @Field("landmark") String landmark,
            @Field("nickname_address") String nickname_address,
            @Field("lat") String lat,
            @Field("long") String lng
    );

    @FormUrlEncoded
    @POST("common.php")
    Call<ResponseBody> getAddress(
            @Field("action") String action,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("common.php")
    Call<ResponseBody> getLatLong(
            @Field("action") String action,
            @Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("common.php")
    Call<ResponseBody> apiDeleteAddress(
            @Field("action") String action,
            @Field("id") String address_id
    );

}