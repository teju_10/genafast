package genafast.com.genafasts.utils;

import retrofit2.Retrofit;

/**
 * Created by abc on 09-Mar-18.
 */

public class ApiClient {

//    private static final String BASE_URL = "http://foodstations.in/api/";
    private static final String BASE_URL = "http://genafast.com/api/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }
   // admin 123456
}