package genafast.com.genafasts.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import genafast.com.genafasts.AppController;

public class SavedData {
    private static final String CITY_NAME = "cityName";
    private static final String RADIUS = "radius";

    static SharedPreferences prefs;

    public static SharedPreferences getInstance() {
        if (prefs == null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
        }
        return prefs;
    }


    public static String getRadius() {
        return getInstance().getString(RADIUS, null);
    }

    public static void saveRadius(String radius) {
        SharedPreferences.Editor editor = getInstance().edit();
        editor.putString(RADIUS, radius);
        editor.apply();
    }

    public static void clear() {
        SharedPreferences.Editor editor = getInstance().edit();
        editor.clear();
        editor.apply();
    }
}