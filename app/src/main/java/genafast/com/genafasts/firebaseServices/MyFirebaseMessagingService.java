package genafast.com.genafasts.firebaseServices;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.json.JSONObject;

import java.util.Map;

import genafast.com.genafasts.R;
import genafast.com.genafasts.activity.CompleteOrder;
import genafast.com.genafasts.activity.MainActivity;

/**
 * Created by Infograins on 9/26/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getData());
        JSONObject object = new JSONObject(remoteMessage.getData());
        Log.e(TAG, "onMessageReceived: " + object.toString());

        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                String channelId = context.getString(R.string.default_notification_channel_id);
//                NotificationChannel channel = new NotificationChannel(channelId,
//                        title, NotificationManager.IMPORTANCE_DEFAULT);
//                channel.setDescription(body);
//                mNotificationManager.createNotificationChannel(channel);
//                builder.setChannelId(channelId);
//            }

            if (remoteMessage.getData().size() > 0) {
                Map<String, String> notification = remoteMessage.getData();
                Bundle bundle = new Bundle();
                for (Map.Entry<String, String> entry : notification.entrySet()) {
                    bundle.putString(entry.getKey(), entry.getValue());
                }

                if (notification.get("type").equals("order_pickup")) {
                    sendNotification(notification.get("notification"), bundle);
                } else if (notification.get("type").equals("order_cancel")) {
                    sendNotification(notification.get("notification"), bundle);
                }else if (notification.get("type").equals("order_complete")) {
                    sendNotification(notification.get("notification"), bundle);
                } else {
                    sendNotification(notification.get("notification"), bundle);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(String messageBody, Bundle b) {

        Intent intent = new Intent();
        if (b.getString("type").equals("order_pickup")) {
            intent = new Intent(this, CompleteOrder.class);
        } else if (b.getString("type").equals("order_complete")) {
            intent = new Intent(this, MainActivity.class);
        }else if (b.getString("type").equals("order_cancel")) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);

        }
        intent.putExtras(b);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setContentTitle("You have new message")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        String channelId = "Default";
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}