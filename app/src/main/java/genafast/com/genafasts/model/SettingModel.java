package genafast.com.genafasts.model;

/**
 * Created by RedixbitUser on 3/23/2018.
 */

public class SettingModel {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
