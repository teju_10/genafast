package genafast.com.genafasts.model;

import genafast.com.genafasts.R;
import tellh.com.stickyheaderview_rv.adapter.DataBean;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;

/**
 * Created by Redixbit 2 on 06-09-2016.
 */
public class ordergetset extends DataBean {
    private String  status;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public String getItemInstruction() {
        return itemInstruction;
    }

    public void setItemInstruction(String itemInstruction) {
        this.itemInstruction = itemInstruction;
    }

    private String id;
    private String name;
    private String price;
    private String desc;
    private String itemInstruction;
    private String created_at;
   private String counting;

    public String getCounting() {
        return counting;
    }

    public void setCounting(String counting) {
        this.counting = counting;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter stickyHeaderViewAdapter) {
        return  R.layout.sticky_item_user;
    }

    public ordergetset() {
    }

    public ordergetset(String status, String image, String id, String name, String price, String desc, String created_at, String counting) {
        this.status = status;
        this.image = image;
        this.id = id;
        this.name = name;
        this.price = price;
        this.desc = desc;
        this.created_at = created_at;
        this.counting = counting;
    }
}
