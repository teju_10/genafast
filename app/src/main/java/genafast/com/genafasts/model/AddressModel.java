package genafast.com.genafasts.model;

public class AddressModel {

    public String getAddress_nickName() {
        return address_nickName;
    }

    public void setAddress_nickName(String address_nickName) {
        this.address_nickName = address_nickName;
    }

    public String getCompleteAddress() {
        return completeAddress;
    }

    public void setCompleteAddress(String completeAddress) {
        this.completeAddress = completeAddress;
    }

    /**
     * id : 6
     * user_id : 10
     * address : MANDSOUR
     * lat : 25..01
     * lng : 22.52
     * status : 1
     * created_at : 1547631724
     */


    private String id;
    private String user_id;
    private String address;
    private String address_nickName;
    private String completeAddress;
    private String lat;
    private String lng;
    private String status;
    private String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}