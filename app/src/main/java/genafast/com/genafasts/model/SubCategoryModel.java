package genafast.com.genafasts.model;

/**
 * Created by Redixbit on 3/15/2018.
 */

public class SubCategoryModel {

    private String id;
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
