package genafast.com.genafasts.model;

public class MyOrderModel {

    /**
     * order_no : 28
     * total_amount : 165.0
     * items : 2
     * date : 17-Dec-2018 14:43:14pm
     * currency :  د.إ
     * restaurant_name : Food Stations Restaurant
     * restaurant_address : airport road indore
     * status : null
     */

    private String order_no;
    private String total_amount;

    public String getRestuarantId() {
        return restuarantId;
    }

    public void setRestuarantId(String restuarantId) {
        this.restuarantId = restuarantId;
    }

    private String restuarantId;
    private String items;
    private String date;
    private String currency;
    private String restaurant_name;
    private String restaurant_address;
    private String restaurantRatting;

    public String getRestaurantRatting() {
        return restaurantRatting;
    }

    public void setRestaurantRatting(String restaurantRatting) {
        this.restaurantRatting = restaurantRatting;
    }

    private Object status;

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_address() {
        return restaurant_address;
    }

    public void setRestaurant_address(String restaurant_address) {
        this.restaurant_address = restaurant_address;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }
}
