package genafast.com.genafasts.Utility;

public class Constants {


    public static final String ADDRESS ="address";
    public static final String LATITUDE ="latitude";
    public static final String LONGITUDE ="longitude";
    public static final String CUSTOMER_LATITUDE ="clat";
    public static final String CUSTOMER_LONGITUDE ="clong";
    public static final String MAP_KEY = "AIzaSyBDXxrTAjFuZ-GYqAWhM6_EIn53PZfZgzU";
   // public static final String MAP_KEY = "AIzaSyBgfvkMHqHinHpUJthIjO_2PZyiXcFOtIo";
    public static final String ORDER_ID = "order_id";

    public static final String DEVICE_ID = "device_id";
    public static final String LANGUAGE = "language";
    public static final String LANG_PREF = "LANG_PREF";
    public static final String SPL_CHK = "SPL_CHK";
    public static final String USER_ID = "id";
    public static final String RESTAURANT_ID = "res_id";
    public static final String CITY_NAME = "city_name";
    public static final String PAYMENTTYPE = "PAYMENTTYPE";
    public static final String NOTES = "description";
    public static final String DRIVER_ID = "driver_id";
    public static final String SHIPPINGPRICE = "shippingprice";

}
