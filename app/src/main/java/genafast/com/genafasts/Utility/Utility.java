package genafast.com.genafasts.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import genafast.com.genafasts.AppController;
import genafast.com.genafasts.R;

public class Utility {
   public static Context mContext;
    private static String PREFERENCE = "Genafast";
    private static String LAN_PREFERENCE = "lan_pre";

    public static void I(Context cx, Class<?> startActivity, Bundle data) {
        Intent i = new Intent(cx, startActivity);
        if (data != null)
            i.putExtras(data);
        cx.startActivity(i);
    }

    public static void I_clear(Context cx, Class<?> startActivity, Bundle data) {
        Intent i = new Intent(cx, startActivity);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (data != null)
            i.putExtras(data);
        cx.startActivity(i);
    }

    public static void E(String msg) {
        if (true)
            Log.e("Log.E By Sohel", msg);
    }

    public static Dialog initProgressDialog(Context cx) {
        ProgressDialog pd = new ProgressDialog(cx);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage(cx.getResources().getString(R.string.loading));
        pd.show();
        return pd;
    }

    public static void T(Context c, String msg) {
        Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static String getSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getString(name, "");
    }

    public static void setSharedPreference(Context context, String name, String value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        editor.putString(name, value);
        editor.apply();
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static int getLanSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(LAN_PREFERENCE, 0);
        return settings.getInt(name, 1);
    }

    public static void setLanSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(LAN_PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        Log.e( "setSharedPreference: ", String.valueOf(value));
        editor.putInt(name,value);
        editor.apply();
    }

    public static void LangChange(Context cx,String lang){
        mContext = cx;
        AppController.localeManager.setNewLocale(cx, lang);
    }


    public static void Alert(final Context context, final String title, final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void ChangeLang(Context appContext, int i) {
        String lang = "";
        switch (i) {
            case 0:
                lang = "en";
                break;
            case 1:
                lang = "es";
                break;
        }

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        Resources res = appContext.getResources();
        config.locale = locale;
        appContext.getResources().updateConfiguration(config, res.getDisplayMetrics());
    }

    public static boolean isAtLeastVersion(int version) {
        return Build.VERSION.SDK_INT >= version;
    }


    public static void clearsharedpreference(Context context) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }
    public static void activityTransition(Context context) {
        Activity activity = (Activity)context;
        activity.overridePendingTransition(R.anim.slide_in,R.anim.nothing);

    }


    public static void removepreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().remove(key).commit();
    }

    public static void ShowToastMessage(Context ctactivity, String message) {
        Toast toast = Toast.makeText(ctactivity, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }


    public static float rounddecimalFormate(float digit){

        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        decimalFormat.applyPattern("##.##");
        decimalFormat.format(digit);
        return digit;
    }

    public static String decimaltwo(String String){
        String  result = "";
        try{
            result = String.format("%.02d", String);

        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

    public static void Rating(float rat, ImageView star1, ImageView star2, ImageView star3,
                              ImageView star4, ImageView star5, int starEmpty , int starHalf,
                              int starFilled)
    {
        if (rat >= 0 && rat < 0.5) {
            star1.setImageResource(starEmpty);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 0.5 && rat < 1) {
            star1.setImageResource(starHalf);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 1 && rat < 1.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 1.5 && rat < 2) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starHalf);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 2 && rat < 2.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 2.5 && rat < 3) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starHalf);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 3 && rat < 3.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 3.5 && rat < 4) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starHalf);
            star5.setImageResource(starEmpty);
        } else if (rat >= 4 && rat < 4.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starEmpty);
        } else if (rat >= 4.5 && rat < 5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starHalf);
        } else if (rat == 5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starFilled);
        }
    }

}
