package genafast.com.genafasts.sticky;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import genafast.com.genafasts.R;
import genafast.com.genafasts.Utility.Constants;
import genafast.com.genafasts.activity.AddToCartActivity;
import genafast.com.genafasts.activity.RestaurantDetailActivity;
import genafast.com.genafasts.listeners.CustomButtonListener;
import genafast.com.genafasts.model.ordergetset;
import tellh.com.stickyheaderview_rv.adapter.StickyHeaderViewAdapter;
import tellh.com.stickyheaderview_rv.adapter.ViewBinder;

/**
 * Created by tlh on 2017/1/22 :)
 */

public class UserItemViewBinder extends ViewBinder<ordergetset, UserItemViewBinder.ViewHolder> {
    final int va = 0;
    final ArrayList<Integer> quantity = new ArrayList<>();
    boolean b;
    Context context;
    Cursor cur = null;
    ArrayList<ordergetset> ordergetsets = new ArrayList<>();
    CustomButtonListener customButtonListener;
    int desk;
    String sa;
    private String menuid321;
    private String de;
    private String foodprice321;
    private LayoutInflater inflater = null;
    private String restID;


    public UserItemViewBinder(ArrayList<ordergetset> ordergetsets, String restID, RestaurantDetailActivity stickyMenuActivity) {
        this.context = stickyMenuActivity;
        this.ordergetsets = ordergetsets;
        this.restID = restID;
    }

    @Override
    public ViewHolder provideViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public void bindView(StickyHeaderViewAdapter stickyHeaderViewAdapter, final ViewHolder holder, final int i, final ordergetset ordergetset) {
        holder.txt_name.setText(String.valueOf(ordergetset.getName()));
        holder.txt_desc.setText(ordergetset.getDesc());
        try {
            holder.txt_price.setText("S/ "+Double.parseDouble(ordergetset.getPrice()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            holder.txt_price.setText("S/ "+ordergetset.getPrice());
        }

        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddToCartActivity.class);
                intent.putExtra(Constants.RESTAURANT_ID, restID);
                intent.putExtra("food_id", ordergetset.getId());
                intent.putExtra("food_name", ordergetset.getName());
                intent.putExtra("food_price", ordergetset.getPrice());
                intent.putExtra("food_instr", ordergetset.getItemInstruction());
                intent.putExtra("food_desc", ordergetset.getDesc());
                context.startActivity(intent);
            }
        });


       /* holder.edTextQuantity.setText("");

        holder.btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDataModel temp = new CartDataModel();
                temp.setFoodid(ordergetset.getId());
                temp.setFoodname(ordergetset.getName());
                temp.setFoodprice(ordergetset.getPrice());
                temp.setFooddesc(ordergetset.getDesc());
                int value = Integer.parseInt(CartDataHelper.getInstance().getUserDataModel().get(i).getQuantity()) + 1;
                temp.setQuantity(String.valueOf(value));
                CartDataHelper.getInstance().insertUserDataModel(temp);
            }
        });*/
    }

    @Override
    public int getItemLayoutId(StickyHeaderViewAdapter adapter) {
        return R.layout.sticky_item_user;
    }

    static class ViewHolder extends ViewBinder.ViewHolder {
        TextView txt_name;
        TextView txt_desc;
        TextView txt_price;
        ImageView imageview,btnAdd;
        LinearLayout btnLayout;
        /*ImageButton btn_minus;
        ImageButton btn_plus;
        EditText edTextQuantity;*/

        public ViewHolder(View view) {
            super(view);
            txt_name = view.findViewById(R.id.txt_name);
            txt_desc = view.findViewById(R.id.txt_desc);
            txt_price = view.findViewById(R.id.txt_price);
            btnLayout = view.findViewById(R.id.btnLayout);
            btnAdd = view.findViewById(R.id.btnAdd);
            /*btn_minus = view.findViewById(R.id.btn_minus);
            btn_plus = view.findViewById(R.id.btn_plus);
            edTextQuantity = view.findViewById(R.id.edTextQuantity);*/
        }

    }

    private class MenuClick implements View.OnClickListener {
        int pos;

        MenuClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, AddToCartActivity.class);
            intent.putExtra("food_id", ordergetsets.get(pos).getId());
            intent.putExtra("food_name", ordergetsets.get(pos).getName());
            intent.putExtra("food_price", ordergetsets.get(pos).getPrice());
            intent.putExtra("food_instr", ordergetsets.get(pos).getItemInstruction());
            intent.putExtra("food_desc", ordergetsets.get(pos).getDesc());
            context.startActivity(intent);
        }
    }
}
