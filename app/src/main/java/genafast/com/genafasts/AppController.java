package genafast.com.genafasts;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import genafast.com.genafasts.data.datahelper.CartDataHelper;
import genafast.com.genafasts.data.datahelper.UserDataHelper;
import genafast.com.genafasts.utils.LocaleManager;


public class AppController extends Application {

    private static AppController mInstance;
    public static final String TAG = AppController.class.getSimpleName();
    private static RequestQueue mRequestQueue;

    public static synchronized AppController getInstance() {
        return mInstance;
    }
    public static LocaleManager localeManager;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/opensans_regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build());



        new UserDataHelper(this);
        new CartDataHelper(this);
    }


    @Override
    protected void attachBaseContext(Context base) {
        localeManager = new LocaleManager(base);
        super.attachBaseContext(localeManager.setLocale(base));
        Log.e(TAG, "attachBaseContext");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localeManager.setLocale(this);
        Log.e(TAG, "onConfigurationChanged: " + newConfig.locale.getLanguage());
    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mInstance);
        }
        return mRequestQueue;
    }

}

